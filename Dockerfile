FROM node:16-alpine3.15 AS builder
WORKDIR /home
ADD ./src ./src
ADD ./public ./public
COPY . .
RUN FONTAWESOME_NPM_AUTH_TOKEN=9099C8B6-F072-4C33-B0BA-34E4910A3BF3 npm install --save @fortawesome/fontawesome-pro
RUN npm run build

FROM nginx:1.21-alpine
WORKDIR /usr/share/nginx/html
COPY --from=builder /home/www .
COPY spa.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]
