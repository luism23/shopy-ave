module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  parser: 'vue-eslint-parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
    parser: '@typescript-eslint/parser'
  },
  plugins: ['@typescript-eslint'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:vue/vue3-recommended',
    'prettier',
    'standard'
  ],
  rules: {
    'vue/no-v-html': 'off',
    'no-console': [2, { allow: ['error'] }],
    'vue/require-default-prop': 'off',
    'vue/multi-word-component-names': 'off',
    'vue/v-on-event-hyphenation': [
      'error',
      'always',
      {
        autofix: false,
        ignore: []
      }
    ],
    'vue/attribute-hyphenation': [
      'error',
      'always',
      {
        ignore: []
      }
    ],
    'space-before-function-paren': [
      'error',
      {
        anonymous: 'never',
        named: 'never',
        asyncArrow: 'always'
      }
    ],
    'vue/require-explicit-emits': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    'no-tabs': 0,
    '@typescript-eslint/ban-types': [
      'error',
      {
        types: {
          Function: false
        },
        extendDefaults: true
      }
    ],
    'vue/padding-line-between-blocks': ['error', 'always'],
    'vue/block-lang': [
      'error',
      {
        script: {
          lang: 'ts'
        }
      }
    ],
    'vue/no-multi-spaces': [
      'error',
      {
        ignoreProperties: false
      }
    ],
    'vue/component-tags-order': [
      'error',
      {
        order: [['template', 'script'], 'style']
      }
    ]
  }
}
