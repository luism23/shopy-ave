import { reactive, computed } from 'vue'
import { defineStore } from 'pinia'

import {
  CartProduct,
  SaleInfoQuote,
  SalePayment,
  SaleCollectedValue,
  SaleProducts,
  OrderCondition,
  CartProductListObject
} from '@/types'

import {
  getAccountBankTypeByID,
  getBankByID,
  updateOrderConditionService
} from '@/services'
import { toClipboard } from '@soerenmartius/vue3-clipboard'

export const useCreateSaleOrder = defineStore('create-sale-order', () => {
  const products = reactive<SaleProducts>({
    searchCategory: '',
    byCategory: [],
    product: {} as CartProductListObject,
    selected: [],
    orderCondition: {} as OrderCondition
  })

  const payment = reactive<SalePayment>({
    isCashDelivery: false, // contraentrega
    isOther: false,
    isElectronic: false,
    isDeliveryPointSale: false,
    isShippingCostDestiny: false,
    otherMethodID: '',
    collection: { isByCashDelivery: false, isByElectronic: false },
    paymentsMethods: [
      { id: 'WIRE_TRANSFER', name: 'Transferencia bancaria' },
      { id: 'MONEY', name: 'Efectivo' },
      { id: 'CREDIT', name: 'Crédito' }
    ]
  })

  const quote = reactive<SaleInfoQuote>({ list: [] })

  const collectedValue = reactive<SaleCollectedValue>({
    total: 0,
    description: ''
  })

  const copyOrder: any = reactive({
    statusCopyGlobal: false,
    reRender: {
      key: 0,
      mutation: () => copyOrder.reRender.key++
    },
    financial: {
      bank: '',
      typeAccount: '',
      getInfo: async () => {
        try {
          const { payment_information: paymentInformation } =
            products.orderCondition

          if (
            paymentInformation?.id_bank &&
            paymentInformation?.id_account_type
          ) {
            const [bank, account] = await Promise.allSettled([
              getBankByID(paymentInformation?.id_bank),
              getAccountBankTypeByID(paymentInformation?.id_account_type)
            ])

            if (bank.status === 'fulfilled') {
              copyOrder.financial.bank = bank.value.data.name
            }

            if (account.status === 'fulfilled') {
              copyOrder.financial.typeAccount = account.value.data.name
            }
          }
        } catch (error) {}
      }
    },
    info: [
      {
        id: 'ALL',
        status: false,
        title: 'Enviar todo',
        isCopied: false,
        text: ''
      },
      {
        id: 'PAYMENT_METHODS',
        status: false,
        title: 'Formas de pago',
        isCopied: false,
        text: 'Contraentrega, pago electrónico, otro medio que son los datos bancarios.'
      },
      {
        id: 'BANK_DATA',
        status: false,
        title: 'Datos bancarios',
        isCopied: false
      },
      {
        id: 'DETAILS_PRODUCTS',
        status: false,
        title: 'Detalle Producto',
        isCopied: false
      },
      {
        id: 'SHIPPING',
        status: false,
        title: 'Flete',
        isCopied: false
      }
    ]
  })

  const reRender = reactive({ key: 0, mutation: () => reRender.key++ })

  const actionProduct = reactive({
    getCategoryProducts: computed(() => {
      return products.byCategory.filter((category: any) => {
        return category.name
          .toLowerCase()
          .includes(products.searchCategory?.toLowerCase())
      })
    }),
    totalProductsSelected: computed(() => products.selected.length),
    hasProductSelected: computed(() => products.selected.length > 0),
    totalOrderValueSelected: computed(() => {
      const getTotal = (acum: number, curr: any) => {
        const { withDiscount, notDiscount } = curr.totalAmount
        if (withDiscount) {
          return acum + Number(withDiscount) * Number(curr.selectedUnits)
        }
        return acum + Number(notDiscount) * Number(curr.selectedUnits)
      }

      if (products.selected.length > 0) {
        const totalOrder = products.selected.reduce(getTotal, 0)
        return totalOrder
      }

      return 0
    }),
    getProduct: (product: any): CartProduct | undefined => {
      if (typeof product === 'object' && product !== null) {
        if (Object.keys(product).length === 0) return undefined
      }
      return product
    },
    deleteProduct: (id: string) => {
      const remove = (product: CartProduct) => product.id !== id
      products.selected = products.selected.filter(remove)

      if (products.product[id]) {
        products.product[id] = {}
        reRender.mutation()
      }

      return products
    }
  })

  const actionPayment = reactive({
    tabs: [
      {
        id: 'PAYMENT_CASH_DELIVERY',
        name: 'Pago contraentrega',
        img: '/assets/img/logo/logo-pay-contraentrega.svg',
        imgDark: '/assets/img/logo/logo-pay-contraentrega-red.svg',
        status: payment.isCashDelivery
      },
      {
        id: 'PAYMENT_ELECTRONIC',
        name: 'Pago electrónico',
        img: '/assets/img/logo/logo-pay-wompi.svg',
        imgDark: '/assets/img/logo/logo-pay-wompi-red.svg',
        status: payment.isElectronic
      },
      {
        id: 'PAYMENT_OTHER',
        name: 'Otro medio',
        img: '/assets/img/logo/logo-pay-other.svg',
        imgDark: '/assets/img/logo/logo-pay-other-red.svg',
        status: payment.isOther
      }
    ],
    getTab: (id: string) => {
      if (products.selected.length > 0) {
        if (id === 'PAYMENT_ELECTRONIC') {
          return ''
        }
        actionPayment.tabs = actionPayment.tabs.map((item: any) => {
          if (id === 'PAYMENT_CASH_DELIVERY' && payment.isDeliveryPointSale) {
            return item
          }

          if (id === item.id) {
            payment.isCashDelivery = ['PAYMENT_CASH_DELIVERY'].includes(item.id)
            // payment.isElectronic = ['PAYMENT_ELECTRONIC'].includes(item.id)
            payment.isElectronic = false
            payment.isOther = ['PAYMENT_OTHER'].includes(item.id)

            if (payment.isCashDelivery || payment.isElectronic) {
              payment.otherMethodID = ''
            }

            return { ...item, status: true }
          }

          return { ...item, status: false }
        })
      }
    },
    disabledAllTabs: () => {
      actionPayment.tabs = actionPayment.tabs.map((item: any) => {
        return { ...item, status: false }
      })
    },
    hasMethodPaySelected: computed(() => {
      return payment.isCashDelivery || payment.isOther || payment.isElectronic
    }),
    mutationDeliveryPointSale: (status: boolean) => {
      if (!status) {
        actionPayment.disabledAllTabs()
      }

      if (status) {
        actionPayment.getTab('PAYMENT_OTHER')
      }
    },
    mutationCollectedValue: async ({
      isActive = false,
      type = ''
    }: {
      isActive: boolean
      type: 'isByCashDelivery' | 'isByElectronic' | ''
    }) => {
      const key =
        type === 'isByCashDelivery'
          ? 'is_delivery_payment'
          : 'is_electronic_payment'

      const body = {
        ...products.orderCondition,
        [key]: isActive
      }

      products.orderCondition = body

      try {
        await updateOrderConditionService(body)
      } catch (error) {}
    }
  })

  const actionCollectedValue = reactive({
    onSubmit: async () => {
      if (collectedValue.total !== 0) {
        // return await stateQuote.quote()
      }
    }
  })

  const actionCopyOrder = reactive({
    canCopy: computed(() => {
      return copyOrder.info.some((item: any) => item.status)
    }),
    getTextCopy: computed(() => (isCopied: boolean) => {
      return isCopied ? 'Copiado' : 'Copiar mensaje'
    }),
    getInfoForCopy: () => {
      let productsText = ''
      const shipping = `Valor del envío: ${0}`
      const payments =
        'Formas de pago: Contraentrega, pago electrónico, transferencia bancaria, efectivo, crédito'
      const bank = `Datos bancarios: Banco: ${copyOrder.financial.bank}\nTipo: ${copyOrder.financial.typeAccount},\nNúmero: ${products.orderCondition?.payment_information?.account_number}`

      if (products.selected && products.selected?.length > 0) {
        productsText = products.selected
          .map((product: any) => {
            let textAttributes = ''

            const getAttrText = (attribute = {}) => {
              const text = []
              const attrs = Object.entries(attribute ?? {})
              if (attrs.length > 0) {
                for (const [key, value] of Object.entries(attribute ?? {})) {
                  text.push(`${key}: ${value}`)
                }
              }

              return text
            }

            const attrs = [
              ...getAttrText(product?.attributes.attribute1),
              ...getAttrText(product?.attributes.attribute2),
              ...getAttrText(product?.attributes.attribute3)
            ]

            if (attrs.length > 0) {
              textAttributes = attrs.filter(Boolean).join(', ')
            }

            const text = `Detalle producto:\n Producto: ${product.name} ${textAttributes}\n`

            return text.trim()
          })
          .join(', ')
      }

      return { products: productsText, payments, bank, shipping }
    },
    getCopyAll: ({ isActive = false } = {}) => {
      const allInfo = actionCopyOrder.getInfoForCopy()
      const { bank, payments, products, shipping } = allInfo
      const all = `${payments},\n${bank},\n${products},\n${shipping}`

      if (isActive) {
        toClipboard(all)
        copyOrder.statusCopyGlobal = true
        return window.setTimeout(() => {
          return (copyOrder.statusCopyGlobal = false)
        }, 1000)
      }

      return toClipboard(all)
    },
    getCopy: (data: any) => {
      const allInfo = actionCopyOrder.getInfoForCopy()
      const { bank, payments, products, shipping } = allInfo

      if (data.id === 'PAYMENT_METHODS') toClipboard(payments)
      if (data.id === 'BANK_DATA') toClipboard(bank)
      if (data.id === 'DETAILS_PRODUCTS') toClipboard(products)
      if (data.id === 'SHIPPING') toClipboard(shipping)
      if (data.id === 'ALL') actionCopyOrder.getCopyAll()

      copyOrder.reRender.mutation()
      data.isCopied = true
      window.setTimeout(() => (data.isCopied = false), 1000)
    },
    getCopySelected: () => {
      const allInfo = actionCopyOrder.getInfoForCopy()
      const { bank, payments, products, shipping } = allInfo

      const onlyOk = copyOrder.info.filter((item: any) => item.status)
      const selected = []

      for (const item of onlyOk) {
        if (item.id === 'PAYMENT_METHODS') selected.push(payments)
        if (item.id === 'BANK_DATA') selected.push(bank)
        if (item.id === 'DETAILS_PRODUCTS') selected.push(products)
        if (item.id === 'SHIPPING') selected.push(shipping)
        if (item.id === 'ALL') actionCopyOrder.getCopyAll()
      }

      if (selected.length > 0) {
        toClipboard(selected.join(', \n'))
      }

      copyOrder.statusCopyGlobal = true
      window.setTimeout(() => (copyOrder.statusCopyGlobal = false), 1000)
    },
    toggleAllChecked: (data: any) => {
      if (data.id === 'ALL') {
        copyOrder.info = copyOrder.info.map((item: any) => {
          return { ...item, status: data.status }
        })
      }
    }
  })

  return {
    products,
    payment,
    collectedValue,
    quote,
    reRender,
    copyOrder,
    actionProduct,
    actionPayment,
    actionCollectedValue,
    actionCopyOrder
  }
})
