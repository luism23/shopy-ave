import { ref } from 'vue'
import { defineStore } from 'pinia'

import { SaleFormCustomer } from '@/types'

export const useCreateSaleCustomer = defineStore('create-sale-customer', () => {
  const isValidate = ref(false)
  const customer = ref<SaleFormCustomer>({
    id: '',
    name: '',
    campaign: { id: '', name: '' },
    email: '',
    phone: undefined,
    country: { id: 170, name: 'Colombia' },
    cityID: undefined
  })

  const mutation = (data: SaleFormCustomer) => (customer.value = data)

  return { customer, isValidate, mutation }
})
