import { Modes } from '@/constants'
import { InoDeliveryNovalty } from '@/types'
import { logDev } from '@/utils'
import { defineStore } from 'pinia'

export const useAppnoDelivaryNovalty = defineStore('appStoreDelivery', {
  state: () => ({
    mode: <string>'',
    dataNoDeliveryNovaltyEdit: <InoDeliveryNovalty | null>{},
    allNoDeliveryDataNovalty: <any[]>[]
  }),
  getters: {
    getMode(state) {
      return state.mode
    },
    getNoDeliveryNovaltylEdit(state) {
      return state.dataNoDeliveryNovaltyEdit
    },
    getAllNoDeliveryDataNovalty(state) {
      logDev('getAllNoDeliveryAllDataNovalty', state)
      return state.allNoDeliveryDataNovalty
    }
  },
  actions: {
    changeMode(mode: Modes) {
      this.mode = mode
    },
    changeNoDeliverydataNovalty(data: InoDeliveryNovalty) {
      this.dataNoDeliveryNovaltyEdit = data
    },
    resetNoDeliveryEditDataNovalty() {
      this.dataNoDeliveryNovaltyEdit = null
    },

    insertNoDeliveryAllDataNovalty(data: InoDeliveryNovalty[]) {
      logDev('insertNoDeliveryAllDataNovalty', data)
      this.allNoDeliveryDataNovalty = data
    }
  }
})
