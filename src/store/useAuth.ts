import { defineStore } from 'pinia'
import { ref } from 'vue'

import { getStorage } from '@/utils'
import { KEY_STORAGE_AUTH } from '@/store'

export const useAuth = defineStore('auth', () => {
  const isAuth = ref(false)

  const updateAuth = () => {
    isAuth.value = Boolean(getStorage(KEY_STORAGE_AUTH)) ?? false
  }

  const removeAuth = () => (isAuth.value = false)

  return { isAuth, updateAuth, removeAuth }
})
