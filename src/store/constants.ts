/**
 * Storage
 */

const KEY_STORAGE_AUTH = 'ave-auth'

/**
 * Query params
 */

const ONBOARDING = 'onboarding'
const ONBOARDING_INVITATION = 'onboarding-invitation'
const LOGIN = 'login'
const COMPANY_CREATED = 'company-created'
const COMPANY_EXIST = 'company-exist'
const NEW_COMPANY = 'new-company'

export {
  KEY_STORAGE_AUTH,
  ONBOARDING,
  ONBOARDING_INVITATION,
  LOGIN,
  COMPANY_CREATED,
  COMPANY_EXIST,
  NEW_COMPANY
}
