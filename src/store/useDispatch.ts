import { reactive } from 'vue'
import { defineStore } from 'pinia'

import { getDispatchService } from '@/services'
import {
  QueryParamsDispatchs,
  Dispatchs,
  DispatchsStatusOptions
} from '@/types'

export const useDispatch = defineStore('dispatch', () => {
  const state = reactive<any>({
    isLoading: false,
    pagination: { rows: [6, 8, 16], total: 0 },
    query: { page: 1, perPage: 5, from: '', to: '', status: '' },
    dispatches: [] as Dispatchs[],
    headersFilter: [] as DispatchsStatusOptions[]
  })

  const action = reactive({
    getDispatches: async () => {
      try {
        state.isLoading = true
        const { data, status } = await getDispatchService(state.query)
        if (status === 200) {
          state.dispatches = data.dispatchs
          state.headersFilter = data.status_options
          state.pagination.total = data.total_pages
        }
      } finally {
        state.isLoading = false
      }
    },
    mutationQuery: (params: QueryParamsDispatchs) => {
      state.query = params
    },
    mutationPageRows: async () => {
      state.query.page = 1
      await action.getDispatches()
    }
  })

  return { state, action }
})
