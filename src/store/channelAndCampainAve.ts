import { Modes } from '@/constants'
import { IdataCampana } from '@/types/GeneralCampainAve'
import { IdataCanales } from '@/types/GeneralChannelsAve'
import { defineStore } from 'pinia'

interface ListCampain {
  id: string
  name: string
  status: boolean
}
interface ListChannel {
  id: string
  name: string
  status: boolean
}
interface ListService {
  id: string
  name: string
  status: boolean
}
export const useAppStore = defineStore('appStore', {
  state: () => ({
    mode: <string>'',
    dataChannelEdit: <IdataCanales | null>{},
    allDataChannel: <IdataCanales[]>[],
    dataCampanaEdit: <IdataCampana | null>{},
    allDataCampana: <IdataCampana[]>[],
    ListCampain: <ListCampain[]>[],
    ListChannel: <ListChannel[]>[],
    ListService: <ListService[]>[]
  }),
  getters: {
    getMode(state) {
      return state.mode
    },
    getChannelEdit(state) {
      return state.dataChannelEdit
    },
    getAllDataChannel(state) {
      return state.allDataChannel
    },

    getCampainEdit(state) {
      return state.dataCampanaEdit
    },
    getAllDataCampain(state) {
      return state.allDataCampana
    },
    getCampain(state) {
      return state.ListCampain
    },
    getChannel(state) {
      return state.ListChannel
    },
    getService(state) {
      return state.ListService
    }
  },

  actions: {
    changeMode(mode: Modes) {
      this.mode = mode
    },
    changedataChannel(data: IdataCanales) {
      this.dataChannelEdit = data
    },
    resetEditDataChannel() {
      this.dataChannelEdit = null
    },

    insertAllDataChannel(data: IdataCanales[]) {
      this.allDataChannel = data

      data.forEach((el) => {
        if (this.ListChannel.length === 0) {
          this.ListChannel.push({
            id: String(el.id),
            name: el.ds,
            status: false
          })
        } else {
          let exist = false
          this.ListChannel.forEach((x) => {
            if (String(x.id) === String(el.id)) {
              exist = true
            }
          })
          if (!exist) {
            this.ListChannel.push({
              id: String(el.id),
              name: el.ds,
              status: false
            })
          }
        }
      })
    },

    changedataCampagne(data: IdataCampana) {
      this.dataCampanaEdit = data
    },
    resetDataCampagne() {
      this.dataCampanaEdit = null
    },

    insertAllDataCampagne(data: IdataCampana[]) {
      this.allDataCampana = data

      data.forEach((el) => {
        if (this.ListService.length === 0) {
          this.ListService.push({
            id: String(el.idservicio),
            name: el.ds_servicio,
            status: false
          })
        }

        /* /////////////////////////  */
        if (this.ListCampain.length === 0) {
          this.ListCampain.push({
            id: String(el.id),
            name: el.nombre,
            status: false
          })
        } else {
          let exist = false
          this.ListService.forEach((x) => {
            if (String(x.id) === String(el.idservicio)) {
              exist = true
            }
          })
          this.ListCampain.forEach((x) => {
            if (String(x.id) === String(el.id)) {
              exist = true
            }
          })
          if (!exist) {
            this.ListService.push({
              id: String(el.idservicio),
              name: el.ds_servicio,
              status: false
            })
          }
          if (!exist) {
            this.ListCampain.push({
              id: String(el.id),
              name: el.nombre,
              status: false
            })
          }
        }
      })
    }
  }
})
