import { defineStore } from 'pinia'

import { getStorage, setStorage, toCapitalize } from '@/utils'
import { LoginResponse } from '@/types'
import { KEY_STORAGE_AUTH, useAuth } from '@/store'

type State = { name: string; role: string }

export const useUser = defineStore('user', {
  state: (): State => ({ name: '', role: '' }),
  getters: {
    getUser: (state): State => {
      if (state && Object.values(state).every((value) => value)) {
        return {
          name: toCapitalize(state.name),
          role: toCapitalize(state.role)
        }
      }

      const storage = getStorage(KEY_STORAGE_AUTH)

      if (storage) {
        return {
          role: toCapitalize(storage.role),
          name: toCapitalize(storage.name)
        }
      }

      return { role: '', name: '' }
    }
  },
  actions: {
    updateUser(data: LoginResponse) {
      const auth = useAuth()
      setStorage({ id: KEY_STORAGE_AUTH, value: data })
      auth.updateAuth()
      this.name = toCapitalize(data.name)
      this.role = toCapitalize(data.role)
    }
  }
})
