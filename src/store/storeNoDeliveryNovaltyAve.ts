import { InoDeliveryNovalty } from '@/types'
import { defineStore } from 'pinia'

export const useNoDeliveryTime = defineStore('NoDeliveryTime', {
  state: () => ({
    mode: '',
    dataNoDeliveryTime: <InoDeliveryNovalty[]>[]
  }),
  getters: {
    getMode(state) {
      return state.mode
    },
    getNoDeliveryTime(state) {
      return state.dataNoDeliveryTime
    }
  },
  actions: {
    setMode(mode: string) {
      this.mode = mode
    },

    setNoDeliveryTime(data: InoDeliveryNovalty[]) {
      this.dataNoDeliveryTime = data
    }
  }
})
