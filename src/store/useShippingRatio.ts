import { reactive } from 'vue'
import { defineStore } from 'pinia'

export const useShippingRatio = defineStore('shipping-ratio', () => {
  const state = reactive({
    operator: {
      logisticOperator: undefined,
      dispatchCenter: { id: undefined, name: '' }
    },
    guides: [],
    observation: '',
    modalError: false,
    tab: {
      current: 1,
      isActiveTab1: false,
      isActiveTab2: false,
      isActiveTab3: false
    },
    getTab: (currentTab: 1 | 2 | 3) => {
      if (currentTab === 1) {
        state.tab.current = 1
        state.guides = []
        state.tab.isActiveTab2 = false
        return (state.tab.isActiveTab1 = true)
      }

      if (currentTab === 2) {
        if (state.operator.dispatchCenter && state.operator.dispatchCenter.id) {
          state.tab.current = 2
          return (state.tab.isActiveTab1 = true)
        }
        return (state.modalError = true)
      }

      if (currentTab === 3) {
        if (state.guides.length > 0) {
          state.tab.current = 3
          return (state.tab.isActiveTab2 = true)
        }
        return (state.modalError = true)
      }
    }
  })

  const reset = () => {
    state.guides = []
    state.operator.logisticOperator = undefined
    state.operator.dispatchCenter.id = undefined
    state.operator.dispatchCenter.name = ''
    state.observation = ''
    state.tab.current = 1
    state.tab.isActiveTab1 = false
    state.tab.isActiveTab2 = false
    state.tab.isActiveTab3 = false
  }

  return { state, reset }
})
