import { defineStore } from 'pinia'
import { IdataNovalty } from '@/types/NovaltyAve'
import { Modes } from '@/constants'
interface ListTransp {
  id: string
  name: string
  status: boolean
}
interface ListDestiny {
  id: string
  name: string
  status: boolean
}

interface ListOrigin {
  id: string
  name: string
  status: boolean
}

interface ListClient {
  id: string
  name: string
  status: boolean
}

interface ListType {
  id: string
  name: string
  status: boolean
}
export const useAppNovalty = defineStore('appStore', {
  state: () => ({
    mode: <string>'',
    dataNovaltyEdit: <IdataNovalty | null>{},
    allDataNovalty: <IdataNovalty[]>[],
    listTranporter: <ListTransp[]>[],
    listDestiny: <ListDestiny[]>[],
    ListOrigin: <ListOrigin[]>[],
    ListClient: <ListClient[]>[],
    ListType: <ListType[]>[]
  }),
  getters: {
    getMode(state) {
      return state.mode
    },
    getNovaltylEdit(state) {
      return state.dataNovaltyEdit
    },
    getAllDataNovalty(state) {
      return state.allDataNovalty
    },
    getTransp(state) {
      return state.listTranporter
    },
    getDestiny(state) {
      return state.listDestiny
    },
    getOrigin(state) {
      return state.ListOrigin
    },
    getClient(state) {
      return state.ListClient
    },
    getType(state) {
      return state.ListType
    }
  },
  actions: {
    changeMode(mode: Modes) {
      this.mode = mode
    },
    changedataNovalty(data: IdataNovalty) {
      this.dataNovaltyEdit = data
    },
    resetEditDataNovalty() {
      this.dataNovaltyEdit = null
    },

    insertAllDataNovalty(data: IdataNovalty[]) {
      this.allDataNovalty = data

      data.forEach((el) => {
        if (this.ListType.length === 0) {
          this.ListType.push({
            id: String(el.pkid),
            name: el.Des_Novedad,
            status: false
          })
        }

        /* /////////////////////////  */
        if (this.ListClient.length === 0) {
          this.ListClient.push({
            id: String(el.pkid),
            name: el.empresa,
            status: false
          })
        }

        /* /////////////////////////  */
        if (this.ListOrigin.length === 0) {
          this.ListOrigin.push({
            id: String(el.pkid),
            name: el.dsciudad,
            status: false
          })
        }
        /* /////////////////////////  */
        if (this.listDestiny.length === 0) {
          this.listDestiny.push({
            id: String(el.pkid),
            name: el.dsciudadd,
            status: false
          })
        }
        /* /////////////////////////  */
        if (this.listTranporter.length === 0) {
          this.listTranporter.push({
            id: el.idtransportador,
            name: el.dstransportadora,
            status: false
          })
        } else {
          let exist = false
          this.ListType.forEach((x) => {
            if (String(x.id) === String(el.pkid)) {
              exist = true
            }
          })
          this.ListClient.forEach((x) => {
            if (String(x.id) === String(el.pkid)) {
              exist = true
            }
          })
          this.ListOrigin.forEach((x) => {
            if (String(x.id) === String(el.pkid)) {
              exist = true
            }
          })
          this.listDestiny.forEach((x) => {
            if (String(x.id) === String(el.pkid)) {
              exist = true
            }
          })
          this.listTranporter.forEach((x) => {
            if (String(x.id) === String(el.idtransportador)) {
              exist = true
            }
          })
          /* /////////////////////////  */
          if (!exist) {
            this.ListType.push({
              id: String(el.pkid),
              name: el.Des_Novedad,
              status: false
            })
          }
          if (!exist) {
            this.ListClient.push({
              id: String(el.pkid),
              name: el.empresa,
              status: false
            })
          }
          if (!exist) {
            this.ListOrigin.push({
              id: String(el.pkid),
              name: el.dsciudad,
              status: false
            })
          }
          if (!exist) {
            this.listDestiny.push({
              id: String(el.pkid),
              name: el.dsciudadd,
              status: false
            })
          }
          if (!exist) {
            this.listTranporter.push({
              id: el.idtransportador,
              name: el.dstransportadora,
              status: false
            })
          }
        }
      })
    }
  }
})
