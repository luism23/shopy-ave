import TheMenu from '../components/TheMenu.vue'

export default {
  title: 'Components/TheMenu',
  component: TheMenu
}

const Template = (args, { argTypes, events }) => ({
  components: { TheMenu },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: '<TheMenu v-bind="args" v-on="events" />'
})

export const Menu = Template.bind({})

Menu.parameters = {
  state: {
    routes: [
      {
        id: 1,
        display: true,
        name: 'Test',
        path_name: 'Inventory',
        icon_name: 'user',
        children: [
          {
            name: 'Inventory',
            path_name: 'Inventory'
          }
        ]
      }
    ]
  }
}

Menu.args = {
  isOpen: true
}
