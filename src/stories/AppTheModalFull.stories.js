import TheModalFull from '../components/TheModalFull.vue'

export default {
  title: 'Components/TheModalFull',
  component: TheModalFull
}

const Template = (args, { argTypes, events }) => ({
  components: { TheModalFull },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: `<TheModalFull v-bind="args" v-on="events">${args.slotTemplate}</TheModalFull>`
})

const MOCK = {
  modelValue: true,
  title: 'Title',
  subtitle: 'Subtitle',
  content: 'Content',
  urlImage: 'https://i.pravatar.cc/500'
}

export const ModalDefault = Template.bind({})

ModalDefault.args = {
  ...MOCK,
  // eslint-disable-next-line quotes
  slotTemplate: `
    <div style="margin: 1rem">
      <h4>Hello!! Here children nodes main</h4>
    </div>
  `
}

export const ModalDefaultWithScrollY = Template.bind({})

ModalDefaultWithScrollY.args = {
  ...MOCK,
  // eslint-disable-next-line quotes
  slotTemplate: `
    <div style="margin: 0.5rem 1rem">
      <h4>Hello!! Here children nodes main</h4>
      <p v-for="(item, index) in Array(100).fill('node')">{{item}} {{index}}</p>
    </div>
  `
}

export const ModalOnlyTitle = Template.bind({})

ModalOnlyTitle.args = {
  modelValue: true,
  title: 'Title',
  // eslint-disable-next-line quotes
  slotTemplate: `
    <div style="margin: 1rem">
      <h4>Hello!! Here children nodes main</h4>
    </div>
  `
}

export const ModalWithManualHeader = Template.bind({})

ModalWithManualHeader.args = {
  modelValue: true,
  hasManualHeader: true,
  // eslint-disable-next-line quotes
  slotTemplate: `
    <template #header>
      <div style="padding: 0rem 0 0 1rem">
        <h4>Im header</h4>
        <p>Children nodes for header</p>
      </div>
    </template>
    <div style="margin: 1rem">
      <h4>Hello!! Here children nodes main</h4>
    </div>
  `
}
