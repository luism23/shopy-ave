import AppDatePickerRange from '../components/AppDatePickerRange.vue'
import { format, startOfTomorrow } from 'date-fns'

export default {
  title: 'Components/AppDatePickerRange',
  component: AppDatePickerRange
}

const Template = (args, { argTypes, events }) => ({
  components: { AppDatePickerRange },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: '<AppDatePickerRange v-bind="args" v-on="events"/>'
})

export const ForDefault = Template.bind({})

ForDefault.args = {}

export const WithPositionRight = Template.bind({})

WithPositionRight.args = { position: 'right' }

WithPositionRight.decorators = [
  () => ({
    template:
      '<div style="display: flex; justify-content: flex-end"><story/></div>'
  })
]

export const WithPositionLeft = Template.bind({})

WithPositionLeft.args = { position: 'left' }

WithPositionLeft.decorators = [
  () => ({
    template:
      '<div style="display: flex; justify-content: flex-start"><story/></div>'
  })
]

export const WithBorder = Template.bind({})

WithBorder.args = { hasBorder: true }

export const WithFormatter = Template.bind({})

WithFormatter.args = {
  ...WithBorder.args,
  formatter: 'yyyy/MM/dd'
}

export const WithStartToFrom = Template.bind({})

WithStartToFrom.args = {
  ...WithBorder.args,
  from: format(new Date().setDate(new Date().getDate() - 2), 'yyyy-MM-dd'),
  to: format(startOfTomorrow(), 'yyyy-MM-dd')
}

export const DatePickerSingle = Template.bind({})

DatePickerSingle.args = {
  type: 'single'
}

export const DatePickerSingleWithLabel = Template.bind({})

DatePickerSingleWithLabel.args = {
  type: 'single',
  formSingle: {
    id: 'id',
    label: 'Label bro *'
  }
}

export const DatePickerSingleWithLabelAndError = Template.bind({})

DatePickerSingleWithLabelAndError.args = {
  type: 'single',
  formSingle: {
    id: 'id',
    label: 'Label bro',
    error: 'Hay error'
  }
}

export const DatePickerSingleWithPlaceholder = Template.bind({})

DatePickerSingleWithPlaceholder.args = {
  type: 'single',
  formSingle: {
    id: 'id',
    label: 'Label bro',
    placeholder: 'DD/MM/YYYY'
  }
}
