import AppUploadFile from '../components/AppUploadFile.vue'

export default {
  title: 'Components/AppUploadFile',
  component: AppUploadFile
}

const Template = (args, { argTypes, events }) => ({
  components: { AppUploadFile },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template:
    '<AppUploadFile v-bind="args" v-model="args.modelValue" v-on="events" />'
})

export const UploadFile = Template.bind({})

UploadFile.args = {
  label: 'label *'
}

export const UploadFileWithTooltip = Template.bind({})

UploadFileWithTooltip.args = {
  label: 'label *',
  type: 'file',
  file: {
    isActiveTooltip: true,
    accept: 'image/jpeg, image/jpg, application/pdf',
    themeTooltip: 'Primary',
    titleTooltip: 'Formato del archivo',
    messageTooltip: 'Formatos permitidos PDF (3 MB), JPEG (5 MB)'
  }
}

export const UploadFileWithError = Template.bind({})

UploadFileWithError.args = {
  label: 'label *',
  error: 'error...'
}
