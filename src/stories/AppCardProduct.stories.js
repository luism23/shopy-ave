import AppCardProduct from '../components/AppCardProduct.vue'

export default {
  title: 'Components/AppCardProduct',
  component: AppCardProduct
}

const Template = (args, { argTypes, events }) => ({
  components: { AppCardProduct },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template:
    '<AppCardProduct v-bind="args" v-model="args.modelValue" v-on="events" />'
})

export const CardImgDefault = Template.bind({})

CardImgDefault.args = {
  product: {
    id: '1x',
    stock: 10,
    name: 'Camiseta',
    reference: 'THOR-01',
    price: 20000,
    discount: 0,
    isVariable: true,
    description: '<p>Camiseta propia</p>',
    img: '',
    attribute1: [{ Color: 'Negro' }, { Color: 'Blanco' }],
    attribute2: [{ Talla: 'L' }, { Talla: 'M' }],
    attribute3: [{ Marca: 'Midgard' }]
  }
}

export const CardWithImg = Template.bind({})

CardWithImg.args = {
  product: {
    id: '1x',
    stock: 10,
    name: 'Camiseta',
    reference: 'THOR-01',
    price: 20000,
    discount: 0,
    description: '<p>Camiseta propia</p>',
    isVariable: true,
    img: 'https://i.pravatar.cc/700',
    attribute1: [{ Color: 'Negro' }, { Color: 'Blanco' }],
    attribute2: [{ Talla: 'L' }, { Talla: 'M' }],
    attribute3: [{ Marca: 'Midgard' }]
  }
}

export const CardWithoutAttributes = Template.bind({})

CardWithoutAttributes.args = {
  product: {
    id: '1x',
    stock: 10,
    name: 'Camiseta',
    reference: 'THOR-01',
    price: 5000,
    isVariable: false,
    description: '<p>Camiseta propia</p>',
    discount: 0,
    img: 'https://i.pravatar.cc/700',
    attribute1: [],
    attribute2: [],
    attribute3: []
  }
}

export const CardStockZero = Template.bind({})

CardStockZero.args = {
  product: {
    id: '1x',
    stock: 0,
    name: 'Camiseta',
    reference: 'THOR-01',
    description: '<p>Camiseta propia</p>',
    price: 5000,
    isVariable: false,
    discount: 0,
    img: 'https://i.pravatar.cc/700',
    attribute1: [],
    attribute2: [],
    attribute3: []
  }
}
