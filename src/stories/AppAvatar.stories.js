import AppAvatar from '../components/AppAvatar.vue'

export default {
  title: 'Components/AppAvatar',
  component: AppAvatar,
  argTypes: {
    size: {
      name: 'size',
      type: { name: 'string', required: false },
      description: 'Size avatar',
      defaultValue: 'sm',
      table: {
        defaultValue: { summary: 'select' }
      },
      options: ['sm', 'md', 'lg', 'xl', 'xxl'],
      control: {
        type: 'select'
      }
    }
  }
}

const Template = (args, { argTypes }) => ({
  components: { AppAvatar },
  props: Object.keys(argTypes),
  setup() {
    return { args }
  },
  template: '<AppAvatar v-bind="args" v-model="args.modelValue"  />'
})

export const Avatar = Template.bind({})

Avatar.args = {
  size: 'lg',
  src: '/file/user/b24b9c77-a03e-4efa-bb59-563f39e7eb80_profile.jpg'
}

export const AvatarDefault = Template.bind({})

AvatarDefault.args = {
  size: 'lg'
}

export const AvatarRandomDefault = Template.bind({})

AvatarRandomDefault.args = {
  size: 'lg',
  srcRandom: true
}
