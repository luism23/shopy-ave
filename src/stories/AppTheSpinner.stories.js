import TheSpinner from '../components/TheSpinner.vue'

export default {
  title: 'Components/TheSpinner',
  component: TheSpinner
}

const Template = (args, { argTypes, events }) => ({
  components: { TheSpinner },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: `<TheSpinner v-bind="args" v-on="events">${args.slotTemplate}</TheSpinner>`
})

export const Spinner = Template.bind({})

Spinner.args = {
  isActive: true
}
