import AppAutocomplete from '../components/AppAutocomplete.vue'

export default {
  title: 'Components/AppAutocomplete',
  component: AppAutocomplete
}

const Template = (args, { argTypes, events }) => ({
  components: { AppAutocomplete },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template:
    '<AppAutocomplete v-bind="args" v-model="args.modelValue" v-on="events" />'
})

export const AutoComplete = Template.bind({})

const mockData = [
  {
    id: '1',
    name: 'Odin'
  },
  {
    id: '2',
    name: 'Hela'
  }
]

AutoComplete.args = {
  label: 'Title',
  name: 'name',
  optionText: 'name',
  optionValue: 'id',
  error: '',
  options: mockData,
  search: async () => {
    const data = await Promise.resolve(mockData)
    return data
  },
  success: (value) => {
    return value
  }
}

export const AutoCompleteWithPlaceholder = Template.bind({})

AutoCompleteWithPlaceholder.args = {
  ...AutoComplete.args,
  placeholder: 'Text placeholder'
}

export const AutoCompleteWithPersistenceValue = Template.bind({})

AutoCompleteWithPersistenceValue.args = {
  ...AutoComplete.args,
  havePersistenceValueSearched: true
}
