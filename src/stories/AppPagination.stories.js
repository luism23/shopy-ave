import AppPagination from '../components/AppPagination.vue'

export default {
  title: 'Components/AppPagination',
  component: AppPagination,
  decorators: [
    () => ({
      template:
        '<div style="display: flex; justify-content: center"><story/></div>'
    })
  ]
}

const Template = (args, { argTypes, events }) => ({
  components: { AppPagination },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template:
    '<AppPagination v-bind="args" v-model="args.modelValue" v-on="events" />'
})

export const Pagination = Template.bind({})

Pagination.args = {
  modelValue: 1,
  total: 50
}
