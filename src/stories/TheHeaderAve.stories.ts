import TheHeaderAveVue from '@/components//TheHeaderAve.vue'

import { Meta, StoryFn } from '@storybook/vue3'

export default {
  title: 'Components/TheHeaderAveVue',
  component: TheHeaderAveVue
} as Meta<typeof TheHeaderAveVue>

export const HeaderAveVue: StoryFn<typeof TheHeaderAveVue> = () => ({
  components: { TheHeaderAveVue },
  template: '<TheHeaderAveVue />'
})
