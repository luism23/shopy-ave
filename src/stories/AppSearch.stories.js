import AppSearch from '../components/AppSearch.vue'

export default {
  title: 'Components/AppSearch',
  component: AppSearch
}

const Template = (args, { argTypes, events }) => ({
  components: { AppSearch },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template:
    '<AppSearch v-bind="args" v-model="args.modelValue" v-on="events" />'
})

export const SearchDefault = Template.bind({})

SearchDefault.args = {}

export const SearchDisabled = Template.bind({})

SearchDisabled.args = {
  disabled: true
}

export const SearchReadonly = Template.bind({})

SearchReadonly.args = {
  readonly: true
}
