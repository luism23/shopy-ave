import AppModalWelcome from '../components/AppModalWelcome.vue'

export default {
  title: 'Components/AppModalWelcome',
  component: AppModalWelcome
}

const Template = (args, { argTypes, events }) => ({
  components: { AppModalWelcome },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: '<AppModalWelcome v-bind="args" v-model="args.modelValue" />'
})

export const Modal = Template.bind({})

Modal.args = {
  isActive: true,
  title: 'Tenemos un mensaje de bienvenida para ti',
  description: 'Conoce como usar nuestra plataforma',
  footer: 'Para comenzar debes agregar...'
}
