import AppFilter from '../components/AppFilter.vue'

export default {
  title: 'Components/AppFilter',
  component: AppFilter
}

const Template = (args, { argTypes, events }) => ({
  components: { AppFilter },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: '<AppFilter v-bind="args" v-on="events"/>'
})

export const FilterDefault = Template.bind({})

FilterDefault.args = {
  label: 'Choose',
  data: [
    {
      id: 1,
      name: 'Odin',
      status: false
    },
    {
      id: 2,
      name: 'Hela',
      status: false
    }
  ]
}

export const FilterWithOptionText = Template.bind({})

FilterWithOptionText.args = {
  label: 'Choose',
  optionText: 'text',
  data: [
    {
      id: 1,
      text: 'Odin - text',
      status: false
    },
    {
      id: 2,
      text: 'Hela - text',
      status: false
    }
  ]
}
