import AppTable from '../components/AppTable.vue'

export default {
  title: 'Components/AppTable',
  component: AppTable
}

const Template = (args, { argTypes, events }) => ({
  components: { AppTable },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: `<AppTable v-bind="args" v-model="args.modelValue" v-on="events">
      ${args.slotTemplate}
    </AppTable>`
})

export const TableNormal = Template.bind({})

TableNormal.args = {
  columns: [
    {
      field: 'col1',
      label: 'Columna 1',
      align: 'center'
    },
    {
      field: 'col2',
      label: 'Columna 2',
      align: 'center'
    }
  ],
  rows: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    }
  ]
}

export const TableSelect = Template.bind({})
TableSelect.args = {
  selectable: true,
  columns: [
    {
      field: 'col1',
      label: 'Columna 1',
      align: 'center'
    },
    {
      field: 'col2',
      label: 'Columna 2',
      align: 'center'
    }
  ],
  rows: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    }
  ]
}

export const TableTemplate = Template.bind({})
TableTemplate.args = {
  slotTemplate:
    '<template #col1="{row}">{{row.col1}} from template.</template>',
  selectable: true,
  columns: [
    {
      field: 'col1',
      label: 'Columna 1',
      align: 'center'
    },
    {
      field: 'col2',
      label: 'Columna 2',
      align: 'center'
    }
  ],
  rows: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    }
  ]
}

export const TableSelectDefault = Template.bind({})

TableSelectDefault.args = {
  selectable: true,
  columns: [
    {
      field: 'col1',
      label: 'Columna 1',
      align: 'center'
    },
    {
      field: 'col2',
      label: 'Columna 2',
      align: 'center'
    }
  ],
  rows: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    },
    {
      id: '3',
      col1: 'Thor',
      col2: 'Thor'
    }
  ],
  modelValue: ['1', '3']
}

export const TableTitleColumnSM = Template.bind({})

TableTitleColumnSM.args = {
  columns: [
    {
      field: 'col1',
      label: 'Columna 1',
      align: 'center',
      size: 'sm'
    },
    {
      field: 'col2',
      label: 'Columna 2',
      align: 'center',
      size: 'sm'
    }
  ],
  rows: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    },
    {
      id: '3',
      col1: 'Thor',
      col2: 'Thor'
    }
  ]
}

export const TableTitleColumnBASE = Template.bind({})

TableTitleColumnBASE.args = {
  columns: [
    {
      field: 'col1',
      label: 'Columna 1',
      align: 'center',
      size: 'base'
    },
    {
      field: 'col2',
      label: 'Columna 2',
      align: 'center',
      size: 'base'
    }
  ],
  rows: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    },
    {
      id: '3',
      col1: 'Thor',
      col2: 'Thor'
    }
  ]
}

export const TableWithFirstRow = Template.bind({})
TableWithFirstRow.args = {
  slotTemplate:
    `<template #first-row>
      <div class="bg-gray-200 p-4 text-center font-bold">
        this is a first row 
      </div>
    </template>`,
  columns: [
    {
      field: 'col1',
      label: 'Columna 1',
      align: 'center'
    },
    {
      field: 'col2',
      label: 'Columna 2',
      align: 'center'
    }
  ],
  rows: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    }
  ]
}