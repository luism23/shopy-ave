import AppCardProductDropshipping from '../components/AppCardProductDropshipping.vue'

export default {
  title: 'Components/AppCardProductDropshipping',
  component: AppCardProductDropshipping
}

const Template = (args, { argTypes, events }) => ({
  components: { AppCardProductDropshipping },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: '<AppCardProductDropshipping v-bind="args"  v-on="events" />'
})

export const Card = Template.bind({})

Card.args = {
  data: [
    {
      id: '1',
      name: 'lorem',
      status: false,
      description: 'Lorem',
      price: 1000,
      image: 'https://i.pravatar.cc/1000',
      total: 146,
      qualificationTotal: 46,
      discount: true,
      discountPrice: '64.000',
      percentage: '55',
      suggesPrice: '80.000',
      percentageDiscount: '+16.6%',
      gain: '$10.000',
      provider: 'CC camisas'
    }
  ]
}
