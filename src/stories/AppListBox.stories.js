import AppListBox from '../components/AppListBox.vue'

export default {
  title: 'Components/AppListBox',
  component: AppListBox,
  argTypes: {
    type: {
      name: 'type',
      type: { name: 'string', required: false },
      description: 'Types availables',
      defaultValue: 'light',
      table: {
        defaultValue: { summary: 'active' }
      },
      options: ['light', 'selectable', 'multiple'],
      control: {
        type: 'select'
      }
    }
  }
}

const Template = (args, { argTypes, events }) => ({
  components: { AppListBox },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: '<AppListBox v-bind="args" v-on="events" />'
})

export const Light = Template.bind({})

Light.args = {
  type: 'light',
  data: [
    {
      id: 1,
      name: 'Odin'
    },
    {
      id: 2,
      name: 'Hela-1'
    },
    {
      id: 3,
      name: 'Hela-2'
    },
    {
      id: 4,
      name: 'Hela-3'
    },
    {
      id: 5,
      name: 'Hela-4'
    },
    {
      id: 6,
      name: 'Hela-5'
    }
  ]
}

export const LightWithDefaultValues = Template.bind({})

LightWithDefaultValues.args = {
  type: 'light',
  modelValue: {
    id: 1,
    description: 'Odin'
  },
  optionText: 'description',
  data: [
    {
      id: 1,
      description: 'Odin'
    },
    {
      id: 2,
      description: 'Hela'
    }
  ]
}

export const LightWithPlaceholder = Template.bind({})

LightWithPlaceholder.args = {
  type: 'light',
  placeholder: 'Placeholder',
  optionText: 'description',
  data: [
    {
      id: 1,
      description: 'Odin'
    },
    {
      id: 2,
      description: 'Hela'
    }
  ]
}

export const LightWithLabel = Template.bind({})

LightWithLabel.args = {
  ...Light.args,
  label: 'Label *'
}

export const LightDisabledWithIcon = Template.bind({})

LightDisabledWithIcon.args = {
  ...LightWithLabel.args,
  showIconDisabled: true,
  disabled: true
}

export const LightDisabled = Template.bind({})

LightDisabled.args = {
  ...LightWithLabel.args,
  disabled: true
}

export const LightWithError = Template.bind({})

LightWithError.args = {
  ...LightWithLabel.args,
  error: 'Error'
}

export const Multiple = Template.bind({})

Multiple.args = {
  type: 'multiple',
  data: [
    {
      id: 1,
      name: 'Odin',
      status: false,
      hasEvents: true
    },
    {
      id: 2,
      name: 'Hela',
      status: false,
      hasEvents: true
    }
  ]
}

export const Selectable = Template.bind({})

Selectable.args = {
  type: 'selectable',
  data: [
    {
      id: 1,
      name: 'Odin'
    },
    {
      id: 2,
      name: 'Hela'
    }
  ]
}

export const RedirectWithEvents = Template.bind({})

RedirectWithEvents.args = {
  type: 'multiple',
  titleRedirect: 'Odin',
  data: [
    {
      id: 1,
      name: 'Odin',
      status: false,
      hasEvents: true
    },
    {
      id: 2,
      name: 'Hela',
      status: false,
      hasEvents: true
    }
  ]
}
