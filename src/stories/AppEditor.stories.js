import AppEditor from '../components/AppEditor.vue'

export default {
  title: 'Components/AppEditor',
  component: AppEditor
}

const Template = (args, { argTypes, events }) => ({
  components: { AppEditor },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: '<AppEditor v-bind="args" v-on="events"/>'
})

export const Editor = Template.bind({})

Editor.args = {
  outputFormat: 'html',
  isActiveMenuBar: false,
  isInline: false,
  height: 500,
  modelValue: ''
}
