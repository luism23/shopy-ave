import AppAlertBase from '../components/AppAlertBase.vue'

export default {
  title: 'Components/AppAlertBase',
  component: AppAlertBase
}

const Template = (args, { argTypes, events }) => ({
  components: { AppAlertBase },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template:
    '<AppAlertBase v-bind="args" v-model="args.modelValue" v-on="events" />'
})

export const Alert = Template.bind({})

Alert.args = {
  title:
    'Hemos creado tu proyecto en este panel podras ingresar a todos tus emprendimientos o empresas y crear nuevas.',
  subtitle:
    'Para comenzar a explorar nuestra plataforma selecciona el proyecto',
  icon: 'check-circle',
  iconColor: 'text-emerald-300'
}
