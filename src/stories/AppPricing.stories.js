import AppPricing from '../components/AppPricing.vue'

export default {
  title: 'Components/AppPricing',
  component: AppPricing,
  argTypes: {
    modelValue: {
      name: 'modelValue',
      type: { name: 'object', required: false },
      description: 'Value',
      defaultValue: {
        planID: '',
        frequencyID: ''
      },
      table: {
        defaultValue: { summary: { planID: '', frequencyID: '' } }
      },
      control: {
        options: { planID: '', frequencyID: '' },
        type: 'object'
      }
    },
    type: {
      name: 'type',
      type: { name: 'string', required: false },
      description: 'Types pricing UI',
      defaultValue: 'select',
      table: {
        defaultValue: { summary: 'select' }
      },
      options: ['select', 'info', 'selected', 'onboarding'],
      control: {
        type: 'select'
      }
    },
    selectedPlan: {
      name: 'selectedPlan',
      type: { name: 'number', required: false },
      description: 'ID plan',
      defaultValue: 0,
      table: {
        defaultValue: { summary: 'select' }
      },
      options: [1, 5, 4, 0],
      control: {
        type: 'select'
      }
    },
    selectedFrequency: {
      name: 'selectedFrequency',
      type: { name: 'number', required: false },
      description: 'ID frequency plan',
      defaultValue: 0,
      table: {
        defaultValue: { summary: 'select' }
      },
      options: [1, 2, 3, 0],
      control: {
        type: 'select'
      }
    }
  }
}

const Template = (args, { argTypes, events }) => ({
  components: { AppPricing },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template:
    '<AppPricing v-bind="args" v-model="args.modelValue" v-on="events" />'
})

export const Selected = Template.bind({})

Selected.args = {
  type: 'selected',
  selectedPlan: 1,
  selectedFrequency: 2
}

export const Select = Template.bind({})

Select.args = {
  type: 'select',
  selectedPlan: 1,
  selectedFrequency: 2
}

export const Info = Template.bind({})

Info.args = {
  type: 'info'
}

export const OnboardingWithPlanDefault = Template.bind({})

OnboardingWithPlanDefault.args = {
  type: 'onboarding',
  selectedPlan: 4,
  selectedFrequency: 1
}

export const OnboardingSimple = Template.bind({})

OnboardingSimple.args = {
  type: 'onboarding'
}
