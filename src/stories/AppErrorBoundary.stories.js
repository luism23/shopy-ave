import AppErrorBoundary from '../components/AppErrorBoundary.vue'

export default {
  title: 'Components/AppErrorBoundary',
  component: AppErrorBoundary
}

const Template = (args) => ({
  components: { AppErrorBoundary },
  setup() {
    return { args }
  },
  template: '<AppErrorBoundary v-bind="args"  />'
})

export const Error = Template.bind({})

Error.args = {
  title: 'Title',
  message: 'Message'
}
