import TheModal from '../components/TheModal.vue'

export default {
  title: 'Components/TheModal',
  component: TheModal
}

const Template = (args, { argTypes, events }) => ({
  components: { TheModal },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: `<TheModal v-bind="args" v-on="events">${args.slotTemplate}</TheModal>`
})

export const ModalDefault = Template.bind({})

ModalDefault.args = {
  modelValue: true,
  title: 'Hello world',
  icon: 'user',
  width: '400px',
  slotTemplate: '<p>Content</p>'
}

export const ModalWithPositionEndAllScreen = Template.bind({})

ModalWithPositionEndAllScreen.args = {
  ...ModalDefault.args,
  size: {
    position: 'end',
    screen: 'complete'
  }
}

export const ModalWithPositionStartAllScreen = Template.bind({})

ModalWithPositionStartAllScreen.args = {
  ...ModalDefault.args,
  size: {
    position: 'start',
    screen: 'complete'
  }
}

export const ModalWithPositionStartAllScreenMiddle = Template.bind({})

ModalWithPositionStartAllScreenMiddle.args = {
  ...ModalDefault.args,
  size: {
    position: 'start',
    screen: 'middle'
  }
}

export const ModalWithPositionEndAllScreenMiddle = Template.bind({})

ModalWithPositionEndAllScreenMiddle.args = {
  ...ModalDefault.args,
  size: {
    position: 'end',
    screen: 'middle'
  }
}

export const ModalWithPositionCenterAllScreenMiddle = Template.bind({})

ModalWithPositionCenterAllScreenMiddle.args = {
  ...ModalDefault.args,
  size: {
    position: 'center',
    screen: 'middle'
  }
}

export const ModalClear = Template.bind({})

ModalClear.args = {
  modelValue: true,
  width: '400px',
  slotTemplate: '<p>Content</p>'
}

export const ModalWhitoutIcon = Template.bind({})

ModalWhitoutIcon.args = {
  modelValue: true,
  title: 'Hello world',
  width: '400px',
  slotTemplate: '<p>Content</p>'
}

export const ModalWhitoutIconClose = Template.bind({})

ModalWhitoutIconClose.args = {
  modelValue: true,
  width: '400px',
  showIconClose: false,
  isManualClose: true,
  slotTemplate: '<p>Content</p>'
}
