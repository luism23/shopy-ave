import AppBadgeState from '../components/AppBadgeState.vue'

export default {
  title: 'Components/AppBadgeState',
  component: AppBadgeState,
  argTypes: {
    state: {
      name: 'state',
      type: { name: 'string', required: false },
      description: 'States availables',
      defaultValue: 'active',
      table: {
        defaultValue: { summary: 'active' }
      },
      options: ['active', 'checking', 'inactive'],
      control: {
        type: 'select'
      }
    },
    title: {
      name: 'title',
      type: { name: 'string', required: false },
      description: 'Title state optional',
      defaultValue: '',
      table: {
        defaultValue: { summary: '' }
      },
      control: {
        type: 'text'
      }
    }
  }
}

const Template = (args) => ({
  components: { AppBadgeState },
  setup() {
    return { args }
  },
  template: '<AppBadgeState v-bind="args" />'
})

export const Badge = Template.bind({})

Badge.args = {
  state: 'active',
  title: '',
  class: 'w-full w-36 h-6'
}
