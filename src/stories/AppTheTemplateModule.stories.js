import { ref, reactive } from 'vue'

import TheTemplateModule from '../components/TheTemplateModule.vue'
import AppDatePickerRange from '@/components/AppDatePickerRange.vue'
import AppSubMenu from '@/components/AppSubMenu.vue'
import AppHeaderBar from '@/components/AppHeaderBar.vue'

export default {
  title: 'Templates/TheTemplateModule',
  component: TheTemplateModule
}

const Template = (args, { argTypes, events }) => ({
  components: {
    TheTemplateModule,
    AppDatePickerRange,
    AppSubMenu,
    AppHeaderBar
  },
  props: Object.keys(argTypes),
  setup() {
    const tabs = ref({
      idCurrentTab: 'Component1',
      items: [
        {
          id: 'Component1',
          name: 'Tab1'
        },
        {
          id: 'Component2',
          name: 'Tab2'
        }
      ]
    })

    const headersFilter = reactive({
      title: { icon: 'user', description: 'title' },
      options: [
        {
          title: 25,
          subtitle: 'title 1',
          name: 'title 1',
          porcentaje: 100
        },
        {
          title: 17,
          subtitle: 'title 2',
          name: 'title 2',
          porcentaje: 40
        },
        {
          title: 23,
          subtitle: 'title 3',
          name: 'title 3',
          porcentaje: 65
        },
        {
          title: 143,
          subtitle: 'title 4',
          name: 'title 4',
          porcentaje: 45
        }
      ]
    })

    return { args, events, tabs, headersFilter }
  },
  template: `
    <TheTemplateModule v-bind="args" v-on="events">
      <template v-if="${'slotDatepicker' in args}" #datepicker>
        ${args.slotDatepicker}
      </template>

      <template v-if="${'slotHeaderStatus' in args}" #header-status>
        ${args.slotHeaderStatus}
      </template>

      <template v-if="${'slotTabs' in args}" #tabs>
        ${args.slotTabs}
      </template>

      <template v-if="${'slotMain' in args}" #main>
        ${args.slotMain}
      </template>
    </TheTemplateModule>`
})

export const TemplateDefault = Template.bind({})

TemplateDefault.args = {
  title: { icon: 'user', name: 'Im title' },
  slotDatepicker: '<AppDatePickerRange position="right" has-border />',
  slotTabs: '<AppSubMenu v-model="tabs" />',
  slotMain: '<p>Here nodes children</p>'
}

export const TemplateWithHeaderStatus = Template.bind({})

TemplateWithHeaderStatus.args = {
  slotHeaderStatus: '<AppHeaderBar :headers="headersFilter" />',
  slotDatepicker: '<AppDatePickerRange position="right" has-border />',
  slotTabs: '<AppSubMenu v-model="tabs" />',
  slotMain: '<p>Here nodes children</p>'
}

export const TemplateWithoutTabs = Template.bind({})

TemplateWithoutTabs.args = {
  title: { icon: 'user', name: 'Im title' },
  slotDatepicker: '<AppDatePickerRange position="right" has-border />',
  slotMain: '<p>Here nodes children</p>'
}

export const TemplateWithoutTabsAndTitle = Template.bind({})

TemplateWithoutTabsAndTitle.args = {
  slotDatepicker: '<AppDatePickerRange position="right" has-border />',
  slotMain: '<p>Here nodes children</p>'
}

export const TemplateWithoutTabsAndTitleAndDatepicker = Template.bind({})

TemplateWithoutTabsAndTitleAndDatepicker.args = {
  slotMain: '<p>Here nodes children</p>'
}
