import AppTableBase from '../components/AppTableBase.vue'

export default {
  title: 'Components/AppTableBase',
  component: AppTableBase
}

const Template = (args, { argTypes, events }) => ({
  components: { AppTableBase },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: `<AppTableBase v-bind="args" v-model="args.modelValue" v-on="events">
      ${args.slotTemplate}
    </AppTableBase>`
})

export const Table = Template.bind({})

Table.args = {
  headers: [
    {
      name: 'col1',
      title: 'Columna 1',
      align: 'center',
      sorted: true
    },
    {
      name: 'col2',
      title: 'Columna 2',
      align: 'center',
      sorted: true
    }
  ],
  items: [
    {
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      col1: 'Zeus',
      col2: 'Hades'
    }
  ]
}

export const TableSelect = Template.bind({})

TableSelect.args = {
  selectable: true,
  headers: [
    {
      name: 'col1',
      title: 'Columna 1',
      align: 'center',
      sorted: true
    },
    {
      name: 'col2',
      title: 'Columna 2',
      align: 'center',
      sorted: true
    }
  ],
  items: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    },
    {
      id: '3',
      col1: 'Kratos',
      col2: 'Yisus'
    }
  ]
}

export const TableSelectDefault = Template.bind({})

TableSelectDefault.args = {
  modelValue: ['1', '3'],
  selectable: true,
  headers: [
    {
      name: 'col1',
      title: 'Columna 1',
      align: 'center',
      sorted: true
    },
    {
      name: 'col2',
      title: 'Columna 2',
      align: 'center',
      sorted: true
    }
  ],
  items: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    },
    {
      id: '3',
      col1: 'Kratos',
      col2: 'Yisus'
    }
  ]
}

export const TableSorted = Template.bind({})

TableSorted.args = {
  headers: [
    {
      name: 'col1',
      title: 'Columna 1',
      align: 'center',
      sorted: true
    },
    {
      name: 'col2',
      title: 'Columna 2',
      align: 'center',
      sorted: true
    }
  ],
  items: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    },
    {
      id: '3',
      col1: 'Kratos',
      col2: 'Yisus'
    }
  ]
}

export const TableSortByDefault = Template.bind({})

TableSortByDefault.args = {
  sortByDefault: 'col1',
  headers: [
    {
      name: 'col1',
      title: 'Columna 1',
      align: 'center',
      sorted: true
    },
    {
      name: 'col2',
      title: 'Columna 2',
      align: 'center',
      sorted: true
    }
  ],
  items: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    },
    {
      id: '3',
      col1: 'Kratos',
      col2: 'Yisus'
    }
  ]
}

export const TableNotSorted = Template.bind({})

TableNotSorted.args = {
  headers: [
    {
      name: 'col1',
      title: 'Columna 1',
      align: 'center',
      sorted: false
    },
    {
      name: 'col2',
      title: 'Columna 2',
      align: 'center',
      sorted: false
    }
  ],
  items: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    },
    {
      id: '3',
      col1: 'Kratos',
      col2: 'Yisus'
    }
  ]
}

export const TableColumnTemplate = Template.bind({})

TableColumnTemplate.args = {
  slotTemplate: `<template #col1={item}> 
    {{ item.col1 }} from column1 template
  </template>
  <template #col2={item}> 
    {{ item.col2 }} from column2 template
  </template>`,
  headers: [
    {
      name: 'col1',
      title: 'Columna 1',
      align: 'center',
      sorted: false
    },
    {
      name: 'col2',
      title: 'Columna 2',
      align: 'center',
      sorted: false
    }
  ],
  items: [
    {
      id: '1',
      col1: 'Odin',
      col2: 'Hela'
    },
    {
      id: '2',
      col1: 'Zeus',
      col2: 'Hades'
    },
    {
      id: '3',
      col1: 'Kratos',
      col2: 'Yisus'
    }
  ]
}
