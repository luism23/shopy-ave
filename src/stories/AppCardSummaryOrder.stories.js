import AppCardSummaryOrder from '../components/AppCardSummaryOrder.vue'

export default {
  title: 'Components/AppCardSummaryOrder',
  component: AppCardSummaryOrder,
  decorators: [
    () => ({
      template:
        '<div style="display: flex; justify-content: center"><story/></div>'
    })
  ],
  argTypes: {
    type: {
      name: 'type',
      type: { name: 'string', required: false },
      description: 'Types UI summary card',
      defaultValue: 'select',
      table: {
        defaultValue: { type: 'summary' }
      },
      options: ['summary', 'sale', 'quote'],
      control: {
        type: 'select'
      }
    }
  }
}

const Template = (args, { argTypes, events }) => ({
  components: { AppCardSummaryOrder },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: `<AppCardSummaryOrder v-bind="args" v-model="args.modelValue" v-on="events" >
    ${args.slotTemplate}
  </AppCardSummaryOrder>
  `
})

const mock = {
  cityDestiny: 'Medellín - Bogotá',
  guide: 1,
  idOperator: '1010',
  products: [
    {
      id: 'x',
      name: 'camisa',
      selectedUnits: 1,
      total: 40000,
      attributes: {
        attribute1: { Color: 'rojo' },
        attribute2: { Talla: 'M' },
        attribute3: { Test: 'test' }
      }
    }
  ],
  dispatchCenter: {
    name: 'Sede belen',
    address: 'Calle 123'
  },
  order: {
    daysShippingTime: 3,
    totalOrder: 10000,
    paymentCost: 5000,
    subtotal: 15000,
    IVA: 0,
    totalQuote: 145000
  }
}

export const CardTypeSummary = Template.bind({})

CardTypeSummary.decorators = [
  () => ({
    template: '<div style="width: 385px"><story/></div>'
  })
]

CardTypeSummary.args = { type: 'summary', ...mock }

export const CardTypeSale = Template.bind({})

CardTypeSale.decorators = [
  () => ({
    template: '<div style="width: 600px"><story/></div>'
  })
]

CardTypeSale.args = { type: 'sale', ...mock }

export const CardTypeQuote = Template.bind({})

CardTypeQuote.args = {
  type: 'quote',
  ...mock,
  slotTemplate: '<template #quote>All data quote...</template>'
}
