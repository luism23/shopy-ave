import AppHeaderBar from '../components/AppHeaderBar.vue'

export default {
  title: 'Components/AppHeaderBar',
  component: AppHeaderBar
}

const Template = (args, { argTypes, events }) => ({
  components: { AppHeaderBar },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template: '<AppHeaderBar v-bind="args" v-on="events"/>'
})

export const HeaderBar = Template.bind({})

HeaderBar.args = {
  headers: {
    id: 1,
    title: {
      icon: 'truck',
      description: 'A title'
    },
    options: [
      {
        id: 1,
        title: 'one',
        percentage: '40'
      },
      {
        id: 2,
        title: 'two',
        percentage: '10'
      },
      {
        id: 3,
        title: 'three',
        percentage: '80'
      }
    ]
  }
}
