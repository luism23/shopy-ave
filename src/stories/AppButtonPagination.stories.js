import AppButtonPagination from '../components/AppButtonPagination.vue'

export default {
  title: 'Components/AppButtonPagination',
  component: AppButtonPagination,
  decorators: [
    () => ({
      template:
        '<div style="display: flex; justify-content: center"><story/></div>'
    })
  ]
}

const Template = (args, { argTypes, events }) => ({
  components: { AppButtonPagination },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template:
    '<AppButtonPagination v-bind="args" v-model="args.modelValue" v-on="events" />'
})

export const ButtonIconLeft = Template.bind({})

ButtonIconLeft.args = {
  sideIcon: 'left'
}

export const ButtonIconRight = Template.bind({})

ButtonIconRight.args = {
  sideIcon: 'right'
}

export const ButtonDisabled = Template.bind({})

ButtonDisabled.args = {
  disabled: true
}
