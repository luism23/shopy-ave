import AppTextField from '../components/AppTextField.vue'

export default {
  title: 'Components/AppTextField',
  component: AppTextField
}

const Template = (args, { argTypes, events }) => ({
  components: { AppTextField },
  props: Object.keys(argTypes),
  setup() {
    return { args, events }
  },
  template:
    '<AppTextField v-bind="args" v-model="args.modelValue" v-on="events" />'
})

export const Input = Template.bind({})

Input.args = {
  label: 'Label',
  name: 'label'
}

export const InputPassword = Template.bind({})

InputPassword.args = {
  ...Input.args,
  type: 'password'
}

export const InputError = Template.bind({})

InputError.args = {
  ...Input.args,
  error: 'A message error'
}

export const InputFile = Template.bind({})

InputFile.args = {
  ...Input.args,
  type: 'file',
  file: {
    accept: 'image/jpeg, image/jpg, application/pdf',
    themeTooltip: 'Primary',
    titleTooltip: 'Formato del archivo',
    messageTooltip: 'Formatos permitidos PDF (3 MB), JPEG (5 MB)'
  }
}

export const InputFileError = Template.bind({})

InputFileError.args = {
  ...InputFile.args,
  error: 'A message error'
}

export const InputIconLeft = Template.bind({})

InputIconLeft.args = {
  ...Input.args,
  icon: {
    name: 'search',
    position: 'left'
  }
}

export const InputIconRight = Template.bind({})

InputIconRight.args = {
  ...Input.args,
  icon: {
    name: 'user',
    position: 'right'
  }
}
