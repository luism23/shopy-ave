import { test, describe } from 'vitest'
import { render } from '@testing-library/vue'

import AppAlertBase from '@/components/AppAlertBase.vue'

describe('AppAlertBase.vue', () => {
  test('Should be render title with subtitle', () => {
    const { getByText } = render(AppAlertBase, {
      props: { title: 'Im title', subtitle: 'Im subtitle people' }
    })

    getByText('Im title')
    getByText('Im subtitle people')
  })
})
