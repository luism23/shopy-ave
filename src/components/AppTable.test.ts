import { describe, expect, it } from 'vitest'
import { render, fireEvent } from '@testing-library/vue'
import AppTable from '@/components/AppTable.vue'

const columns = [
  {
    label: 'Name',
    field: 'name',
    sortable: true
  },
  {
    label: 'Age',
    field: 'age',
    sortable: true,
    width: '200px'
  },
  {
    label: 'City',
    field: 'city',
    sortable: true
  }
]

const rows = [
  {
    id: '1',
    name: 'John',
    age: '30',
    city: 'New York'
  },
  {
    id: '2',
    name: 'Mary',
    age: '25',
    city: 'Paris'
  }
]

describe('Render Table', () => {
  it('Render Rows No Selectable', () => {
    const { getByText, getByTestId } = render(AppTable, {
      props: {
        columns: columns,
        rows: rows,
        selectable: false
      }
    })
    getByText('John')
    getByText('Mary')
    getByText('New York')
    getByText('Paris')
    getByText('30')
    getByText('25')
    getByText('Name')
    getByText('Age')
    getByText('City')
    expect(() => getByTestId('select-all')).toThrowError()
  })

  it('Render Rows  Selectable', () => {
    const { getByText, getByTestId } = render(AppTable, {
      props: {
        columns: columns,
        rows: rows,
        selectable: true
      }
    })
    getByText('John')
    getByText('Mary')
    getByText('New York')
    getByText('Paris')
    getByText('30')
    getByText('25')
    getByText('Name')
    getByText('Age')
    getByText('City')
    const selectedAll = getByTestId('select-all') as HTMLInputElement
    expect(selectedAll.checked).toBeFalsy()
  })

  it('Selected All', async () => {
    const { getByTestId, getByDisplayValue } = render(AppTable, {
      props: {
        columns: columns,
        rows: rows,
        selectable: true
      }
    })
    const selectedAll = getByTestId('select-all') as HTMLInputElement
    expect(selectedAll.checked).toBeFalsy()
    await fireEvent.change(selectedAll, { target: { checked: true } })
    expect(selectedAll.checked).toBeTruthy()
    const checkValue = getByDisplayValue('1') as HTMLInputElement
    expect(checkValue.checked).toBeTruthy()
    await fireEvent.change(selectedAll, { target: { checked: false } })
    const checkValue2 = getByDisplayValue('2') as HTMLInputElement
    expect(checkValue2.checked).toBeFalsy()
  })

  it('Selected Row', async () => {
    const { getByDisplayValue } = render(AppTable, {
      props: {
        columns: columns,
        rows: rows,
        selectable: true
      }
    })
    const checkValue = getByDisplayValue('1') as HTMLInputElement
    await fireEvent.change(checkValue, { target: { checked: true } })
    expect(checkValue.checked).toBeTruthy()
  })

  it('Render default Values', async () => {
    const { getByDisplayValue } = render(AppTable, {
      props: {
        columns: columns,
        rows: rows,
        selectable: true,
        modelValue: ['1']
      }
    })

    const checkValue = getByDisplayValue('1') as HTMLInputElement
    expect(checkValue.checked).toBeTruthy()
  })

  it('Render Slots', async () => {
    const { getAllByText } = render(AppTable, {
      props: {
        columns: columns,
        rows: rows,
        selectable: true
      },
      slots: {
        name: '<div>Test</div>'
      }
    })
    getAllByText('Test')
  })

  it('Select All One to One', async () => {
    const { getByTestId, getByDisplayValue } = render(AppTable, {
      props: {
        columns: columns,
        rows: rows,
        selectable: true
      }
    })
    const checkOne = getByDisplayValue('1') as HTMLInputElement
    await fireEvent.change(checkOne, { target: { checked: true } })
    const checkTwo = getByDisplayValue('2') as HTMLInputElement
    await fireEvent.change(checkTwo, { target: { checked: true } })
    const selectedAll = getByTestId('select-all') as HTMLInputElement
    expect(selectedAll.checked).toBeTruthy()
  })

  it('Render Sorted table', async () => {
    const { getByText, getByDisplayValue } = render(AppTable, {
      props: {
        columns: [
          {
            label: 'ID',
            field: 'id',
            sortable: true
          },
          ...columns
        ],
        rows: rows,
        selectable: true
      }
    })

    getByText('ID')
    await fireEvent.click(getByText('ID'))
    getByDisplayValue('1')
  })
})
