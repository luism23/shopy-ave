import { defineStore } from 'pinia'
const navClassName = {
  select: 'w-6/12 text-red-500 sansBold border-b-4 border-b-red-500',
  noSelect: 'w-6/12 text-gray-600 sansBold'
}

const useNovedadesStore = defineStore('useNovedadesStore', {
  state: () => ({
    mode: <string>'NOVEDADESFIRST',
    classesNav: {
      mode: <string>navClassName.select,
      noentrega: <string>navClassName.noSelect,
      sinnovedad: <string>navClassName.noSelect,
      clientesnovedades: <string>navClassName.noSelect,
      novedadesa: <string>navClassName.noSelect
    }
  }),
  getters: {
    getMode(state) {
      return state.mode
    },
    getClasses(state) {
      return state.classesNav
    }
  },
  actions: {
    changeMode(mode: string) {
      this.mode = mode
    },
    resetClass() {
      this.classesNav.mode = navClassName.select
      this.classesNav.clientesnovedades = navClassName.noSelect
      this.classesNav.noentrega = navClassName.noSelect
      this.classesNav.novedadesa = navClassName.noSelect
      this.classesNav.sinnovedad = navClassName.noSelect
    },

    noEntrega() {
      this.classesNav.noentrega = navClassName.select
    },
    clientesNovedades() {
      this.classesNav.clientesnovedades = navClassName.select
    },
    novedadesa() {
      this.classesNav.novedadesa = navClassName.select
    },
    sinNovedad() {
      this.classesNav.sinnovedad = navClassName.select
    },
    novedadesFirst() {
      this.classesNav.mode = navClassName.select
    }
  }
})
export { useNovedadesStore }
