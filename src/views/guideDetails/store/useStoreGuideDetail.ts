import { defineStore } from 'pinia'
import { ref } from 'vue'
import {
  IDataGuideDetails,
  IDataGuideInfoGeneral,
  IDataSaveShippingNews,
  IDataShippingIncident,
  IDataShippingNews,
  IDataShippingNewsHistory
} from '../models/guideDetailsAve'
export const useAppGuideDetails = defineStore('appDescuento', {
  state: () => ({
    dataGuideInfo: <IDataGuideInfoGeneral[]>[],
    dataGuideHistory: <IDataGuideDetails[]>[],
    dataGuideShippingNews: <IDataShippingNews[]>[],
    dataGuideShippingNewsInt: <IDataShippingNewsHistory[]>[],
    dataShippingIncident: <IDataShippingIncident[]>[],
    saveDataShippingNews: <IDataSaveShippingNews[]>[],
    guiProv: '533269135',
    dsconsec: ref<string>('')
  }),
  getters: {
    getGuideInfo(state) {
      return state.dataGuideInfo[0]
    },
    getDataShippingNews(state) {
      return state.saveDataShippingNews
    }
  },
  actions: {
    SetGuideInfo(dataConfDescuento: IDataGuideInfoGeneral[]) {
      this.dataGuideInfo = dataConfDescuento
    }
  }
  // GetListGuideHistory
})
