import http from '@/services/httpAve'
import { IDataSaveShippingNews } from '../models/guideDetailsAve'
/**
 * Solo para usar en DEV
 * @returns
 */

function SaveShippingNews(data: IDataSaveShippingNews) {
  const route = ''
  const body = {
    tipo: 'GuardarNovedad',
    ...data
  }
  return http().post(route, body)
}

function GetListShippingNews(guia: string) {
  const route = '/comunes/v2.0/novedades.php'
  const body = {
    tipo: 'historicoNovedades',
    guia
  }
  return http().post(route, body)
}

function GetListGuideHistory(guia: string) {
  const route = '/comunes/v2.0/guiasNacional.php'
  const body = {
    tipo: 'historialEstado',
    guia
  }
  return http().post(route, body)
}

function GetListGuideInfo(guia: string) {
  const route = '/comunes/v2.0/guiasNacional.php'
  const body = {
    tipo: 'infoGuia',
    guia
  }
  return http().post(route, body)
}

function GetListShippingNewsHistory(guia: string) {
  const route = '/comunes/v2.0/novedades.php'
  const body = {
    tipo: 'NovedadesGuiaInternos',
    guia
  }
  return http().post(route, body)
}

function GetListShippingNewsIncident(idexp: number) {
  const route = '/comunes/v2.0/novedades.php'
  const body = {
    tipo: 'novedadesOperativas',
    idexp
  }
  return http().post(route, body)
}

export {
  GetListShippingNews,
  GetListGuideHistory,
  GetListGuideInfo,
  GetListShippingNewsHistory,
  SaveShippingNews,
  GetListShippingNewsIncident
}
