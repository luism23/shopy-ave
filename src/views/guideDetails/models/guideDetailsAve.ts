export interface IDataShippingIncident {
  pkid: number
  dsconsec: string
  idtransportador: string
  idestado: string
  dsfecha: string
  dsestado: string
  dsfechalarga: string
  idexp: number
  idagente: number
  dsciudad: string
  dsciudadd: string
  dsnombred: string
  dsdird: string
  dsteld: string
  dstipotrayecto: string
  asesor: string
  // eslint-disable-next-line camelcase
  dias_transcurridos: number
  // eslint-disable-next-line camelcase
  Des_Novedad: string
  dsaclaracion: string
  dscomentario: string
  // eslint-disable-next-line camelcase
  fecha_novedad: string
  // eslint-disable-next-line camelcase
  cod_novedad: number
  dstransportadora: string
  // eslint-disable-next-line camelcase
  total_novedades: number
  ultimoEstado: {
    // eslint-disable-next-line camelcase
    Responsable_Solucion: string
    dsfecharegistro: string
  }
}

export interface IDataSaveShippingNews {
  observaciones: string
  tiposolucion: number
  guia: string
  celular: string
  ciudad: string
  direccion: string
  referencia: string
  devolucion: string
}

export interface IDataShippingNewsHistory {
  pkid: number
  dsconsec: string
  idestado: string
  dsfecha: string
  dsestado: string
  dsfechalarga: string
  idexp: number
  idagente: number
  dsciudad: string
  dsciudadd: string
  dsnombred: string
  dsdird: string
  dsteld: string
  empresa: string
  dstipotrayecto: string
  asesor: string
  dsaclaracion: string
  dscomentario: string
  // eslint-disable-next-line camelcase
  fecha_novedad: string
  // eslint-disable-next-line camelcase
  cod_novedad: number
  dstransportadora: string
  // eslint-disable-next-line camelcase
  total_novedades: number
}
export interface IDataShippingNews {
  dsguia: number
  dsfecharegistro: string
  idtransportadora?: string
  dsaclaracion?: string
  // eslint-disable-next-line camelcase
  dscomentario?: string
  // eslint-disable-next-line camelcase
  Cod_Novedad?: number
  idnovedad?: number
  dsfecha?: string
  // eslint-disable-next-line camelcase
  Des_Novedad?: string
  dsdescripcion?: string
}

export interface IDataGuideDetails {
  id: number
  dsguia: string
  dsestado: string
  dsdescripcion: string
  dsaclaracion?: string
  dscomentario?: string
  dsfecha?: string
  dsfecharegistro: string
  // eslint-disable-next-line camelcase
  Cod_Novedad?: number
  // eslint-disable-next-line camelcase
  Des_Novedad?: string
  idtransportadora: number
  dstransportadora: string
  idguia: number
}

export interface IDataGuideInfoGeneral {
  pkid: number
  idconsec: number
  dsconsec: string
  idservicio: number
  dstransportadora: string
  dsnombre: string
  dsnit: string
  idexp: number
  dsdir: string
  dsciudad: string
  dspais: string
  dstel: string
  dstel2?: string
  dsnombred: string
  dsnitd: string
  dsdird: string
  dsbarriod: string
  dsciudadd: string
  dspaisd: string
  dsteld: string
  dstel2d: string
  dscorreod: string
  dstotalkilos: string
  dstotalkilosreales: string
  largo: string
  ancho: string
  alto: string
  pesovol: number
  idtotaluni: string
  dsvalor: string
  dsvaloracion: string
  dssegurov: string
  dsvalorseguro: string
  dstotal: string
  dscom: string
  dsfechalarga: string
  dsfecha: string
  idtransportador: string
  dsestado: string
  dsfactor: number
  dstipotrayecto: string
  dskilosliquidados: string
  IMGRotulos: string
  IMGRelacionEnvio: string
  dsbolsa: string
  dscontraentrega: string
  dsestadobox: string
  dscostorecaudo: string
  codigoserviciorecaudo: string
  serviciorecaudo: string
  valortransrecaudo: string
  dsvalorrecaudo: string
  idasumecosto: number
  // eslint-disable-next-line camelcase
  dsnumfacturaave_c?: string
  // eslint-disable-next-line camelcase
  idfactura_c?: string
  // eslint-disable-next-line camelcase
  dsfechafactura_c?: string
  // eslint-disable-next-line camelcase
  dsfechalargafactura_c?: string
  // eslint-disable-next-line camelcase
  dsprefijofactura_c?: string
  dscostomanejo: string
  dsobscostomanejo?: string
  dsvalorfijo?: string
  dsimgtrans: string
  // eslint-disable-next-line camelcase
  idcodigo_envio: string
  dsnumfactura: string
  // eslint-disable-next-line camelcase
  idvalortotalguia_base: string
  // eslint-disable-next-line camelcase
  idotros_costos?: string
  // eslint-disable-next-line camelcase
  dsprefijo_guias: string
  idselecservicionacio: number
  costomanejo?: string
  valorflete?: string
  // eslint-disable-next-line camelcase
  idnumero_pedido?: string
  // eslint-disable-next-line camelcase
  dsvalor_pedido: string
  // eslint-disable-next-line camelcase
  dsrelacion_envio?: string
  dsnotacredito?: string
  dsres?: string
  flete: string
  dsimg1?: string
  dsimg2?: string
  asesor?: string
}
