export interface IDataInputRowTable {
  Fecha?: string
  NúmeroDeDocumento?: string
  ValorDeLaTransacción?: string
  IdentificaciónPagador?: string
  Observaciones?: string
  ConsecutivoExtracto?: string
  TipoDeDocumento?: string
}

export interface ItypeDocumentSelec {
  tipo: number
  ds: string
}
export interface ISelect {
  value: string | number
  label: string
}
