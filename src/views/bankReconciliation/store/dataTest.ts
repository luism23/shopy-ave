import { IDataInputRowTable, ItypeDocumentSelec } from '../models/conciliation'

const Dataprueba: IDataInputRowTable[] = [
  {
    Fecha: '30/03/2022',
    ValorDeLaTransacción: '30000',
    IdentificaciónPagador: 'CC 09320129310',
    Observaciones: 'Consignación bancaria',
    ConsecutivoExtracto: '242323423',
    TipoDeDocumento: '1',
    NúmeroDeDocumento: '023910239'
  },
  {
    Fecha: '30/04/2002',
    ValorDeLaTransacción: '30000',
    IdentificaciónPagador: 'CC 09320129310',
    Observaciones: 'Consignación bancaria',
    ConsecutivoExtracto: '242323423',
    TipoDeDocumento: '2',
    NúmeroDeDocumento: '023910238'
  },
  {
    Fecha: '30/03/2011',
    ValorDeLaTransacción: '30000',
    IdentificaciónPagador: 'CC 09320129310',
    Observaciones: 'Consignación bancaria',
    ConsecutivoExtracto: '242323423',
    TipoDeDocumento: '1',
    NúmeroDeDocumento: '023910237'
  }
]

const tipoDocumento: ItypeDocumentSelec[] = [
  {
    tipo: 1,
    ds: 'NU'
  },
  {
    tipo: 2,
    ds: 'NC'
  },
  {
    tipo: 3,
    ds: 'TI'
  }
]

export { Dataprueba, tipoDocumento }
