import http from '@/services/httpAve'
import { IEditDescuentos, IguadarDescuentos } from '../models/descuentoAve'

function GetListDiscounts() {
  const route = '/comunes/v2.0/descuentos.php'
  const body = {
    tipo: 'ListarDescuento'
  }
  return http().post(route, body)
}

function SaveDiscount(data: IguadarDescuentos) {
  const route = '/comunes/v2.0/descuentos.php'
  const body = {
    tipo: 'CrearDescuento',
    ...data
  }
  return http().post(route, body)
}

function EditDiscount(data: IEditDescuentos) {
  const route = '/comunes/v2.0/descuentos.php'
  const body = {
    tipo: 'ActualizarDescuento',
    data: { ...data }
  }
  return http().post(route, body)
}

function GetListCampains() {
  const route = '/comunes/v2.0/campanasCanales.php'
  const body = {
    tipo: 'ListarCampana'
  }
  return http().post(route, body)
}

function GetListCanales() {
  const route = '/comunes/v2.0/campanasCanales.php'
  const body = {
    tipo: 'ListarCanal'
  }
  return http().post(route, body)
}

export {
  GetListDiscounts,
  SaveDiscount,
  EditDiscount,
  GetListCampains,
  GetListCanales
}
