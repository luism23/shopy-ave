/* eslint-disable camelcase */
export interface IdataFlete {
  flete_fact_desde: number
  flete_fact_hasta: number
  flete_porcentaje_descuento: number
  flete_valor_descuento: number
}

export interface IdataRecuado {
  recaudo_total_desde: number
  recaudo_total_hasta: number
  recaudo_porcentaje_descuento: number
  recaudo_valor_descuento: number
}

export interface IDescuentos {
  id: number
  fecha: string
  estado: string
  nombre: string
  clientes: string
  numero_campanas: string
  flete_fecha_inicio?: string
  flete_fecha_final?: string
  data_flete?: IdataFlete[]
  recaudo_fecha_inicio?: string
  recaudo_fecha_final?: string
  data_recaudo?: IdataRecuado[]
}

export interface IdataGenerica {
  total_desde: string
  total_hasta: string
  porcentaje_descuento: string
  valor_descuento: string
}

export interface IguadarDescuentos {
  nombre: string
  flete_fecha_inicio?: string
  flete_fecha_final?: string
  data_flete?: IdataFlete[]
  recaudo_fecha_inicio?: string
  recaudo_fecha_final?: string
  data_recaudo?: IdataRecuado[]
}

export interface IEditDescuentos extends IguadarDescuentos {
  id?: string
}

export interface IRespGeneAveData {
  message: string
  data: IDescuentos[]
  status: 'ok' | 'error'
}

export type Modes = 'Created' | 'Edit' | 'Load'
export type TypeIconOrder = 'chevron-down' | 'chevron-up'

export interface InfoFecha {
  init: string
  final: string
}

export interface IdataCampana {
  ds_canal: string
  ds_servicio: string
  fecha: string
  id: number
  idactivo: number
  idcanal: number
  idservicio: number
  inversion: number
  nombre: string
  roi: number
  total_clientes: number
}

export interface IRespGeneCamp {
  message: string
  data: IdataCampana[]
  status: 'ok' | 'error'
}

export interface IdataCanales {
  id: number
  ds: string
  idactivo: string
  fecha: string
}

export interface IRespGeneCanal {
  message: string
  data: IdataCanales[]
  status: 'ok' | 'error'
}

export interface ICampana {
  value: string | number
  label: string
}
