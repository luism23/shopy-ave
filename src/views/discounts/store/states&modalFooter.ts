import { defineStore } from 'pinia'
type ChangeToogle = '' | 'false' | 'true'
export interface DateInputs {
  final: string
  init: string
}
export const useStatesModalStore = defineStore('statesModal', {
  state: () => ({
    toggleWalletRecharge: <boolean>false,
    toggleMoneyCollected: <boolean>false,
    toggleInvoices: <boolean>false,
    toggleCompensation: <boolean>false,
    toggleAdvances: <boolean>false,
    toggleCrediNotes: <boolean>false,
    toggleWithDrawal: <boolean>false,
    dateSearch: <DateInputs>{}
  }),
  getters: {
    GetDates(state) {
      return state.dateSearch
    },
    WalletRecharge(state) {
      return state.toggleWalletRecharge
    },
    MoneyCollected(state) {
      return state.toggleMoneyCollected
    },
    Invoices(state) {
      return state.toggleInvoices
    },
    Compensation(state) {
      return state.toggleCompensation
    },
    Advances(state) {
      return state.toggleAdvances
    },
    CrediNotes(state) {
      return state.toggleCrediNotes
    },
    WithDrawal(state) {
      return state.toggleWithDrawal
    }
  },
  actions: {
    SaveDates(date: DateInputs) {
      this.dateSearch = date
    },
    ToggleWalletRecharge(str: ChangeToogle = '') {
      if (str) {
        this.toggleWalletRecharge = Boolean(str)
        return
      }
      this.toggleWalletRecharge = !this.toggleWalletRecharge
    },
    ToggleMoneyCollected(str: ChangeToogle = '') {
      if (str) {
        this.toggleMoneyCollected = Boolean(str)
        return
      }
      this.toggleMoneyCollected = !this.toggleMoneyCollected
    },
    ToggleInvoices(str: ChangeToogle = '') {
      if (str) {
        this.toggleInvoices = Boolean(str)
        return
      }
      this.toggleInvoices = !this.toggleInvoices
    },
    ToggleCompensation(str: ChangeToogle = '') {
      if (str) {
        this.toggleCompensation = Boolean(str)
        return
      }
      this.toggleCompensation = !this.toggleCompensation
    },
    ToggleAdvances(str: ChangeToogle = '') {
      if (str) {
        this.toggleAdvances = Boolean(str)
        return
      }
      this.toggleAdvances = !this.toggleAdvances
    },
    ToggleCrediNotes(str: ChangeToogle = '') {
      if (str) {
        this.toggleCrediNotes = Boolean(str)
        return
      }
      this.toggleCrediNotes = !this.toggleCrediNotes
    },
    ToggleWithDrawal(str: ChangeToogle = '') {
      if (str) {
        this.toggleWithDrawal = Boolean(str)
        return
      }
      this.toggleWithDrawal = !this.toggleWithDrawal
    }
  }
})
