import { defineStore } from 'pinia'
import {
  IDescuentos,
  Modes,
  IdataFlete,
  IdataRecuado
} from '../models/descuentoAve'

export const useAppDescuento = defineStore('appDescuento', {
  state: () => ({
    mode: <string>'',
    allDataDescuento: <IDescuentos[]>[],
    editConfDescuento: <IDescuentos | null>{},
    dataFlete: <IdataFlete[]>[],
    dataRecuado: <IdataRecuado[]>[],
    validationInput: {
      errorInput: false,
      errorDate: false,
      dataInput: false,
      dataDate: false
    }
  }),

  getters: {
    getMode(state) {
      return state.mode
    },
    getDataConfDescuento(state) {
      return state.allDataDescuento
    },
    getDataEdit(state) {
      return state.editConfDescuento
    },
    getDataFlete(state) {
      return state.dataFlete
    },
    getDataRecuado(state) {
      return state.dataRecuado
    }
  },
  actions: {
    changeMode(mode: Modes) {
      this.mode = mode
    },
    insertDataDescuento(dataConfDescuento: IDescuentos[]) {
      this.allDataDescuento = dataConfDescuento
    },
    changeDataEdit(editConfDescuento: IDescuentos) {
      this.editConfDescuento = editConfDescuento
    },
    resetDataEdit() {
      this.editConfDescuento = null
    },
    resetDataFlete() {
      this.dataFlete = []
    },
    deleteDataFlete() {
      this.dataFlete.pop()
    },
    addDataFlete(dataFlete: IdataFlete) {
      this.dataFlete.push(dataFlete)
    },

    saveDataFlete(data: IdataFlete, index: number) {
      this.dataFlete[index].flete_fact_desde = data.flete_fact_desde
      this.dataFlete[index].flete_fact_hasta = data.flete_fact_hasta
      this.dataFlete[index].flete_porcentaje_descuento =
        data.flete_porcentaje_descuento
      this.dataFlete[index].flete_valor_descuento = data.flete_valor_descuento
    },
    resetDataRecuado() {
      this.dataRecuado = []
    },
    deleteDataRecuado() {
      this.dataRecuado.pop()
    },
    addDataRecuado(dataRecuado: IdataRecuado) {
      this.dataRecuado.push(dataRecuado)
    },
    saveDataRecuado(data: IdataRecuado, index: number) {
      this.dataRecuado[index].recaudo_total_desde = data.recaudo_total_desde
      this.dataRecuado[index].recaudo_total_hasta = data.recaudo_total_hasta
      this.dataRecuado[index].recaudo_porcentaje_descuento =
        data.recaudo_porcentaje_descuento
      this.dataRecuado[index].recaudo_valor_descuento =
        data.recaudo_valor_descuento
    }
  }
})
