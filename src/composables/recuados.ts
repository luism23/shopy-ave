/* eslint-disable camelcase */
import { IResponseAllDataRecaudos } from '@/constants'
import { useRecuadosStore } from '@/store'

const recudosStore = useRecuadosStore()

export const mutationDataStore = (dataStore: IResponseAllDataRecaudos) => {
  const {
    totales,
    liquidadas,
    arecaudar,
    devolucion,
    indemnizado,
    proceso_indemnizacion,
    recaudadas,
    sin_movimiento
  } = dataStore

  recudosStore.setterValues('liquidadas').mutation(totales.liquidadas)
  recudosStore.setterValues('arecaudar').mutation(totales.arecaudar)
  recudosStore.setterValues('devoluciones').mutation(totales.devoluciones)
  recudosStore.setterValues('indenizaciones').mutation(totales.indemnizaciones)
  recudosStore
    .setterValues('procesoindem')
    .mutation(totales.procesoindemnizacion)
  recudosStore.setterValues('recaudadas').mutation(totales.recaudadas)

  recudosStore.setterData('arecaudar').mutation(arecaudar)
  recudosStore.setterData('devoluciones').mutation(devolucion)
  recudosStore.setterData('indenizaciones').mutation(indemnizado)
  recudosStore.setterData('liquidadas').mutation(liquidadas)
  recudosStore.setterData('procesoindem').mutation(proceso_indemnizacion)
  recudosStore.setterData('recaudadas').mutation(recaudadas)
  recudosStore.setterData('sin_movimientos').mutation(sin_movimiento)
}
