import { ref } from 'vue'

function useShow() {
  const isOpen = ref<boolean>(false)

  const onChangeStatus = () => {
    isOpen.value = !isOpen.value
  }

  return { isOpen, onChangeStatus }
}

export { useShow }
