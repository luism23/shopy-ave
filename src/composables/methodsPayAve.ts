import {
  Base64Req,
  IGetRefWompyReq,
  IPostNotificarPago,
  TypeIcon
} from '@/constants/TypesGenerales'
import { PayToWompy } from '@/services/h2hPayToWompyAve'
import { PostNotificacionPago } from '@/services/PostNotificacionPagoAve'
import { useUsuarioStore } from '@/store'
import { useFacturasStore } from '@/store/invoiceStoreAve'
import { IFacturasData } from '@/types/CollectionsAve'
import { logDev, tokenMaskDimamic } from '@/utils'
import { base64 } from '@/utils/toolsAve'
import {
  goToWompi,
  selectInvoicesOverdue,
  selectInvoicesSelect
} from '@/utils/woompiAve'
import { format } from 'date-fns'
import { computed, onMounted, ref } from 'vue'

interface Modal {
  show: boolean
  icon: TypeIcon
  title: string
  massage: string
}

export const methodsPayAve = () => {
  const stateFacturas = useFacturasStore()
  const stateUser = useUsuarioStore()
  const dataFacturas = computed<IFacturasData[]>(() =>
    stateFacturas.getterFacturasPendientes()
  )
  const facturasSelecionadas = ref<number>(0)
  const dataBase64 = ref<Base64Req>()
  const partialValue = ref<string>('')
  const maskered = computed(() => tokenMaskDimamic(partialValue.value, true))

  const dataForm = ref({
    medioDePago: '',
    observaciones: '',
    valor: 0,
    fecha: format(new Date(), 'yyyy-MM-dd'),
    load: false
  })

  const classForm = ref({
    valor:
      'py-[5px] h-[39px] px-4 rounded-[5px] border w-full border-gray-400 focus:outline-none'
  })

  const loaders = ref({
    totalDeuda: false,
    selecionadas: false,
    vencidas: false,
    parcial: false
  })

  const showModal = ref<Modal>({
    show: false,
    icon: '',
    title: '',
    massage: ''
  })

  const prepareToWompi = async (value: number | string, env: string) => {
    const datos = stateUser.getDatosRemitente[0]
    let factu: string[] = []
    value = Number(String(value).replace(/\./g, ''))
    switch (env) {
      case 'totalDeuda':
        loaders.value.totalDeuda = value > 0 ?? false
        factu = dataFacturas.value.map((el) => {
          return `${el.dsprefijo}-${el.idpedido}`
        })
        break
      case 'parcial':
        loaders.value.parcial = value > 0 ?? false
        factu = dataFacturas.value.map((el) => {
          return `${el.dsprefijo}-${el.idpedido}`
        })
        break
      case 'facturasSelecionadas':
        loaders.value.selecionadas = value > 0 ?? false
        factu = selectInvoicesSelect(dataFacturas.value)
        break
      case 'facturasVencidas':
        loaders.value.vencidas = value > 0 ?? false
        factu = selectInvoicesOverdue(dataFacturas.value)
        break
    }
    const joinInvoices = factu.join()

    if (value > 1 && joinInvoices !== '') {
      const body: IGetRefWompyReq = {
        tipo: 'solicitarReferencia',
        idexp: stateUser.getIdUser,
        id_sector: datos.idsector,
        valor_ave: String(value),
        facturas: factu.join()
      }
      const res = await PayToWompy(body)
      logDev('xxxx', res)
      if (res && res.data) {
        const consec = res.data
        goToWompi(String(value), consec, true)
      }
    }
  }

  const selected = (idFactura: number, value: number) => {
    const element = document.getElementById(
      `check-${idFactura}`
    ) as HTMLInputElement
    if (element) {
      const check = element.checked
      if (check === true) {
        facturasSelecionadas.value = facturasSelecionadas.value + value
      } else if (check === false) {
        facturasSelecionadas.value = facturasSelecionadas.value - value
      }
    }
  }

  const setterAllInvoicesValue = () => {
    dataFacturas.value.forEach((el) => {
      const checkbox = document.getElementById(
        `check-${el.idpedido}`
      ) as HTMLInputElement
      if (checkbox) {
        checkbox.checked = true
        facturasSelecionadas.value = facturasSelecionadas.value + el.saldo
      }
    })
  }

  const handlerBase64 = async (ev: Event) => {
    const target = ev.currentTarget as HTMLInputElement
    if (target && target.files) {
      const archivo = target.files[0]
      const base = await base64(archivo)
      if (base) {
        const base64 = String(base.base).split(',')[1]
        dataBase64.value = {
          type: base.blob.type,
          name: base.blob.name,
          ext: String(base.blob.name).split('.')[1],
          base: base64
        }
      }
    }
  }

  const sendPaymentConfirmation = async () => {
    if (dataForm.value.valor < 1) {
      classForm.value.valor =
        'py-[5px] h-[39px] px-4 rounded-[5px] border w-full border-red-400 focus:outline-none'
    }
    if (dataBase64.value && dataBase64.value.base) {
      const facturas: string[] = []
      dataFacturas.value.forEach((el) => {
        facturas.push(`${el.dsprefijo}-${el.idpedido}`)
      })
      const listFacturas = facturas.join()
      dataForm.value.load = true
      const body: IPostNotificarPago = {
        tipo: 'insertar',
        idexp: Number(stateUser.getIdUser),
        dsvalor: dataForm.value.valor,
        dsfecha_pago: dataForm.value.fecha,
        dsconsec: '',
        dsidentificacion: '',
        idmedio_pago: Number(dataForm.value.medioDePago),
        pkid: '',
        pkids: '',
        dsfacturas: listFacturas,
        guiacourier: '',
        observaciones: dataForm.value.observaciones,
        file: dataBase64.value
      }
      try {
        const resp = await PostNotificacionPago(body)
        const data = resp.data
        if (data.status === 'ok') {
          dataForm.value.load = false
          showModal.value = {
            show: true,
            icon: 'error',
            title: 'Envio exitoso',
            massage: ''
          }
        } else {
          dataForm.value.load = false
          showModal.value = {
            show: true,
            icon: 'error',
            title: 'Algo salió mal',
            massage: data.message || 'Vuelve a intentarlo más tarde'
          }
        }
      } catch (error) {
        dataForm.value.load = false
        showModal.value = {
          show: true,
          icon: 'error',
          title: 'Algo salió mal',
          massage: 'Vuelve a intentarlo más tarde'
        }
      }
    }
  }
  onMounted(() => {
    setterAllInvoicesValue()
  })

  return {
    dataForm,
    classForm,
    loaders,
    showModal,
    dataFacturas,
    prepareToWompi,
    setterAllInvoicesValue,
    sendPaymentConfirmation,
    selected,
    handlerBase64,
    partialValue,
    maskered,
    totalvencido: computed(() => stateFacturas.getterTotales().vencido),
    totalDeuda: computed(() => stateFacturas.getterTotales().subTotal)
  }
}
