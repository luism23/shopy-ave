import { RouteRecordRaw } from 'vue-router'
import { ROUTES } from '@/constants'
import { breadcrumb } from './breadcrumb'

export const referredCustomers: RouteRecordRaw = {
  path: ROUTES.REFERREDDCUSTOMERS.PATH,
  name: ROUTES.REFERREDDCUSTOMERS.NAME,
  component: () =>
    import('@/views/referredCustomers/ViewReferredCustomers.vue'),
  meta: { breadcrumb: [breadcrumb.AccountStatusAve, breadcrumb.referidos] }
}
