import { RouteRecordRaw } from 'vue-router'
import { ROUTES } from '@/constants'
import { breadcrumb } from './breadcrumb'

export const Conciliaciones: RouteRecordRaw = {
  path: ROUTES.CONCILIACIONES.PATH,
  name: ROUTES.CONCILIACIONES.NAME,
  component: () =>
    import('@/views/bankReconciliation/ViewBanckReconciliationAve.vue'),
  meta: { breadcrumb: [breadcrumb.AccountStatusAve, breadcrumb.conciliaciones] }
}
