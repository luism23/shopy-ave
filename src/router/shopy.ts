import { RouteRecordRaw } from 'vue-router'
import { ROUTES } from '@/constants'
import { breadcrumb } from './breadcrumb'

export const shopy: RouteRecordRaw = {
  path: ROUTES.SHOPY.PATH,
  name: ROUTES.SHOPY.NAME,
  component: () => import('@/views/shopy/ViewShopy.vue'),
  meta: { breadcrumb: [breadcrumb.AccountStatusAve, breadcrumb.referidos] }
}
