import { RouteRecordRaw } from 'vue-router'
import { ROUTES } from '@/constants'
import { breadcrumb } from './breadcrumb'

export const AccountStatusAve: RouteRecordRaw = {
  path: ROUTES.ESTADO_CUENTA.PATH,
  name: ROUTES.ESTADO_CUENTA.NAME,
  component: () => import('@/views/accountStatusAve/ViewAccountStatus.vue'),
  meta: { breadcrumb: [breadcrumb.AccountStatusAve] }
}
