import { RouteRecordRaw } from 'vue-router'
import { ROUTES } from '@/constants'
import { breadcrumb } from './breadcrumb'

export const shopyExceed: RouteRecordRaw = {
  path: ROUTES.SHOPY_EXCEED.PATH,
  name: ROUTES.SHOPY_EXCEED.NAME,
  component: () => import('@/views/shopy/ViewShopyOrderExceed.vue'),
  meta: { breadcrumb: [breadcrumb.AccountStatusAve, breadcrumb.referidos] }
}
