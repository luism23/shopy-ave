import { RouteRecordRaw } from 'vue-router'
import { ROUTES } from '@/constants'
import { breadcrumb } from './breadcrumb'

const NovedadesAve: RouteRecordRaw = {
  path: ROUTES.NOVEDADES.PATH,
  name: ROUTES.NOVEDADES.NAME,
  component: () => import('@/views/novedades/ViewNovedadesIntrAve.vue'),
  meta: { breadcrumb: [breadcrumb.AccountStatusAve, breadcrumb.NewsAve] }
}

const NovedadesCarga: RouteRecordRaw = {
  path: ROUTES.NOVEDADESCARGA.PATH,
  name: ROUTES.NOVEDADESCARGA.NAME,
  component: () => import('@/views/novedades/ViewChargueNovedadesAve.vue'),
  meta: {
    breadcrumb: [
      breadcrumb.AccountStatusAve,
      breadcrumb.NewsAve,
      breadcrumb.NewsChargeAve
    ]
  }
}

const NovedadesDocumentoAve: RouteRecordRaw = {
  path: ROUTES.NOVEDADESDOCUMENTO.PATH,
  name: ROUTES.NOVEDADESDOCUMENTO.NAME,
  component: () => import('@/views/novedades/ViewDocsNovaltyAve.vue')
}

export { NovedadesAve, NovedadesCarga, NovedadesDocumentoAve }
