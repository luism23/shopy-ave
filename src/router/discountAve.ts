import { RouteRecordRaw } from 'vue-router'
import { ROUTES } from '@/constants'
import { breadcrumb } from './breadcrumb'

export const DiscountAve: RouteRecordRaw = {
  path: ROUTES.CONFIGDESCUENTOS.PATH,
  name: ROUTES.CONFIGDESCUENTOS.NAME,
  component: () => import('@/views/discounts/ViewConfigDiscountAve.vue'),
  meta: { breadcrumb: [breadcrumb.AccountStatusAve, breadcrumb.DiscountAve] }
}

export const DiscountCRUDAve: RouteRecordRaw = {
  path: ROUTES.CONFIGDESCUENTOSCRUD.PATH,
  name: ROUTES.CONFIGDESCUENTOSCRUD.NAME,
  component: () => import('@/views/discounts/ViewConfigDiscountCRUDAve.vue'),
  meta: {
    breadcrumb: [
      breadcrumb.AccountStatusAve,
      breadcrumb.DiscountAve,
      breadcrumb.DiscountCrudAve
    ]
  }
}
