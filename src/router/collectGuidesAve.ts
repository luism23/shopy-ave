import { RouteRecordRaw } from 'vue-router'
import { ROUTES } from '@/constants'
import { breadcrumb } from './breadcrumb'

export const CollectGuidesAve: RouteRecordRaw = {
  path: ROUTES.RECAUDOS.PATH,
  name: ROUTES.RECAUDOS.NAME,
  component: () => import('@/views/collectGuides/ViewReacudos.vue'),
  meta: { breadcrumb: [breadcrumb.AccountStatusAve, breadcrumb.CollectGuides] }
}
