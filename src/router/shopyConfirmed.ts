import { RouteRecordRaw } from 'vue-router'
import { ROUTES } from '@/constants'
import { breadcrumb } from './breadcrumb'

export const shopyConfirmed: RouteRecordRaw = {
  path: ROUTES.SHOPY_CONFIRMED.PATH,
  name: ROUTES.SHOPY_CONFIRMED.NAME,
  component: () => import('@/views/shopy/ViewShopyCorfirmed.vue'),
  meta: { breadcrumb: [breadcrumb.AccountStatusAve, breadcrumb.referidos] }
}
