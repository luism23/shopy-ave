import { createRouter, createWebHistory, Router } from 'vue-router'
import { routes } from '@/router/routes'
import { globalGuard } from '@/router/globalGuard'

const router: Router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach(globalGuard)

export default router
