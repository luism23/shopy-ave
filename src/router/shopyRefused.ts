import { RouteRecordRaw } from 'vue-router'
import { ROUTES } from '@/constants'
import { breadcrumb } from './breadcrumb'

export const shopyRefused: RouteRecordRaw = {
  path: ROUTES.SHOPY_REFUSED.PATH,
  name: ROUTES.SHOPY_REFUSED.NAME,
  component: () => import('@/views/shopy/ViewShopyRefused.vue'),
  meta: { breadcrumb: [breadcrumb.AccountStatusAve, breadcrumb.referidos] }
}
