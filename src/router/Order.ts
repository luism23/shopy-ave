import { RouteRecordRaw } from 'vue-router'
import { breadcrumb } from './breadcrumb'

const root = '/sales'

const order: RouteRecordRaw[] = [
  {
    path: `${root}/create`,
    name: 'CreateSale',
    component: () => import('@/views/orders/sales/CreateSale.vue'),
    meta: { requiredAuth: true, breadcrumb: [breadcrumb.CreateSale] }
  },
  {
    path: `${root}/my-sales`,
    name: 'MySales',
    component: () => import('@/views/orders/sales/MySales.vue'),
    meta: { requiredAuth: true, breadcrumb: [breadcrumb.MySales] }
  },
  {
    path: `${root}/channel`,
    name: 'Channel',
    component: () => import('@/views/orders/campaignChannel/Channel.vue'),
    meta: { requiredAuth: true, breadcrumb: [breadcrumb.Channel] }
  },
  {
    path: `${root}/price-list`,
    name: 'TabPriceList',
    component: () => import('@/views/orders/priceList/TabPriceList.vue'),
    meta: { requiredAuth: true, breadcrumb: [breadcrumb.TabPriceList] }
  },
  {
    path: `${root}/price-list/create`,
    name: 'CreatePriceList',
    component: () => import('@/views/orders/priceList/CreatePriceList.vue'),
    meta: {
      requiredAuth: false,
      breadcrumb: [breadcrumb.TabPriceList, breadcrumb.CreatePriceList]
    }
  },
  {
    path: `${root}/price-list/update/:id`,
    name: 'UpdatePriceList',
    component: () => import('@/views/orders/priceList/UpdatePriceList.vue'),
    meta: {
      requiredAuth: false,
      breadcrumb: [breadcrumb.TabPriceList, breadcrumb.UpdatePriceList]
    }
  },
  {
    path: `${root}/discount`,
    name: 'Discounts',
    component: () => import('@/views/orders/discount/Discounts.vue'),
    meta: { requiredAuth: false, breadcrumb: [breadcrumb.Discounts] }
  },
  {
    path: `${root}/discount/create`,
    name: 'CreateDiscount',
    component: () => import('@/views/orders/discount/CreateDiscount.vue'),
    meta: {
      requiredAuth: true,
      breadcrumb: [breadcrumb.Discounts, breadcrumb.CreateDiscount]
    }
  },
  {
    path: `${root}/customer`,
    name: 'Customer',
    component: () => import('@/views/orders/customer/Customer.vue'),
    meta: { requiredAuth: true, breadcrumb: [breadcrumb.Customer] }
  },
  {
    path: `${root}/dealer`,
    name: 'Dealer',
    component: () => import('@/views/orders/dealer/Dealer.vue'),
    meta: { requiredAuth: true, breadcrumb: [breadcrumb.Dealer] }
  },
  {
    path: `${root}/dropshipping`,
    name: 'Dropshipping',
    component: () => import('@/views/orders/dropshipping/Dropshipping.vue'),
    meta: { requiredAuth: false, breadcrumb: [breadcrumb.Dropshipping] }
  },
  {
    path: `${root}/suppliers`,
    name: 'Suppliers',
    component: () => import('@/views/orders/suppliers/Suppliers.vue'),
    meta: { requiredAuth: true, breadcrumb: [breadcrumb.Suppliers] }
  },
  {
    path: `${root}/create-customer`,
    name: 'CreatedCustomer',
    component: () => import('@/views/orders/customer/CreatedCustomer.vue'),
    meta: { requiredAuth: true, breadcrumb: [breadcrumb.CreatedCustomer] }
  }
]

export { order }
