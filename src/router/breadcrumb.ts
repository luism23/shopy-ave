import { BreadcrumbComponent } from '@/types'
import { getBreadcrumb } from '@/utils'
// import { ROUTES } from '@/constants'

const BREADCRUMB: Record<BreadcrumbComponent, string> = {
  TheLayout: 'Inicio',
  Dispatch: 'Despacho',
  CreateDispatch: 'Crear despacho',
  Inventory: 'Inventario',
  InventoryManager: 'Gestión de inventario',
  CreateProduct: 'Crear producto',
  Pickup: 'Recogidas',
  ShippingRatio: 'Relación de envio',
  StepsShippingRatio: 'Crear relación de envio',
  CreateSale: 'Crear venta',
  Channel: 'Canales',
  ChannelAve: 'CanalesAve',
  CampaingAve: 'Campanas',
  Campaing: 'Campanas',
  ChannelCampain: 'Canales y campañas',
  TabPriceList: 'Lista de precios',
  Discounts: 'Descuentos',
  Customer: 'Clientes',
  Dealer: 'Distribuidores',
  Dropshipping: 'Dropshipping',
  Suppliers: 'Proveedores',
  MySales: 'Mis ventas',
  CreatedCustomer: 'Crear cliente',
  CreateDiscount: 'Crear descuento',
  CreatePriceList: 'Crear lista de precio',
  UpdatePriceList: 'Actualizar lista de precio',
  AccountStatus: 'Estado de cuenta',
  AccountStatusAve: 'Estado de cuenta',
  PaymentLink: 'Recaudos y link de pago',
  MyTreasury: 'Mi tesoreria',
  CollectGuides: 'Mis Recaudos',
  DiscountAve: 'Descuento',
  DiscountCrudAve: 'Configuracion de descuento',
  NewsAve: 'Novedades',
  referidos: 'referidos',
  conciliaciones: 'conciliaciones',
  NewsChargeAve: 'novedadescarga'
}

const { breadcrumb } = getBreadcrumb(BREADCRUMB)

export { breadcrumb }
