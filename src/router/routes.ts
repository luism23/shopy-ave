import { RouteRecordRaw } from 'vue-router'
import { AccountStatusAve } from './accountStatus'
import { CollectGuidesAve } from './collectGuidesAve'
import { CanalesCampanas, Canales, Campanas } from './campanasCanalesAve'
import { DiscountAve, DiscountCRUDAve } from './discountAve'
import {
  NovedadesAve,
  NovedadesCarga,
  NovedadesDocumentoAve
} from './novedadesAve'
import { Conciliaciones } from './conciliacionesAve'
import { guideDetails } from './guideDetailAvev'
import { referredCustomers } from './referredCustomers'
import { shopy } from './shopy'
import { shopyConfirmed } from './shopyConfirmed'
import { shopyRefused } from './shopyRefused'
import { shopyExceed } from './shopyExceed'

const routes: RouteRecordRaw[] = [
  AccountStatusAve,
  CollectGuidesAve,
  CanalesCampanas,
  DiscountAve,
  DiscountCRUDAve,
  NovedadesCarga,
  NovedadesAve,
  NovedadesDocumentoAve,
  Canales,
  Campanas,
  Conciliaciones,
  guideDetails,
  referredCustomers,
  shopy,
  shopyConfirmed,
  shopyRefused,
  shopyExceed
]

export { routes }
