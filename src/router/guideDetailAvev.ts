import { RouteRecordRaw } from 'vue-router'
import { ROUTES } from '@/constants'

export const guideDetails: RouteRecordRaw = {
  path: ROUTES.DETALLEPOPUP.PATH,
  name: ROUTES.DETALLEPOPUP.NAME,
  component: () => import('@/views/guideDetails/ViewDetail.vue')
}
