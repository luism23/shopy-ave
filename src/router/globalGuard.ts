import { NavigationGuardNext, RouteLocationNormalized } from 'vue-router'

const globalGuard = (
  to: RouteLocationNormalized,
  _: RouteLocationNormalized,
  next: NavigationGuardNext
): void => {
  next()
}

export { globalGuard }
