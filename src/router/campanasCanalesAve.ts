import { RouteRecordRaw } from 'vue-router'
import { ROUTES } from '@/constants'
import { breadcrumb } from './breadcrumb'

const CanalesCampanas: RouteRecordRaw = {
  path: ROUTES.CANALESYCAMPANA.PATH,
  name: ROUTES.CANALESYCAMPANA.NAME,
  component: () => import('@/views/campaignChannel/ViewChannelCampaingAve.vue'),
  meta: { breadcrumb: [breadcrumb.AccountStatusAve, breadcrumb.ChannelCampain] }
}

const Canales: RouteRecordRaw = {
  path: ROUTES.CANAL.PATH,
  name: ROUTES.CANAL.NAME,
  component: () => import('@/views/campaignChannel/ViewChannelCrudAve.vue'),
  meta: {
    breadcrumb: [
      breadcrumb.AccountStatusAve,
      breadcrumb.ChannelCampain,
      breadcrumb.ChannelAve
    ]
  }
}

const Campanas: RouteRecordRaw = {
  path: ROUTES.CAMPANA.PATH,
  name: ROUTES.CAMPANA.NAME,
  component: () => import('@/views/campaignChannel/ViewCampaingCrudAve.vue'),
  meta: {
    breadcrumb: [
      breadcrumb.AccountStatusAve,
      breadcrumb.ChannelCampain,
      breadcrumb.CampaingAve
    ]
  }
}

export { Canales, Campanas, CanalesCampanas }
