/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_ILA_V1: string
  readonly VITE_APP_V1: string
  readonly VITE_KEY_EDITOR_TINY: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
