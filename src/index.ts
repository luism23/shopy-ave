import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { library } from '@fortawesome/fontawesome-svg-core'
import VTooltip from 'v-tooltip'

import 'vue-inner-image-zoom/lib/vue-inner-image-zoom.css'
import '@/styles/index.scss'

import { allIcons } from '@/styles/fontawesome'
import router from '@/router'
import App from '@/App.vue'

library.add(allIcons)

const app = createApp(App)

app.use(router)
app.use(createPinia())
app.use(VTooltip, {
  themes: {
    Primary: {
      triggers: ['hover'],
      autoHide: false,
      offset: [0, 15]
    },
    Secondary: {
      triggers: ['hover'],
      autoHide: false,
      offset: [0, 15]
    }
  }
})

app.mount('#app')

if (import.meta.env.MODE === 'development') {
  app.config.performance = true
  if (import.meta.hot) {
    import.meta.hot.accept()
    import.meta.hot.dispose(() => {
      app.unmount()
    })
  }
}
