import { IFacturasReq, IFacturasResp } from '@/types/CollectionsAve'
import axios, { AxiosResponse } from 'axios'

export const axiosInstance = axios.create({
  baseURL: 'https://aveonline.co/api/recaudos/v1.0/index.php',
  headers: {
    Authorization: 'auth_token'
  }
})

const GetFacturas = async (data: IFacturasReq) => {
  // const urlFull = `${baseURL}/api/recaudos/v1.0/index.php`;
  const urlFull = 'https://aveonline.co/api/recaudos/v1.0/index.php'
  try {
    const dataResponse: AxiosResponse = await axiosInstance.post(urlFull, data)
    if (dataResponse) {
      const resposeAve: IFacturasResp = dataResponse.data
      return Promise.resolve(resposeAve)
    }
  } catch (err) {
    return Promise.reject(err)
  }
}

export { GetFacturas }
