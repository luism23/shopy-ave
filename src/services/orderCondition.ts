import { AxiosResponse } from 'axios'

import http from '@/services/httpAve'
import { OrderCondition, Insurance } from '@/types'

function getOrderConditionService(): Promise<AxiosResponse<OrderCondition>> {
  const route = '/order-condition'
  return http().get<OrderCondition>(route)
}

function updateOrderConditionService(order: OrderCondition) {
  const route = '/order-condition'
  return http().put(route, order)
}

function getAllInsuranceValueService(): Promise<AxiosResponse<Insurance[]>> {
  const route = '/order-condition/insurance-value'
  return http().get<Insurance[]>(route)
}

export {
  getOrderConditionService,
  updateOrderConditionService,
  getAllInsuranceValueService
}
