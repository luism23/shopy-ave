import { AxiosResponse } from 'axios'

import http from '@/services/httpAve'

import { Product, ProductByCategory, FilterResponseAttributes } from '@/types'

function getProductsService({ page = 1, perPage = 20 } = {}) {
  const route = '/product'
  return http().get(route, {
    params: {
      page,
      per_page: perPage
    }
  })
}

function getProductsPriceListService({ page = 1, perPage = 20 } = {}) {
  const route = '/product/priceList'
  return http().get(route, {
    params: {
      page,
      per_page: perPage
    }
  })
}

function getProductTypeService() {
  const route = '/product/type'
  return http().get(route)
}

function getInventories({
  page = 0,
  perPage = 0,
  center = '',
  search = '',
  category = '',
  name = ''
} = {}) {
  const route = '/product/inventory'
  return http().get(route, {
    params: {
      page,
      per_page: perPage,
      idDispatchCenter: center,
      search: search,
      category: category,
      name: name
    }
  })
}

function updateInventoryUnits(data: any) {
  const route = '/product/inventory'
  return http().patch(route, data)
}

function createProductService(product: Product) {
  const route = '/product'
  return http().post(route, product)
}

function getOneProductService(id: string) {
  const route = `/product/${id}`
  return http().get(route)
}

function updateProductService({ id, status }: any) {
  const route = `/product/${id}`
  return http().patch(route, { status })
}

function getReportInventoryCsv() {
  const route = '/product/inventory/report'
  return http().get(route)
}

function editStatusProductId(id: string, state: boolean) {
  const route = 'product/status'
  return http().put(route, {
    id: id,
    is_active: state
  })
}

function getHistoryInventory({ page = 1, perPage = 20 } = {}) {
  const route = 'product/inventory/history'
  return http().get(route, {
    params: {
      page,
      per_page: perPage
    }
  })
}

function getHistoryInventoryId(id: string) {
  const route = `product/inventory/history/${id}`
  return http().get(route)
}

function getProductsByCategory(): Promise<AxiosResponse<ProductByCategory[]>> {
  const route = 'product/by_category'
  return http().get<ProductByCategory[]>(route)
}

function getFilterByAttribute({
  idGroupProduct = '',
  attributes = []
}: {
  idGroupProduct: string
  attributes: Record<string, any>[]
}): Promise<AxiosResponse<FilterResponseAttributes[]>> {
  const route = 'product/filter_by_attributes'
  return http().get<FilterResponseAttributes[]>(route, {
    params: {
      id_group_product: idGroupProduct,
      attributes: JSON.stringify(attributes)
    }
  })
}

export {
  createProductService,
  getOneProductService,
  updateProductService,
  getProductsService,
  getInventories,
  updateInventoryUnits,
  getReportInventoryCsv,
  getProductTypeService,
  editStatusProductId,
  getHistoryInventory,
  getHistoryInventoryId,
  getProductsByCategory,
  getFilterByAttribute,
  getProductsPriceListService
}
