import { IGetResultPayWompi } from '@/types'
import { logDev } from '@/utils'
import http from './httpAve'

/**
 * CONSUMO CRUDO Y PURO DEL ENDPOINT DE TRANSPORTADORA
 * * no recibe body
 * @returns Promise[]
 */
function validPayWompi(body: IGetResultPayWompi) {
  const route = '/wompi/index.php'
  return http().post(route, body)
}

const validatePayWompi = async (id: string) => {
  try {
    const body: IGetResultPayWompi = {
      tipo: 'getResposeId',
      id_wompi: id,
      env_test: true
    }
    return new Promise<IGetResultPayWompi>((resolve, reject) => {
      validPayWompi(body).then((resp) => {
        const data = resp.data
        if (data.status === 'ok') {
          const resposeAve: IGetResultPayWompi = data.data
          resolve(resposeAve)
        } else {
          reject(data.status)
        }
      })
    })
  } catch (error) {
    logDev('error', error)
  }
}

export { validatePayWompi, validPayWompi }
