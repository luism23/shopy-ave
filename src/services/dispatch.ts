import http from '@/services/httpAve'
import { ResponseDispatch } from '@/types'
import { AxiosResponse } from 'axios'

function getDispatchService({
  page = 1,
  perPage = 10,
  status = '',
  from = '',
  to = ''
}): Promise<AxiosResponse<ResponseDispatch>> {
  const route = '/dispatch'
  return http().get<ResponseDispatch>(route, {
    params: { page, per_page: perPage, status, from, to }
  })
}

function getEnlistmentService(
  page: number,
  perPage: number,
  state: string,
  from?: string,
  to?: string
) {
  const route = '/dispatch/enlistment'
  return http().get(route, {
    params: { page, per_page: perPage, state, from, to }
  })
}

function postEnlistmentService(data: any) {
  const route = '/dispatch/enlistment'
  return http().post(route, data)
}

function updateEnlistmentService(id: string, data: any) {
  const route = `/dispatch/enlistment/${id}`
  return http().patch(route, data)
}

function getEnlistmentByIDService(id: string, state: string) {
  const route = `/dispatch/enlistment/${id}`
  return http().get(route, { params: { state } })
}

function deleteDispatchInEnlistmentByIDService(id: string, data: any) {
  const route = `/dispatch/enlistment/${id}/delete`
  return http().patch(route, data)
}

function getDispatchesInDateRangeService(from: string, to: string) {
  const route = '/dispatch/options'
  return http().get(route, { params: { from, to } })
}

function getEnlistmentsInDateRangeService(from: string, to: string) {
  const route = '/dispatch/enlistment/options'
  return http().get(route, { params: { from, to } })
}

function updateDispatcherByIDService(id: string, data: any) {
  const route = `/dispatch/${id}/dispatcher`
  return http().patch(route, data)
}

function getEnlistmentByDispatchIDService(id: string) {
  const route = `/dispatch/${id}/enlistment`
  return http().get(route)
}

function getReportDispatchService(state: string, from?: string, to?: string) {
  const route = '/dispatch/report'
  return http().get(route, { params: { state, from, to } })
}

function getReportEnlistmentService(state: string, from?: string, to?: string) {
  const route = '/dispatch/enlistment/report'
  return http().get(route, { params: { state, from, to } })
}

function updateEnlistmentDispatchesService(
  id: string,
  state: string,
  dispatches: any
) {
  const route = `/dispatch/enlistment/${id}/dispatches`
  return http().patch(route, { state: state, dispatches: dispatches })
}

export {
  getDispatchService,
  getEnlistmentService,
  postEnlistmentService,
  getEnlistmentByIDService,
  updateEnlistmentService,
  deleteDispatchInEnlistmentByIDService,
  getDispatchesInDateRangeService,
  getEnlistmentsInDateRangeService,
  updateDispatcherByIDService,
  getEnlistmentByDispatchIDService,
  getReportDispatchService,
  getReportEnlistmentService,
  updateEnlistmentDispatchesService
}
