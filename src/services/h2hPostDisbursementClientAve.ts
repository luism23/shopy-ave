import { IBankSendPays } from '@/types/BankAve'
import http from './httpAve'

function PostDisbursementClient(body: IBankSendPays) {
  const route =
    'https://api.aveonline.co/api-h2h/public/api/v1/wallet/insertPayment'
  return http().post(route, body)
}

export { PostDisbursementClient }
