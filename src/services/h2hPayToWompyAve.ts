import { IGetRefWompyReq } from '@/constants/TypesGenerales'
import { logDev } from '@/utils'
import http from './httpAve'

function PayToWompy(body: IGetRefWompyReq) {
  const route = 'https://aveonline.co/api/comunes/v2.0/pagosWompi.php'
  return http().post(route, body)
}

const validateToWompy = async (id: string) => {
  try {
    const body: IGetRefWompyReq = {
      tipo: 'solicitarReferencia',
      idexp: id,
      id_sector: 0,
      valor_ave: '',
      facturas: ''
    }
    return new Promise<IGetRefWompyReq>((resolve, reject) => {
      PayToWompy(body).then((resp) => {
        const data = resp.data
        logDev('ssss', data)
        if (data.status === 'ok') {
          const resposeAve: IGetRefWompyReq = data.data
          resolve(resposeAve)
        } else {
          reject(data.status)
        }
      })
    })
  } catch (error) {
    logDev('', error)
  }
}

export { PayToWompy, validateToWompy }
