import { AxiosResponse } from 'axios'
import http from '@/services/httpAve'
import {
  PricingRequestList,
  PricingResponseList,
  ResponseAveonline,
  RequestUpdatePlan
} from '@/types'

function getPricingService({ tipo = 'listarPlanes' } = {}): Promise<
  AxiosResponse<PricingResponseList, PricingRequestList>
> {
  const route = '/ila/plan/'
  return http('APP_AVEONLINE').post<
    string,
    AxiosResponse<PricingResponseList, PricingRequestList>,
    PricingRequestList
  >(route, { tipo })
}

function updatePricingPlanService({
  idEnterpise = '',
  idPlan = '',
  idFrecuency = ''
} = {}): Promise<AxiosResponse<ResponseAveonline, RequestUpdatePlan>> {
  const route = '/ila/plan/'
  return http('APP_AVEONLINE').post<
    string,
    AxiosResponse<ResponseAveonline, RequestUpdatePlan>,
    RequestUpdatePlan
  >(route, {
    tipo: 'actualizarPlanCliente',
    idEmpresa: idEnterpise,
    idPlan,
    periodoPago: idFrecuency
  })
}

export { updatePricingPlanService, getPricingService }
