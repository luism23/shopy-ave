import http from '@/services/httpAve'
import { parseJWT } from '@/utils/token'

const TOKEN_NAME = 'TokenAve'
const authtoken = localStorage.getItem(TOKEN_NAME)
/**
 * Solo para usar en DEV
 * @returns
 */
export const GetToken = async () => {
  // const urlFull = `${baseURL}/api/comunes/v2.0/autenticarusuario.php`; // TODO
  const urlFull = 'https://aveonline.co/api/comunes/v2.0/autenticarusuario.php'
  try {
    const body = {
      tipo: 'authLogin',
      usuario: 'Demo - Webservices',
      idtipo_cliente: '1',
      ipvisitante: '55521244444',
      latitud: 'arriba',
      longitud: 'aladerecha',
      ciudad: 'medallo',
      pais: 'polombia',
      nombre: 'Demo',
      i_idusuario: '112',
      i_idempresa: '16833' // '22029'//'22007'//'17889'//'21680'// '12185'//
    }
    if (!authtoken && process.env.NODE_ENV === 'development') {
      fetch(urlFull, { body: JSON.stringify(body), method: 'POST' })
        .then((res) => res.json())
        .then((json) => {
          if (json.status === 'ok') {
            localStorage.setItem(TOKEN_NAME, json.token)
          }
        })
    }
    return Promise.resolve(true)
  } catch (error) {
    return Promise.reject(error)
  }
}

const extracDataToken = () => {
  if (authtoken) {
    return parseJWT(authtoken)
  }
  return null
}

function GetDatosRemitente() {
  const data = extracDataToken()
  if (data) {
    const route = '/courier/dhl/v1.0/datosremitente.php'
    const body = {
      tipo: 'listar',
      idempresa: data?.enterprise
    }
    return http().post(route, body)
  }
}
export { GetDatosRemitente, extracDataToken }
