import http from '@/services/httpAve'
import { Bank } from '@/types'

function createBankService(bank: Bank) {
  const route = '/financial_information/bank'
  return http().post(route, bank)
}

function getBankPaginationService(page: number, perPage: number) {
  const route = `/financial_information/bank?page=${page}&per_page=${perPage}`
  return http().get(route)
}

function getBankByID(id: string) {
  const route = `/financial_information/bank/${id}`
  return http().get(route)
}

function updateBankService(id: string, data: Bank) {
  const route = `/financial_information/bank/${id}`
  return http().patch(route, { status: data })
}

function deleteBankService(id: string) {
  const route = `/financial_information/bank/${id}`
  return http().delete(route)
}

export {
  createBankService,
  getBankPaginationService,
  updateBankService,
  deleteBankService,
  getBankByID
}
