import http from '@/services/httpAve'
import { AccountBankType } from '@/types'

function createAccountBankTypeService(category: AccountBankType) {
  const route = '/financial_information/account_type_bank'
  return http().post(route, category)
}

function getAccountBankTypePaginationService(page: number, perPage: number) {
  const route = `/financial_information/account_type_bank?page=${page}&per_page=${perPage}`
  return http().get(route)
}

function getAccountBankTypeByID(id: string) {
  const route = `/financial_information/account_type_bank/${id}`
  return http().get(route)
}

function updateAccountBankTypeService(id: string, data: AccountBankType) {
  const route = `/financial_information/account_type_bank/${id}`
  return http().patch(route, data)
}

function deleteAccountBankTypeService(id: string) {
  const route = `/financial_information/account_type_bank/${id}`
  return http().delete(route)
}

export {
  createAccountBankTypeService,
  getAccountBankTypePaginationService,
  getAccountBankTypeByID,
  updateAccountBankTypeService,
  deleteAccountBankTypeService
}
