import { ISaveAccountBank } from '@/constants/TypesGenerales'
import http from './httpAve'

function PostNewAccountBank(body: ISaveAccountBank) {
  const route =
    'https://api.aveonline.co/api-h2h/public/api/v1/wallet/inscriptionAcount'
  return http().post(route, body)
}

export { PostNewAccountBank }
