import http from './httpAve'
import { format, subDays } from 'date-fns'
import { useUsuarioStore } from '@/store'
import { IRecaudosDataReq, IRecaudosReq } from '@/constants'

/**
 * SOLO SE CONSUME AL INICIO DE LA VISTA
 * @returns Promise
 */
function getRecaudos() {
  const route = '/recaudos/v1.0/index.php'
  const body: IRecaudosReq = {
    tipo: 'consultarRecaudos',
    data: {
      idtransportador: '',
      dsconsec: '',
      dsvalorrecaudo: '',
      dsfechai: format(subDays(new Date(), 30), 'yyyy-MM-dd'),
      dsfechaf: format(new Date(), 'yyyy-MM-dd'),
      dsciudad: '',
      dsciudadd: '',
      idexp: useUsuarioStore().getIdEnterprise,
      idcampobase: '',
      dsempresap: ''
    }
  }
  return http().post(route, body)
}

function getColletedGuideBody(protoBody: Partial<IRecaudosDataReq>) {
  const route = '/recaudos/v1.0/index.php'
  const body: IRecaudosReq = {
    tipo: 'consultarRecaudos',
    data: {
      idtransportador: protoBody.idtransportador ?? '',
      dsconsec: protoBody.dsconsec ?? '',
      dsvalorrecaudo: '',
      dsfechai:
        protoBody.dsfechai ?? format(subDays(new Date(), 30), 'yyyy-MM-dd'),
      dsfechaf: protoBody.dsfechaf ?? format(new Date(), 'yyyy-MM-dd'),
      dsciudad: '',
      dsciudadd: '',
      idexp: useUsuarioStore().getIdEnterprise,
      idcampobase: '',
      dsempresap: ''
    }
  }
  return http().post(route, body)
}

export { getRecaudos, getColletedGuideBody }
