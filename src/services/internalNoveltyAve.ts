import http from '@/services/httpAve'

function GetListNovelty(transportadora: number[] = [], destino: string[] = []) {
  const route = 'comunes/v2.0/novedades.php'
  const body = {
    tipo: 'listarNovedadesGuiaInternos',
    fechaIni: '',
    fechaFin: '',
    transportadora,
    destino
  }
  return http().post(route, body)
}

function GetNoDeliveryNovalty() {
  const route = 'comunes/v2.0/novedades.php'
  const body = {
    tipo: 'noCumplimientoEntrega'
  }
  return http().post(route, body)
}

export { GetListNovelty, GetNoDeliveryNovalty }
