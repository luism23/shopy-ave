import http from '@/services/httpAve'

function GetListCampains() {
  const route = 'comunes/v2.0/campanasCanales.php'
  const body = {
    tipo: 'ListarCampana'
  }

  return http().post(route, body)
}

export { GetListCampains }
