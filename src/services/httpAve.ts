import axios, { AxiosInstance } from 'axios'
type TYPE_URL = 'APP_AVEONLINE'
const TOKEN_NAME = 'TokenAve'
const authtoken = localStorage.getItem(TOKEN_NAME)

function createAxios() {
  const getURL = (typeURL: TYPE_URL): string | undefined => {
    if (typeURL === 'APP_AVEONLINE') return import.meta.env.VITE_APP_V1
  }

  return (typeURL: TYPE_URL = 'APP_AVEONLINE'): AxiosInstance => {
    return axios.create({
      baseURL: getURL(typeURL as TYPE_URL),
      withCredentials: false,
      headers: {
        Authorization: `${authtoken}`
      }
    })
  }
}

const httpInstance = createAxios()

export default httpInstance
