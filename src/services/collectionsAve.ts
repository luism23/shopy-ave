import { IRecaudosReq } from '../types/CollectionsAve'
import http from './httpAve'

function GetRecuados(body: IRecaudosReq) {
  const route = '/recaudos/v1.0/index.php'
  return http().post(route, body)
}

export { GetRecuados }
