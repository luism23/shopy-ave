import { ISendMsg } from '@/types/BankAve'
import { coverUpIdEnterprise, coverUpIdUser } from '@/utils/encryDataAve'
import http from './httpAve'

function RequestMessagePayDisbursement(idUser: string, idEnterprise: string) {
  const route =
    'https://api.aveonline.co/api-h2h/public/api/v1/h2h/requestSMSH2H'
  const body: ISendMsg = {
    userEnterprise: coverUpIdUser(idUser),
    enterprise: coverUpIdEnterprise(idEnterprise)
  }
  return http().post(route, body)
}

export { RequestMessagePayDisbursement }
