import { IFacturasReq } from '../types/CollectionsAve'
import http from './httpAve'

function GetRecibosYnotas(bodyFac: IFacturasReq) {
  const route = '/recaudos/v1.0/index.php'
  return http().post(route, bodyFac)
}

export { GetRecibosYnotas }
