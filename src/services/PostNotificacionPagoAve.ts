import { IPostNotificarPago } from '@/constants/TypesGenerales'
import http from './httpAve'

const PostNotificacionPago = async (body: IPostNotificarPago) => {
  const route = 'https://aveonline.co/api/comunes/v2.0/pagosListado.php'
  return http().post(route, body)
}

export { PostNotificacionPago }
