import { IFavoriteAccount } from '@/types/BankAve'
import http from './httpAve'

function ChangeFavoriteAccount(body: IFavoriteAccount) {
  const route =
    'https://api.aveonline.co/api-h2h/public/api/v1/wallet/acountFavorite'
  return http().post(route, body)
}

export { ChangeFavoriteAccount }
