import http from '@/services/httpAve'
import { logDev } from '@/utils'

function GetListChannels() {
  const route = 'comunes/v2.0/campanasCanales.php'
  const body = {
    tipo: 'ListarCanal'
  }
  return http().post(route, body)
}
const validateGetListCannels = async (body: any) => {
  logDev('canals', body)
}

export { GetListChannels, validateGetListCannels }
