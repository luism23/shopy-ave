import { IResqGetlisBank } from '@/constants/TypesGenerales'
// import { logDev } from '@/utils'
import http from './httpAve'

function GetListBanks(body: IResqGetlisBank) {
  const route = 'https://api.aveonline.co/api-h2h/public/api/v1/h2h/getBanks'
  return http().post(route, body)
}

export { GetListBanks }
