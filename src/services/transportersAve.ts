import { TRANSPORTERS_PERMITED } from '@/constants'
import { IlistTranspo, Transporter } from '@/types'
import { logError } from '@/utils'
import http from './httpAve'

/**
 * CONSUMO CRUDO Y PURO DEL ENDPOINT DE TRANSPORTADORA
 * * no recibe body
 * @returns Promise[]
 */
function transportersList() {
  const route = '/box/v1.0/transportadora.php'
  const body = {
    tipo: 'listaSimple'
  }
  return http().post(route, body)
}

/**
 * RETORNA LA LISTA DE TRANSPORTADORAS YA LISTA PARA EL APPFILTER
 * @returns Promise<IlistTranspo[]>
 */
const getListTransporterPermited = () => {
  const data: IlistTranspo[] = []
  try {
    return new Promise<IlistTranspo[]>((resolve, reject) => {
      transportersList()
        .then((res) => {
          const resp: Transporter[] = [...res.data.transportadoras]
          if (resp) {
            resp.forEach((el) => {
              if (TRANSPORTERS_PERMITED.includes(String(el.id))) {
                data.push({
                  id: el.id,
                  name: el.text,
                  status: false
                })
              }
            })
          }
          resolve(data)
        })
        .catch((err) => {
          logError('err getListTransporterPermited', err)
          reject(err)
        })
    })
  } catch (error) {
    logError('error getListTransporterPermited', error)
  }
}

export { transportersList, getListTransporterPermited }
