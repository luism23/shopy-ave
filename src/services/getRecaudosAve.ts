import {
  IFacturasReq,
  INotasRecibosResp,
  IRecaudosReq
} from '@/types/CollectionsAve'
import { ErrorRespose, IResponseAveRecaudos } from '@/types/ResponseAve'
import { AxiosError, AxiosResponse } from 'axios'
import { axiosInstance } from './expensesAve'

export const GetRecuados = async (
  data: IRecaudosReq
): Promise<IResponseAveRecaudos> => {
  const urlFull = 'https://aveonline.co/api/recaudos/v1.0/index.php'
  try {
    const dataResponse: AxiosResponse = await axiosInstance.post(urlFull, data)
    if (dataResponse) {
      const resposeAve: IResponseAveRecaudos = dataResponse.data
      return Promise.resolve(resposeAve)
    }
    return Promise.reject(dataResponse)
  } catch (err: AxiosError | unknown) {
    const Error = err as AxiosError
    const ErrResp: ErrorRespose = Error.response?.data
    return Promise.reject(ErrResp)
  }
}

export const GetRecibosYnotas = async (data: IFacturasReq) => {
  // const urlFull = `${baseURL}/api/recaudos/v1.0/index.php`;
  const urlFull = 'https://aveonline.co/api/recaudos/v1.0/index.php'
  try {
    const dataResponse: AxiosResponse = await axiosInstance.post(urlFull, data)
    if (dataResponse) {
      const resposeAve: INotasRecibosResp = dataResponse.data
      return Promise.resolve(resposeAve)
    }
  } catch (err) {
    return Promise.reject(err)
  }
}
