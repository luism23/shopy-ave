import http from '@/services/httpAve'

function GetListServices() {
  const route = 'comunes/v2.0/campanasCanales.php'
  const body = {
    tipo: 'ListarServicios'
  }
  return http().post(route, body)
}

export { GetListServices }
