const base = '/appdev/modulos/'

export const ROUTES = {
  ESTADO_CUENTA: {
    PATH: `${base}carteraV2`,
    NAME: 'AccountStatusAve'
  },
  DETALLE_COMERCIAL: {
    PATH: `${base}detalleComercial`,
    NAME: 'detalleComercial'
  },
  RECAUDOS: {
    PATH: `${base}recaudos`,
    NAME: 'CollectGuides'
  },
  CONFIGDESCUENTOS: {
    PATH: `${base}configdiscount`,
    NAME: 'DiscountAve'
  },
  CONFIGDESCUENTOSCRUD: {
    PATH: `${base}configdiscountCRUD`,
    NAME: 'DiscountCrudAve'
  },
  CANALESYCAMPANA: {
    PATH: `${base}canalesycampana`,
    NAME: 'ChannelCampain'
  },
  NOVEDADES: {
    PATH: `${base}novedades`,
    NAME: 'NewsAve'
  },
  NOVEDADESCARGA: {
    PATH: `${base}novedadescarga`,
    NAME: 'NewsChargeAve'
  },
  NOVEDADESDOCUMENTO: {
    PATH: `${base}novedadesdocumento`,
    NAME: 'NewsDocumentAve'
  },
  CAMPANA: {
    PATH: `${base}campana`,
    NAME: 'CampaingAve'
  },
  CANAL: {
    PATH: `${base}canales`,
    NAME: 'ChannelAve'
  },
  DETALLEPOPUP: {
    PATH: '/appdev/modulos/guideDetails/:id?',
    NAME: 'guideDetails'
  },
  CONCILIACIONES: {
    PATH: `${base}conciliaciones`,
    NAME: 'conciliaciones'
  },
  REFERREDDCUSTOMERS: {
    PATH: `${base}referidos`,
    NAME: 'referidos'
  },
  SHOPY: {
    PATH: `${base}shopy`,
    NAME: 'shopy'
  },
  SHOPY_CONFIRMED: {
    PATH: `${base}shopyConfirmed`,
    NAME: 'shopyConfirmed'
  },
  SHOPY_REFUSED: {
    PATH: `${base}shopyRefused`,
    NAME: 'shopyRefused'
  },
  SHOPY_EXCEED: {
    PATH: `${base}shopyExceed`,
    NAME: 'shopyExceed'
  }
}
