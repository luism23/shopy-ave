/* eslint-disable camelcase */

export type ButtonTypes = {
  type: 'button' | 'submit' | 'reset'
}

export interface DateInputs {
  final: string
  init: string
}

export interface TokenPayload {
  aud: string
  data: string
  exp: number
  // eslint-disable-next-line camelcase
  i_idusuario: string
  iss: string
  usuario: string
  enterprise: string
}

export interface Base64 {
  blob: File
  image: string
  base: ArrayBuffer | string | null
}
export interface Base64Req {
  base: ArrayBuffer | string | null
  type: string
  name: string
  ext: string
}

export interface IUserData {
  // eslint-disable-next-line camelcase
  id_user: string
  // eslint-disable-next-line camelcase
  id_enterprise: string
  userName: string
}

export type TypeIcon = '' | 'ok' | 'error' | 'alert'

export interface IDataModal {
  show: boolean
  title: string
  message: string
  type: TypeIcon
}

export interface IConfirmAccount {
  identificacionBeneficiario: number | null
  nombreDelBeneficiario: string | null
  bancoCuentaDelBeneficiarioDestino: number | null
  numeroCuentaDelBeneficiario: string | null
  tipoDeTransaccion: number | null
  tipoDeDocumentoDeIdentificacion: number | null
}
export interface IConfirmAccountForm extends IConfirmAccount {
  accountNumber: string | null
}

export interface ISaveAccountBankBody {
  identificacionBeneficiario: number
  nombreDelBeneficiario: string
  bancoCuentaDelBeneficiarioDestino: number
  numeroCuentaDelBeneficiario: string
  tipoDeTransaccion: number
  tipoDeDocumentoDeIdentificacion: number
}

export interface ISaveAccountBank {
  code: string
  bodyToken: string
  // body: Body[];
  body: ISaveAccountBankBody[]
}

export interface IResqGetlisBank {
  // eslint-disable-next-line camelcase
  type_code: 1 | 2
  bank: string
}

export interface ILisBankResp {
  dsnombre: string
  idcodbanco: number
}
export interface IArrayMultiSelect {
  label: string
  value: number
}

export interface IExportData {
  data: IConfirmAccount
  bancos: IArrayMultiSelect[]
}

export interface IRespGeneAve {
  message: string
  status: 'ok' | 'error'
}

export interface IAveTransp {
  id: number
  text: string
}
export interface IRespGeneAveTransp {
  message: string
  status: 'ok' | 'error'
  transportadoras: IAveTransp[]
}
export interface IRespGeneAveData {
  message: string
  data: string
  status: 'ok' | 'error'
}
export interface IRecaudosDataReq {
  idtransportador: string
  dsconsec: string
  dsvalorrecaudo: string
  dsfechai: string
  dsfechaf: string
  dsciudad: string
  dsciudadd: string
  idexp: string
  idcampobase: string
  dsempresap: string
}
export interface IRecaudosReq {
  tipo: 'consultarRecaudos'
  data: IRecaudosDataReq
}
export interface IRespGetInvoice {
  base: string
  name: string
  status: 'ok'
}
export interface IRecaudosData {
  numero_guia: string
  idtransportador: string
  dsconsec: string
  pkid: string
  idagente: string
  agente: string
  codagente: string
  idcliente: string
  dscomtotal: string
  dscom: string
  tipodoc: string
  numdoc: string
  dsfecha: string
  idexp: string
  dsres: string
  dsprefijofactura: string
  dscontrol: string
  dsnumfacturaave: string
  dsnumfacturaave_fc: string
  dsres_fc: string
  recibo: number
  egreso: number
  valoregreso: number
  fechaegreso: number
  estadorecaudotxt: string
  valortransrecaudo: number
  flete: number
  dstotal: number
  dscostomanejo: number
  idsercontraentrega: string
  dsvalorrecaudo: number
  idasumecosto: string
  dscostorecaudo: number
  dsnombred: string
  dsestado: string
  empresa: string
  dsciudad: string
  dsciudadd: string
  transportadora: string
  dscontraentrega: string
  dsestadobox: string
  dsfechaentrega: string
  estadorecaudo: string

  idfactura_fc: number
  dsfleteave_fc: number
  dsmanejoave_fc: number
  dstotalave_fc: number
  dsotrosave_fc: number
}
export interface IRecaudosTotales {
  arecaudar: number
  devoluciones: number
  indemnizaciones: number
  liquidadas: number
  liquidadas_billetera: number
  procesoindemnizacion: number
  recaudadas: number
  sinmovimiento: number
}
export interface IResponseAllDataRecaudos {
  arecaudar: IRecaudosData[]
  devolucion: IRecaudosData[]
  indemnizado: IRecaudosData[]
  liquidadas: IRecaudosData[]
  proceso_indemnizacion: IRecaudosData[]
  recaudadas: IRecaudosData[]
  sin_movimiento: IRecaudosData[]
  totales: IRecaudosTotales
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type TypeRowCSV = object | any
export interface IDatosRemintente {
  ciudad: string
  ciudadCliente: string
  codDepartamento: string
  codPostalCliente: string
  correo1: string
  correo2: string
  direccion: string
  // eslint-disable-next-line camelcase
  dsemail_responsable: string
  idcliente: number
  idsector: number
  nombre: string
  tel1: string
  tel2: string
  tel3: string
  tel4: string
  // eslint-disable-next-line camelcase
  tipo_identificacion: number
  dsimg: string
  dsimg1: string
  dsimg2: string
}

export interface IDatosRemintenteResp {
  cliente: IDatosRemintente[]
  message: string
  status: 'ok' | 'error'
}

export interface IOptions {
  label: string
  value: string | number
}

export interface IPostNotificarPago {
  tipo: 'insertar'
  idexp: number
  dsvalor: number
  // eslint-disable-next-line camelcase
  dsfecha_pago: string
  dsconsec: string
  dsidentificacion: string
  // eslint-disable-next-line camelcase
  idmedio_pago: number
  pkid: string
  pkids: string
  dsfacturas: string
  guiacourier: string
  observaciones: string
  file: Base64Req
}

export interface IGetRefWompyReq {
  tipo: 'solicitarReferencia'
  idexp: string
  // eslint-disable-next-line camelcase
  id_sector: number
  // eslint-disable-next-line camelcase
  valor_ave: string
  facturas: string
}

export interface IGetResultPayWompi {
  tipo: 'getResposeId'
  // eslint-disable-next-line camelcase
  id_wompi: string
  // eslint-disable-next-line camelcase
  env_test?: boolean
}
export interface IResultPayWompiResp extends IRespGeneAve {
  data: [
    {
      // eslint-disable-next-line camelcase
      numero_factura: string
      status: string
      // eslint-disable-next-line camelcase
      total_wompi: string
    }
  ]
}

export type Modes = 'Created' | 'Edit'
export type TypeIconSort = 'sort' | 'sort-down' | 'sort-up'
export type TypeIconOrder = 'chevron-down' | 'chevron-up'

export type IOrd = 'Desc' | 'Asc'
export interface ICurrency {
  value: number
  fractionDigits: number
  locales: 'es-CO' | 'en-US'
  currency: 'COP' | 'USD'
}
export interface IOpcionCurrency {
  style?: string
  currency: string
  minimumFractionDigits: number
}

export interface ISendMsg {
  enterprise: string
  userEnterprise: string
}

// eslint-disable-next-line camelcase
export interface IResp_SendMsg {
  status: string
  bodyToken: string
  exp: number
  phone: string
  mail: string
}

export interface IBankPayDataAccount {
  identificacionBeneficiario: string
  nombreDelBeneficiario: string
  bancoCuentaDelBeneficiarioDestino: string
  numeroCuentaDelBeneficiario: string
  tipoDeTransaccion: number
  valorTransaccion: string
  tipoDeDocumentoDeIdentificacion: number
  oficinaDeEntrega?: string
  email?: string
}

export interface IBankSendPays {
  code: string
  bodyToken: string
  body: IBankPayDataAccount[]
}

export interface IFavoriteAccount {
  body: [
    {
      codigoConfirmacion: number
      bancoCuentaDelBeneficiarioDestino: string
      numeroCuentaDelBeneficiario: string
      bodyToken: string
    }
  ]
}

export interface IBankSusbcriptDataAccount {
  identificacionBeneficiario: string
  nombreDelBeneficiario: string
  bancoCuentaDelBeneficiarioDestino: string
  numeroCuentaDelBeneficiario: string
  tipoDeTransaccion: number
  tipoDeDocumentoDeIdentificacion: number
}

export interface IBankSendSusbcripts {
  body: IBankSusbcriptDataAccount[]
}

export interface IRespListSusbcripts {
  identificacionBeneficiario: string
  nombreDelBeneficiario: string
  tipoDeDocumentoDeIdentificacion: number
  numeroCuentaDelBeneficiario: number
  tipoDecuenta: number
  nombreBanco: string
  bancoCuentaDelBeneficiarioDestino: string
  cuentaPorDefecto: boolean
}
