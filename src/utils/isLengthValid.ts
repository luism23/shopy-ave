function isLengthValid(value: string, total = 8): boolean {
  return value?.length >= total
}

export { isLengthValid }
