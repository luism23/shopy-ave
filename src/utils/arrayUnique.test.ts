import { test, expect, describe } from 'vitest'
import { arrayUnique } from './'

type Mock = { id: number; name: string }

const KEY_FILTER = 'name'

const MOCK_DATA: Mock[] = [
  { id: 1, name: 'One' },
  { id: 1, name: 'One' },
  { id: 2, name: 'Two' },
  { id: 3, name: 'Two' }
]

const RESULT_OPERATION: Mock[] = [
  { id: 1, name: 'One' },
  { id: 2, name: 'Two' }
]

describe('utils/arrayUnique.ts', () => {
  test('Array with unique objects', () => {
    expect(arrayUnique(MOCK_DATA as any, KEY_FILTER)).toEqual(RESULT_OPERATION)
  })

  test('Total number of unique items', () => {
    expect(arrayUnique(MOCK_DATA as any, KEY_FILTER)).toMatchObject(
      RESULT_OPERATION
    )
  })

  test('Array one dimension with unique elements', () => {
    const onlyIDs = MOCK_DATA.map((item) => item.id)
    expect(arrayUnique(onlyIDs)).toEqual([1, 2, 3])
  })
})
