function setStorage({ id = '', value = {} }): any {
  return window.localStorage.setItem(id, JSON.stringify(value))
}

function getStorage(id = ''): any {
  const data = window.localStorage.getItem(id)
  if (data) {
    const storage = typeof data === 'string' ? JSON.parse(data) : data
    return storage
  }
}

function removeStorage({ id = '', allItems = false }): any {
  return allItems
    ? window.localStorage.clear()
    : window.localStorage.removeItem(id)
}

export { setStorage, getStorage, removeStorage }
