import { test, expect, describe } from 'vitest'
import { isValidFileFormat } from './'

describe('utils/isValidFileFormat.ts', () => {
  test('Return true if the file format is valid', () => {
    const formatFile = [{ type: 'image/jpeg' }]
    expect(isValidFileFormat(formatFile, { isOptional: false })).toBe(true)
  })

  test('Return false if the file format is not valid', () => {
    const formatFile = [{ type: 'image/png' }]
    expect(isValidFileFormat(formatFile, { isOptional: false })).toBe(false)
  })

  test('Return true if the file format is optional', () => {
    const formatFile = [{ type: 'image/jpeg' }]
    expect(isValidFileFormat(formatFile, { isOptional: true })).toBe(true)
  })
})
