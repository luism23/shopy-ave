function isNumber(value = ''): boolean {
  return /(?=.*?[0-9])\w+/g.test(value)
}

export { isNumber }
