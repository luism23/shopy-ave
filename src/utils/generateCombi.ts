import { RequestCombi } from '@/types'

function generateCombi(reques: RequestCombi): any[] {
  const combining: any = []
  const result: any = []

  const getValues = {
    atributes: [],
    getNext() {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      return this.atributes.map((values) => values.values[values.location])
    }
  }

  const process = (combinatoria: any) => {
    getValues.atributes = combinatoria
    combinatoria[0].structure = getValues

    combinatoria[combinatoria.length - 1].calculation()
  }

  const setValues = (atributes: any) => {
    const size = atributes.length - 1

    combining.push({
      values: atributes[size],
      start: 0,
      location: 0,
      end: atributes[size].length,
      structure: {},
      calculation() {
        for (
          this.location = this.start;
          this.location < this.end;
          this.location++
        ) {
          result.push(this.structure.getNext())
        }
      }
    })

    let funtions = 0
    for (let index = size - 1; index >= 0; index--, funtions++) {
      const value = funtions
      combining.push({
        values: atributes[index],
        start: 0,
        location: 0,
        end: atributes[index].length,
        calculation() {
          for (
            this.location = this.start;
            this.location < this.end;
            this.location++
          ) {
            combining[value].calculation()
          }
        }
      })
    }

    return combining
  }

  const test = []
  test.push(reques.listOne)
  if (reques?.listTwo?.length > 0) {
    test.push(reques.listTwo)
  }

  if (reques?.listthree?.length > 0) {
    test.push(reques.listthree)
  }

  const combi = setValues(test)
  process(combi)

  return result
}

export { generateCombi }
