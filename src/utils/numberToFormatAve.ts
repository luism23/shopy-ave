/** Forxa los dos decimales a un numero */
export const numberToFormatAve = (numberToFormat: string) => {
  return Number.parseFloat(numberToFormat).toFixed(2)
}
