import { IOpcionCurrency } from '@/constants'

/** Dar el formato de dinero COP */
export const formatCurrencyCoAve = (number: number, fractionDigits = 0) => {
  const locales = 'es-CO'
  const currency = 'COP'
  const opcions: IOpcionCurrency = {
    style: 'currency',
    currency: currency,
    minimumFractionDigits: fractionDigits
  }
  if (number < 0) {
    delete opcions.style
  }
  const formatted = new Intl.NumberFormat(locales, opcions).format(number)
  return number < 0 ? `$ ${formatted}` : formatted
}
