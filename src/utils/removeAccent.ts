function removeAccent(letter: string): string {
  return letter.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
}

export { removeAccent }
