import { test, expect, describe } from 'vitest'
import { isValidFileSize } from './'

describe('utils/isValidFileSize.ts', () => {
  test('Return true if the file size and type img/jpeg is valid', () => {
    const file = [{ size: 5300000, type: 'image/jpeg' }]
    expect(isValidFileSize(file, { isOptional: false })).toBe(true)
  })

  test('Return false if the file has type is not valid', () => {
    const file = [{ size: 5300000, type: 'image/png' }]
    expect(isValidFileSize(file, { isOptional: false })).toBe(false)
  })

  test('Return true if the file size and type is optional', () => {
    const file = [{ size: 5300000, type: 'image/jpeg' }]
    expect(isValidFileSize(file, { isOptional: true })).toBe(true)
  })

  test('Return true if the file size and type application/pdf is valid', () => {
    const file = [{ size: 3200000, type: 'application/pdf' }]
    expect(isValidFileSize(file, { isOptional: false })).toBe(true)
  })
})
