/**
 * Elimina los elementos repetidos del array que se pase ne los parametros
 * @param arr array de tipo Any
 * @returns el array sin elementos repetidos
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const removeDuplicatesAve = (arr: Array<any>): Array<any> => {
  return arr.filter((valor, ind) => {
    return arr.indexOf(valor) === ind
  })
}
