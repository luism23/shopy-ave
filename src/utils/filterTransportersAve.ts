import { IAveTransp, IArrayMultiSelect } from '@/constants'

export const filterTransportersAve = (
  data: IAveTransp[]
): IArrayMultiSelect[] => {
  const blackList = [1012, 1013, 21, 1013, 1023, 1015, 1020]
  const dataFilter: IArrayMultiSelect[] = []
  data.forEach((t) => {
    if (!blackList.includes(Number(t.id))) {
      dataFilter.push({
        label: t.text,
        value: t.id
      })
    }
  })
  return dataFilter
}
