import { test, expect, describe } from 'vitest'
import { isNumber } from './'

describe('utils/isNumber.ts', () => {
  test('Should be a number', () => expect(isNumber('66')).toBeTruthy())
  test('Not is a valid number', () => {
    return expect(isNumber('fakeNumber')).toBeFalsy()
  })
})
