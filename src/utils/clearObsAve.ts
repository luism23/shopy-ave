import { REGEX } from '@/constants/'
/**
 * Funcion Auxiliar del prepareInvoiceToCSV()
 * limpia los etiquetados html y espacios dobles
 * @param str
 * @returns string limpio
 */
export const clearObsAve = (str: string): string => {
  const newStr = str.replace(REGEX.TAG_HTML, '')
  return newStr.replace(REGEX.SPACES, ' ')
}
