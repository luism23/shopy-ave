type Props = { isOptional: boolean }

const FORMAT = ['image/jpeg', 'application/pdf', 'image/jpg']
const messageFileFormat = 'Formato archivo no corresponde al solicitado'

function isValidFileFormat(value: any, props: Props): boolean {
  const { isOptional = false } = props

  if (value?.length) {
    return FORMAT.includes(value[0].type)
  }

  return isOptional
}

export { isValidFileFormat, messageFileFormat }
