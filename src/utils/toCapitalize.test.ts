import { test, expect, describe } from 'vitest'
import { toCapitalize } from './'

describe('utils/toCapitalize.ts', () => {
  test('Capitalize letter valid', () => {
    const value = 'hello'
    expect(toCapitalize(value)).toBe('Hello')
  })
})
