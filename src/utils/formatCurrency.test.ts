import { test, expect, describe } from 'vitest'
import { formatCurrency } from './'

describe('utils/formatCurrency.ts', () => {
  test('Return the currency format COP', () => {
    const response = '$ 50.000'
    const entry = { number: 50000 }
    expect(formatCurrency(entry)).toEqual(response)
  })

  test('Return the currency format with two decimals', () => {
    const response = '$ 50.000,00'
    const entry = { number: 50000, fractionDigits: 2 }
    expect(formatCurrency(entry)).toEqual(response)
  })
})
