// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function arrayUniqueAve<T extends Record<string, any>>(
  array: T[],
  key = ''
) {
  if (key) {
    return array.filter((value, index, cloneArray) => {
      const findIndex = cloneArray.findIndex((element) => {
        return element?.[key] === value?.[key]
      })

      return findIndex === index
    })
  }

  return array.filter((value, index, cloneArray) => {
    const findIndex = cloneArray.findIndex((element) => element === value)
    return findIndex === index
  })
}
