import { test, expect, describe } from 'vitest'
import { formatDate, formatDay, parseDateFormat } from './'

describe('utils/formatDate.ts', () => {
  test('formatDate: Return the date format dd-MM-yyyy', () => {
    const response = '01/01/2020'
    const entry = '2020-01-01'
    expect(formatDate(entry)).toEqual(response)
  })

  test("formatDate: Return the date format eeee d MMMM 'de' yyyy", () => {
    const response = 'Wednesday 1 January de 2020'
    const entry = '2020-01-01'
    expect(formatDate(entry, "eeee d MMMM 'de' yyyy")).toEqual(response)
  })

  test('formatDate: Return the date format dd/MM/yyyy', () => {
    const response = '01/01/2020'
    const entry = '2020-01-01'
    expect(formatDate(entry, 'dd/MM/yyyy')).toEqual(response)
  })

  test("formatDay: Return the day format eeeee d MMMM 'de' yyyy", () => {
    const response = 'miércoles 1 enero de 2020'
    const entry = '2020-01-01'
    expect(formatDay(entry, "eeee d MMMM 'de' yyyy")).toEqual(response)
  })

  test("parseDateFormat: Return the date format dd 'de' MMMM", () => {
    const response = '01 de enero'
    const entry = '2020-01-01'
    expect(parseDateFormat(entry, 'yyyy-MM-dd', "dd 'de' MMMM")).toEqual(
      response
    )
  })
})
