import { Breadcrumb, BreadcrumbComponent, BreadcrumbContent } from '@/types'

function getBreadcrumb(
  breadcrumbs: Record<BreadcrumbComponent, string>
): Breadcrumb {
  const initialBread = {} as Record<BreadcrumbComponent, BreadcrumbContent>

  const addBread = (
    accumulator: Record<string, BreadcrumbContent>,
    current: string[]
  ): Record<BreadcrumbComponent, BreadcrumbContent> => {
    const [component, nameBread] = current

    if (!accumulator[component]) {
      const bread = { name: component, bread: nameBread }
      accumulator[component] = bread
      return accumulator
    }

    return initialBread
  }

  const breadcrumb = Object.entries(breadcrumbs).reduce(addBread, initialBread)

  return { breadcrumb }
}

export { getBreadcrumb }
