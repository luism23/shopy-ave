/** Valida si es number */
export const isNumberAve = (value: string) => {
  return /(?=.*?[0-9])\w+/g.test(value)
}
