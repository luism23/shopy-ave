import { test, expect, describe } from 'vitest'
import { getFileIcon } from './'

describe('utils/getFileIcon.ts', () => {
  test('Return the icon file valid img jpeg', () => {
    const response = { type: 'image/jpeg', icon: 'file-image' }
    expect(getFileIcon('image/jpeg')).toMatchObject(response)
  })

  test('Return the icon file valid img png', () => {
    const response = { type: 'image/png', icon: 'file-image' }
    expect(getFileIcon('image/png')).toMatchObject(response)
  })

  test('Return the icon file valid doc pdf', () => {
    const response = { type: 'application/pdf', icon: 'file-pdf' }
    expect(getFileIcon('application/pdf')).toMatchObject(response)
  })

  test('Return the icon file valid csv', () => {
    const response = { type: 'text/csv', icon: 'file-csv' }
    expect(getFileIcon('text/csv')).toMatchObject(response)
  })
})
