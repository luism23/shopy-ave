import { format, parseISO, parse } from 'date-fns'
import { es } from 'date-fns/locale'

function formatDate(date: string, formatter = 'dd/MM/yyyy'): string {
  return format(parseISO(date), formatter)
}
function formatDay(date: string, formatter = "eeee d MMMM 'de' yyyy"): string {
  return format(parseISO(date), formatter, { locale: es })
}

function parseDateFormat(
  date: string,
  formatDate: string,
  formatTo: string
): string {
  return format(parse(date, formatDate, new Date()), formatTo, { locale: es })
}

export { formatDate, formatDay, parseDateFormat }
