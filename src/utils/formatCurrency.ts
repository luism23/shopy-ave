function formatCurrency({
  locales = 'es-CO',
  currency = 'COP',
  fractionDigits = 0,
  number = 0
}) {
  const formatted = new Intl.NumberFormat(locales, {
    style: 'currency',
    currency: currency,
    minimumFractionDigits: fractionDigits
  }).format(number)
  return formatted
}

export { formatCurrency }
