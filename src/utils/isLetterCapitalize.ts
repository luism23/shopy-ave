function isLetterCapitalize(value = ''): boolean {
  return /(?=.*?[A-Z])\w+/g.test(value)
}

export { isLetterCapitalize }
