import { test, expect, describe } from 'vitest'
import { generateCombi } from './'

describe('utils/generateCombi.ts', () => {
  test('Return the combination', () => {
    const entry = {
      listOne: [1, 2, 3],
      listTwo: [2],
      listthree: [3]
    }

    const response = [
      [3, 2, 1],
      [3, 2, 2],
      [3, 2, 3]
    ]

    expect(generateCombi(entry)).toEqual(response)
  })
})
