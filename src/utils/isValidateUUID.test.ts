import { test, expect } from 'vitest'
import { isValidateUUID } from './'

test('Return true if the uuid is valid', () => {
  const uuid = 'f9a3e6c0-8e1b-11e9-b0b6-0a58ac101f6d'
  expect(isValidateUUID(uuid)).toBe(true)
})

test('Return false if the uuid is invalid', () => {
  const uuid = 'test-hello'
  expect(isValidateUUID(uuid)).toBe(false)
})
