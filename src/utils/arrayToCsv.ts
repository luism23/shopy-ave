function arrayToCsv(
  data: any[],
  headers: any[],
  filename: string,
  readyCsv?: string
) {
  if (data.length > 0 && headers.length > 0) {
    const lineArray: any[] = []
    const lineHeader = headers.map((item: any) => {
      return item?.title.toUpperCase()
    })
    lineArray.push(lineHeader)
    const report = data.map((item: any) => {
      return headers.map((header: any) => {
        return item[header.name]
      })
    })

    report.map((item: any) => {
      lineArray.push(item.join(','))
      return item
    })
    const csvContent = lineArray.join('\n')
    const a: any = document.createElement('a')
    document.body.appendChild(a)
    a.style = 'display: none'
    const blob = new Blob([csvContent], { type: 'text/csv' })
    const url = window.URL.createObjectURL(blob)
    a.href = url
    a.download = `${filename}.csv`
    a.click()
    window.URL.revokeObjectURL(url)
  } else {
    const a: any = document.createElement('a')
    document.body.appendChild(a)
    a.style = 'display: none'
    const blob = new Blob([readyCsv], { type: 'text/csv' })
    const url = window.URL.createObjectURL(blob)
    a.href = url
    a.download = `${filename}.csv`
    a.click()
    window.URL.revokeObjectURL(url)
  }
}

export { arrayToCsv }
