import { test, expect, describe } from 'vitest'
import { getStorage, setStorage } from './'

describe('utils/localStorage.ts', () => {
  test('getStorage: Return the storage', () => {
    setStorage({ id: 'KEY_TEST', value: { test: 'VALUE_TEST' } })
    const response = { test: 'VALUE_TEST' }
    expect(getStorage('KEY_TEST')).toEqual(response)
  })
})
