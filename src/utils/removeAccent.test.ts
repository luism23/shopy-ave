import { test, expect, describe } from 'vitest'
import { removeAccent } from './'

describe('utils/removeAccent.ts', () => {
  test('Remove accent from a letter', () => {
    expect(removeAccent('á')).toEqual('a')
    expect(removeAccent('é')).toEqual('e')
    expect(removeAccent('í')).toEqual('i')
    expect(removeAccent('ó')).toEqual('o')
    expect(removeAccent('ú')).toEqual('u')
  })

  test('Remove accent from a word', () => {
    expect(removeAccent('árvíztűrőütvefúrógép')).toEqual('arvizturoutvefurogep')
  })
})
