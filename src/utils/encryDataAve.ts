/* eslint-disable camelcase */
/**
 * Generador de una cadena de numeros random
 * @param len tamaño de la cadena esperada
 * @param onlyNum si se desea solo numeros
 * @returns
 */
function generateRandomNumber(len = 18, onlyNum = true) {
  let text = ''
  const possible = onlyNum
    ? '0123456789'
    : 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  for (let i = 0; i < len; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }
  return text
}

/**
 * Cubre un id antes de ser enviado por http
 * @param id_self id que se desea ocultar
 * @returns
 */
function coverUpIdUser(id_self: string): string {
  const key = generateRandomNumber()
  const init = key.substring(0, 7)
  const fin = key.substring(7, 18)
  return `${init}${id_self}${fin}`
}

/**
 * Cubre un id antes de ser enviado por http
 * @param id_self id que se desea ocultar
 * @returns
 */
function coverUpIdEnterprise(id_self: string): string {
  const key = generateRandomNumber()
  const init = key.substring(0, 5)
  const fin = key.substring(5, 18)
  return `${init}${id_self}${fin}`
}

export { coverUpIdUser, coverUpIdEnterprise, generateRandomNumber }
