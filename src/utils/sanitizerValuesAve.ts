import { REGEX } from '@/constants/'
/**
 * Elimina caracteres extraños solo permitiendo alfanumericos.
 * Si el resultado no es alfanumerico retorna @type: false
 * @param value el valor a sanizar
 * @returns string | boolean
 */
export const sanitizerValuesAve = (value: string): string => {
  const strangeCharacters = REGEX.CARACTERES_EXTRAÑOS
  // const allowedCharacters = REGEX.ALFA_NUM;
  const sanitized = value.replace(strangeCharacters, '')
  return sanitized.replace(/  +/g, ' ')
  // if (allowedCharacters.test(sanitized)) {
  //   return sanitized;
  // }
}
