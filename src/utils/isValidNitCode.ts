import { from, map, reduce } from 'rxjs'

function isValidNitCode(nit: string, code: string): boolean {
  let state = false
  const prime = [41, 37, 29, 23, 19, 17, 13, 7, 3]

  from(nit)
    .pipe(
      map((num, index) => Number(num) * prime[index]),
      reduce((acc, temp) => acc + temp, 0),
      map((acumulation) => {
        const residue = acumulation % 11
        return residue > 1 ? 11 - residue : residue
      })
    )
    .subscribe((residue) => {
      state = String(code) === String(residue)
    })

  return state
}

export { isValidNitCode }
