import { Base64 } from '@/constants/'
/**
 * Convierte archivos File en base 64
 * @param file File de cualquier tipo
 */
export const base64Ave = async (file: File) =>
  new Promise<Base64>((resolve, reject) => {
    try {
      const image = window.URL.createObjectURL(file) // crea un DOMString que contiene una URL que representa al objeto pasado como parámetro.
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = () => {
        resolve({
          blob: file,
          image,
          base: reader.result
        })
      }
      reader.onerror = () => {
        resolve({
          blob: file,
          image,
          base: null
        })
      }
    } catch (err) {
      reject(err)
      return null
    }
  })
