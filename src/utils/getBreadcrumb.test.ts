import { test, expect, describe } from 'vitest'
import { getBreadcrumb } from './'

describe('utils/getBreadcrumb.ts', () => {
  test('Success mapping breadcrumbs valid structure', () => {
    const entry = { TheLayout: 'Inicio', CreateSale: 'Crear venta' }

    const response = {
      breadcrumb: {
        TheLayout: { name: 'TheLayout', bread: 'Inicio' },
        CreateSale: { name: 'CreateSale', bread: 'Crear venta' }
      }
    }

    expect(getBreadcrumb(entry)).toStrictEqual(response)
  })
})
