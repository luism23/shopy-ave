function toPrint(sectionPrint: string, type: string) {
  if (type === 'html') {
    const html = `<html>
                     ${document.getElementsByTagName('head')[0].innerHTML}
                     <body>
                     ${document.getElementById(sectionPrint)?.innerHTML}
                     </body>
                 </html>`
    const printWindow: any = window.open('', 'imprimir')
    printWindow.document.write(html)
    printWindow.document.close()
    printWindow.onload = () => {
      printWindow.focus()
      printWindow.print()
      printWindow.close()
    }
  }
}
export { toPrint }
