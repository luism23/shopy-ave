type Props = { isOptional: boolean }

const SIZE_IMAGES_BYTES = 5300000
const SIZE_DOC_BYTES = 3200000

const messageFileSize = 'Tamaño del archivo no corresponde al solicitado'

function getValidation(value: any, isOptional = false): boolean {
  if (value?.length) {
    switch (value[0].type) {
      case 'image/jpeg':
        return value[0].size <= SIZE_IMAGES_BYTES

      case 'image/jpg':
        return value[0].size <= SIZE_IMAGES_BYTES

      case 'application/pdf':
        return value[0].size <= SIZE_DOC_BYTES

      default:
        return false
    }
  }

  return isOptional
}

function isValidFileSize(value: any, props: Props): boolean {
  const { isOptional = false } = props
  return getValidation(value, isOptional)
}

export { isValidFileSize, messageFileSize }
