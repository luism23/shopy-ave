import { TOKEN_NAME, TokenPayload } from '@/constants/'

/**
 * Parseador del toekn
 */
const parseJWT = (token: string): TokenPayload | null => {
  try {
    return JSON.parse(atob(token.split('.')[1]))
  } catch (e) {
    return null
  }
}

const getLocalToken = (): string | null => {
  return localStorage.getItem(TOKEN_NAME)
}

const tokenMaskDimamic = (value: number | string, extend = false) => {
  const num = Number(String(value).replace(/\./g, ''))
  if (num > 999999999999 && extend) {
    return '#.###.###.###.###'
  }
  if (num > 99999999999 && extend) {
    return '###.###.###.###'
  }
  if (num > 9999999999 && extend) {
    return '##.###.###.###'
  }
  if (num > 999999999 && extend) {
    return '#.###.###.###'
  }
  if (num > 99999999 && extend) {
    return '###.###.###'
  }
  if (num > 9999999) {
    return '##.###.###'
  }
  if (num > 999999) {
    return '#.###.###'
  }
  if (num > 99999) {
    return '###.###'
  }
  if (num > 9999) {
    return '##.###'
  }
  if (num > 999) {
    return '#.###'
  } else {
    return '###.###.###.###'
  }
}

export { parseJWT, getLocalToken, tokenMaskDimamic }
