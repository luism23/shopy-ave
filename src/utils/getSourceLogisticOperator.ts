type Operator = { id: number; name: string }

const allOperators: Operator[] = [
  { id: 29, name: 'Envia' },
  { id: 33, name: 'Servientrega' },
  { id: 1010, name: 'TCC' },
  { id: 1016, name: 'Interrapidisimo' },
  { id: 1022, name: 'Saferbo' },
  { id: 1026, name: 'Domina' }
]

function getSourceLogisticOperator(identify: string | number): string {
  const operator = allOperators.find((value: Operator) => {
    if (typeof identify === 'string') {
      return identify.toLowerCase() === value.name.toLowerCase()
    }

    return identify === value.id
  })

  if (operator) {
    return `/assets/img/logistic/${operator.name.toLowerCase()}.png`
  }

  return ''
}

function getNameLogisticOperator(identify: string | number): string {
  const operator = allOperators.find((value: Operator) => {
    return identify === value.id || identify === value.name
  })

  if (operator) {
    return operator.name
  }

  return ''
}

function getIdLogisticOperator(identify: string | number): number {
  const operator = allOperators.find((value: Operator) => {
    return identify === value.id || identify === value.name
  })

  if (operator) {
    return operator.id
  }

  return 0
}

export {
  getSourceLogisticOperator,
  getNameLogisticOperator,
  getIdLogisticOperator,
  allOperators
}
