/**
 * GENERA UN STRING RAMDON
 * @param length > largo de la cadena esperada
 * @returns
 */
export const generarKeyAve = (length = 10) => {
  let result = ''
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}
