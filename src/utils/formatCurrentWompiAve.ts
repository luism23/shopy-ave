/** Dar el formato de dinero para wompi */
export const formatCurrentWompiAve = (value: string): number => {
  const valueSplit = value.split('.')
  return Number(valueSplit[0]) * 100
}
