function getFileFormData({
  bucket = '' as 'user' | 'company',
  file,
  filename,
  ...props
}: any) {
  const form: any = new FormData()
  form.append('bucket', bucket)
  form.append('filename', `${filename}.${file[0]?.name.split('.').pop()}`)
  form.append('file', file[0])

  if ([props].length > 0) {
    for (const [prop, value] of Object.entries(props)) {
      form.append(prop, value)
    }
  }

  return form
}

export { getFileFormData }
