import { IOrd } from '@/constants'

/**
 * Ordenar cualquier Objeto recibiendo el parametros por el que se quiera ordenar y el tipo de orden
 * @param data Cualquier arreglo
 * @param param el parametro por el que se quiera ordenar
 * @param ord Asc || Desc
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const orderAnyAve = (
  data: any[],
  param: string,
  ord: IOrd = 'Desc'
): void => {
  if (ord === 'Asc') {
    data.sort((a, b) => {
      if (Number(a[param]) && Number(b[param])) {
        return a[param] - b[param]
      }
      if (a[param] < b[param]) {
        return -1
      }
      if (a[param] > b[param]) {
        return 1
      }
      return 0
    })
  } else if (ord === 'Desc') {
    data.sort((a, b) => {
      if (Number(a[param]) && Number(b[param])) {
        return b[param] - a[param]
      }
      if (a[param] < b[param]) {
        return 1
      }
      if (a[param] > b[param]) {
        return -1
      }
      return 0
    })
  }
}
