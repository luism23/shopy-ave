function arrayUnique(array = [], key = '') {
  if (key) {
    return array.filter((value, index, cloneArray) => {
      const findIndex = cloneArray.findIndex((element) => {
        return element?.[key] === value?.[key]
      })

      return findIndex === index
    })
  }

  return array.filter((value, index, cloneArray) => {
    const findIndex = cloneArray.findIndex((element) => element === value)
    return findIndex === index
  })
}
function arrayUnique2(array = [], key = '') {
  if (key) {
    return array.filter((value, index, cloneArray) => {
      const findIndex = cloneArray.findIndex((element) => {
        return element?.[key] === value?.[key]
      })

      return findIndex === index
    })
  }

  return array.filter((value, index, cloneArray) => {
    const findIndex = cloneArray.findIndex((element) => element === value)
    return findIndex === index
  })
}

export { arrayUnique, arrayUnique2 }
