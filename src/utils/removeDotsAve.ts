export const removeDots = (strNumber: string) => {
  const spt = strNumber.split('.')
  return Number(spt.join(''))
}
