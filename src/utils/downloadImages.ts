async function downloadImages(listImage: any, index: any) {
  for (const imageSrc of listImage) {
    const link = document.createElement('a')
    const nameImages = imageSrc.split('/')
    link.href = URL.createObjectURL(await (await fetch(imageSrc)).blob())
    link.download = nameImages[index]
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
  }
}

export { downloadImages }
