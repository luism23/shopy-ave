import { IconFile, IconFileEntry } from '@/types'

function getFileIcon(type: IconFileEntry): IconFile | undefined {
  if (['image/jpeg', 'image/png', 'jpeg', 'jpg', 'png'].includes(type)) {
    return { type, icon: 'file-image' }
  }
  if (['application/pdf', 'pdf'].includes(type)) {
    return { type, icon: 'file-pdf' }
  }
  if (['text/csv', 'csv'].includes(type)) {
    return { type, icon: 'file-csv' }
  }
  return { type, icon: 'file-alt' }
}

export { getFileIcon }
