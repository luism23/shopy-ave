import { IconPack } from '@fortawesome/fontawesome-svg-core'

import { fab } from './brands'
import { fal } from './light'
import { far } from './regular'
import { fa } from './solid'

const allIcons: IconPack = { ...fa, ...far, ...fal, ...fab }

export { allIcons }
