import type { IconPack } from '@fortawesome/free-brands-svg-icons'
import {
  faWhatsapp as fabWhatsapp,
  faWhatsappSquare as fabWhatsappSquare
} from '@fortawesome/free-brands-svg-icons'

const fab: IconPack = { fabWhatsapp, fabWhatsappSquare }

export { fab }
