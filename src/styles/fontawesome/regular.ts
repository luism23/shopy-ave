import type { IconPack } from '@fortawesome/pro-regular-svg-icons'
import {
  faCheckCircle as farCheckCircle,
  faSearch as farSearch,
  faExclamationCircle as farExclamationCircle,
  faTimes as farTimes
} from '@fortawesome/pro-regular-svg-icons'

const far: IconPack = {
  farSearch,
  farCheckCircle,
  farExclamationCircle,
  farTimes
}

export { far }
