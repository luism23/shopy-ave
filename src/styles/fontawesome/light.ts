import type { IconPack } from '@fortawesome/pro-light-svg-icons'
import {
  faFileDownload as falFileDownload,
  faCheck as falCheck,
  faTimes as falTimes
} from '@fortawesome/pro-light-svg-icons'

const fal: IconPack = { falFileDownload, falCheck, falTimes }

export { fal }
