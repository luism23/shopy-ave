interface NotificationCompany {
  id: string
  name: string
  description: string
  status: boolean
  idCompany: string
  isCheck: boolean
}

export { NotificationCompany }
