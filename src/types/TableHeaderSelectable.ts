export interface TableHeaderSelectable {
  field: string
  label: string
  align: 'left' | 'right' | 'center'
}
