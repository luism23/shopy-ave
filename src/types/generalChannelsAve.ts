export interface IdataCanales {
  id: number
  ds: string
  idactivo: string
  fecha: string
}

export interface IguadarCanales {
  ds: string
  idactivo: string
}

export interface IGuardarCanales {
  ds: string
  idactivo: string
}

export interface IEditarCanales {
  ds: string
  idactivo: string
}

export interface IRespGeneAveData {
  message: string
  data: IdataCanales[]
  status: 'ok' | 'error'
}

export type ModesChannels = 'Created' | 'Edit' | 'Load'
export type TypeIconOrder = 'chevron-down' | 'chevron-up'
