type Country = {
  id: number
  name: string
}
type City = {
  id: number
  description?: string
}
type Location = {
  country?: Country
  city?: City
}

/* eslint-disable camelcase */
type DispatchCenter = {
  name: string
  contact_name: string
  status: boolean
  mobile: string
  location: Location
  address: string
  note: string
  is_default: boolean
  document_number: number
}

interface DispatchCenterForm extends Omit<DispatchCenter, 'location'> {
  country_name: string
  id_city: number
}
interface DispatchCenterList extends Omit<DispatchCenter, 'status'> {
  id: string
}

interface DispatchCenterOne extends DispatchCenter {
  id: string
}

export {
  DispatchCenter,
  DispatchCenterList,
  DispatchCenterForm,
  DispatchCenterOne
}
