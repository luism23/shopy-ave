/* eslint-disable camelcase */
interface ISendMsg {
  enterprise: string
  userEnterprise: string
}

interface IResp_SendMsg {
  status: string
  bodyToken: string
  exp: number
  phone: string
  mail: string
}

interface IBankPayDataAccount {
  identificacionBeneficiario: string
  nombreDelBeneficiario: string
  bancoCuentaDelBeneficiarioDestino: string
  numeroCuentaDelBeneficiario: string
  tipoDeTransaccion: number
  valorTransaccion: string
  tipoDeDocumentoDeIdentificacion: number
  oficinaDeEntrega?: string
  email?: string
}

interface IBankSendPays {
  code: string
  bodyToken: string
  body: IBankPayDataAccount[]
}

interface IFavoriteAccount {
  body: [
    {
      codigoConfirmacion: number
      bancoCuentaDelBeneficiarioDestino: string
      numeroCuentaDelBeneficiario: string
      bodyToken: string
    }
  ]
}

interface IBankSusbcriptDataAccount {
  identificacionBeneficiario: string
  nombreDelBeneficiario: string
  bancoCuentaDelBeneficiarioDestino: string
  numeroCuentaDelBeneficiario: string
  tipoDeTransaccion: number
  tipoDeDocumentoDeIdentificacion: number
}

interface IBankSendSusbcripts {
  body: IBankSusbcriptDataAccount[]
}

interface IRespListSusbcripts {
  identificacionBeneficiario: string
  nombreDelBeneficiario: string
  tipoDeDocumentoDeIdentificacion: number
  numeroCuentaDelBeneficiario: number
  tipoDecuenta: number
  nombreBanco: string
  bancoCuentaDelBeneficiarioDestino: string
  cuentaPorDefecto: boolean
}

export {
  ISendMsg,
  IResp_SendMsg,
  IBankPayDataAccount,
  IBankSendPays,
  IFavoriteAccount,
  IBankSendSusbcripts,
  IRespListSusbcripts
}
