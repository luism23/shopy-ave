/* eslint-disable camelcase */
type PaymentInformation = {
  id: string
  id_bank: string
  id_account_type: string
  account_number: number
  account_holder: string
}

type OrderCondition = {
  id_insurance_value: string | undefined
  id?: string | undefined
  refund_description?: string | undefined
  is_delivery_payment?: boolean | undefined
  is_electronic_payment?: boolean | undefined
  payment_information?: PaymentInformation | undefined
  payment_information_description?: string | undefined
}

type Insurance = {
  id: string
  description: string
}

export { OrderCondition, Insurance }
