export interface InoDeliveryNovalty {
  dsconsec: string
  idexp: number
  dsfecha: string
  idestado: string
  fechaUltimoEstado: string
  nombreEstado: string
  dstipotrayecto: string
  idtransportador: string
  dstransportadora: string
  diasSinMovimiento: string
  empresa: string
}
