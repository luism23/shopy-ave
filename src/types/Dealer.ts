type Dealer = {
  id: string
  nickname: string
  email: string
  idCampaign: string
  idPriceList: string
}
interface DealerResponse {
  page?: number
  perPage?: number
  status?: string | undefined
  search?: string | undefined
  date?: string | undefined
  city?: string | undefined
  idListPrice?: string | undefined
}

export { Dealer, DealerResponse }
