export interface IlistTranspo {
  id: string | number
  name: string
  status: boolean
}

export interface Transporter {
  id: number
  text: string
}

export interface TransportSelected {
  id: number | string
  name: string
  status: boolean
}
