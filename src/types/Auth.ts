type ApiKeyResponse = {
  platform: string
  token: string
}

export { ApiKeyResponse }
