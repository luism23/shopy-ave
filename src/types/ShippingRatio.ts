import { Pagination } from './Pagination'

interface PaginationShippingRatio extends Pagination {
  tipo?: string
  idCompany: string
  dateStart: string
  dateEnd: string
  logisticOperator?: Array<string>
  searchParameter?: string
}

interface PaginationShippingRatioGuidesCreate extends Pagination {
  tipo?: string
  idAgent: string
  dateStart: string
  dateEnd: string
  logisticOperator: string
  searchParameter?: string
}

export { PaginationShippingRatio, PaginationShippingRatioGuidesCreate }
