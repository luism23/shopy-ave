/* eslint-disable camelcase */
type City = {
  name: string
}

interface CityDepartment {
  id: number
  description: string
}

interface CityByIDResponse {
  id: number
  name: string
  id_department: number
  full_name: string
}

export { City, CityDepartment, CityByIDResponse }
