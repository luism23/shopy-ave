type Product = {
  alto: number
  largo: number
  ancho: number
  peso: number
  unidades: number
  valorDeclarado: number
}

type QuoteRequest = {
  tipo?: 'cotizar'
  idempresa: number
  origen: number
  destino: number
  idasumecosto: boolean
  valorrecaudo: number
  contraentrega: boolean
  valorMinimo: boolean
  productos: Product[]
}

type QuoteResponse = {
  codigotransportadora: '29' | '1010' | '1022' | '33' | '999' | ''
  transportadora: string
  logotransportadora: string
  origen: string
  destino: string
  unidades: string
  kilosacobrar: number
  valoracion: string
  porvaloracion: string
  trayecto: string
  codtrayecto: string
  tipoenvio: string
  fletexkilo: number
  fletexund: number
  fletetotal: number
  diasentrega: string
  flete: number
  costomanejo: number
  valortotal: number
  valorotros: string
  grantotal: number
  codmostrartransportadora: string
  topemaximo: string
  dscontraentrega: number
  alto: number
  largo: number
  ancho: number
}

export { QuoteRequest, QuoteResponse }
