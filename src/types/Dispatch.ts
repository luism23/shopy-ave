/* eslint-disable camelcase */
type Dispatchs = {
  id: string
  date_created: string
  status: string
  customer: string
  sale_number: number
  logistic_operator: string
  destination: string
  in_charge: string
}

type DispatchsStatusOptions = {
  id: number
  title: number
  subtitle: string
  name: string
  percentage: number
}

type ResponseDispatch = {
  total_pages: number
  dispatchs: Dispatchs[]
  status_options: DispatchsStatusOptions[]
}

type QueryParamsDispatchs = {
  page?: number
  perPage?: number
  status?: string
  from: string
  to: string
}

export {
  ResponseDispatch,
  QueryParamsDispatchs,
  Dispatchs,
  DispatchsStatusOptions
}
