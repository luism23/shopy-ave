type SubModule = {
  id: string
  name: string
  status: boolean
}

export { SubModule }
