/* eslint-disable camelcase */
type Customer = {
  name: string
  idDocumentType: string
  documentNumber: number
  mobile: string
  idCountry: string
  email: string
  idCity: number
  address: string
  idCampaign: string
  idPriceList: string
  note: string
}

interface CustomerIDResponse {
  id: string
  name: string
  id_document_type: string
  document_number: number
  mobile: string
  email: string
  id_city: number
  address: string
  note: string
  id_company: string
  id_campaign: string
  id_price_list?: string
}

interface CustomerSearchResponse {
  id: string
  full_name: string
}

export { Customer, CustomerSearchResponse, CustomerIDResponse }
