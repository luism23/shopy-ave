import { Pagination } from './Pagination'

interface SalePagination extends Pagination {
  status: string
  to?: string
  from?: string
  search: string
  carrier: string
  destination: string
  product: string
}

export { SalePagination }
