/* eslint-disable camelcase */
interface Country {
  name: string
}

interface Countries extends Country {
  id: number | string | undefined
}

interface CountryResponse {
  total_pages: number
  countries: Countries[]
}

export { Country, Countries, CountryResponse }
