/* eslint-disable camelcase */
export interface IdataNovalty {
  Des_Novedad: string
  asesor: string
  cod_novedad: number
  dias_transcurridos: number
  dsaclaracion: string
  dsciudad: string
  dsciudadd: string
  dscomentario: string
  dsconsec: string
  dsdird: string
  dsestado: string
  dsfecha: string
  dsfechalarga: string
  dsnombred: string
  dsteld: string
  dstipotrayecto: string
  dstransportadora: string
  empresa: string
  fecha_novedad: string
  idagente: number
  idestado: string
  idexp: number
  idtransportador: string
  pkid: number
  total_novedades: number
}
