/* eslint-disable camelcase */
import { ResponseAveonline } from './Aveonline'

type Company = {
  name: string
  idStatus: string
  idDocumentType: string
  documentNumber: number
}

type CompanyValidateAppRequest = {
  tipo: string
  documento: string
}

type CompanyValidateAppResponse = {
  status: 'VEDADO' | 'EXISTE' | 'NO_EXISTE' | 'NUEVO'
  message: string
}

type CompanyCredentialRequest = {
  tipo: string
  usuario: string
  clave: string
}

type CompanyCredentialDataResponse = {
  idEmpresa: number
  razonSocial: string
  nombre: string
  identificacion: string
  acronimoTipoDocumento: 'NIT' | 'CC' | 'CE'
  codigoDane: string
  telefono: string
  correo: string
}

type CompanyCredentialResponse = {
  status: 'ok' | 'error'
  empresa: CompanyCredentialDataResponse
}

type MyCompany = {
  id: string
  name: string
}

type AccountHolder = {
  document_number: number
  email: string
  id_city_company: number
  id_document_type: string
  last_name: string
  name: string
  phone: string
}

type CompaniesResponse = {
  company: MyCompany
  status: boolean
  role_name: string
  account_holder: AccountHolder
  is_default: string
}

interface CompanyResponseAveonline extends ResponseAveonline {
  idEmpresa: number | string
}

interface RequestCreateCompanyAveonline {
  tipo?: string
  nombre?: string
  telefono?: string
  correo?: string
  documento?: string
  tipoDocumento?: 'CC' | 'CE' | 'NIT' | string
  nombreComercial?: string
  codigoDane?: string
  direccion?: string
  tipoOrganizacion?: number
  responsabilidadTributaria?: number
  tipoRegimen: number
  tipoMercancia: string
}

interface CompanyInfo {
  id: string
  name: string
  id_status: string
  id_city: number
  id_document_type: string
  document_number: number
  id_company_aveonline: number
  is_dropshipper: boolean
  url_avatar: string
}

interface DatosFacturacion {
  correo: string[]
  ciudad: string
  regimen: string
  organizacion: string
  codigoCiudad: string
  idResponsabilidadTributaria: number
  responsabilidad: string
  celular: string
  plazoPago: string
  diasFacturacionFlete: number
  direccion: string
  fechaProximaFactura: string
}

interface InfoGeneral {
  razonSocial: string
  nit: string
  celular: string
  correo: string[]
  representanteLegal: string
  cedulaRepresentante: string
  estado: string
  mercancia: string
}

interface Plan {
  id: number
  valor: string
  nombre: string
  frecuencia: number
}

interface CompanyAveonlineInfo {
  infoGeneral: InfoGeneral
  datosFacturacion: DatosFacturacion
  plan: Plan
}

interface CompanyAveonlineFormEdit {
  idEmpresa: number
  celular: string
  mercancia: string
}

interface CompanyAveonlineBillingFormEdit {
  idEmpresa: number
  correo: string
  codigoDane: number
  direccion: string
}

interface CancelPlanAveonlineForm {
  idEmpresa: string
  motivo: string
}

export {
  RequestCreateCompanyAveonline,
  Company,
  CompanyValidateAppRequest,
  CompanyValidateAppResponse,
  CompanyCredentialRequest,
  CompanyCredentialResponse,
  CompaniesResponse,
  CompanyResponseAveonline,
  CompanyInfo,
  CompanyAveonlineInfo,
  CompanyAveonlineFormEdit,
  CompanyAveonlineBillingFormEdit,
  CancelPlanAveonlineForm,
  AccountHolder
}
