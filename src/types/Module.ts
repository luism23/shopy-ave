import { SubModule } from './SubModule'

type Module = {
  id: string
  name: string
  status: boolean
  subModules: SubModule[]
}

export { Module }
