/* eslint-disable camelcase */

interface IGetResultPayWompi {
  tipo: 'getResposeId'
  // eslint-disable-next-line camelcase
  id_wompi: string
  // eslint-disable-next-line camelcase
  env_test?: boolean
}

interface IRespGeneAve {
  message: string
  status: 'ok' | 'error'
}

interface IResultPayWompiResp extends IRespGeneAve {
  data: [
    {
      numero_factura: string
      status: string
      total_wompi: string
    }
  ]
}

export { IGetResultPayWompi, IResultPayWompiResp, IRespGeneAve }
