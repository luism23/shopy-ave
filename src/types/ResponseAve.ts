import { IResponseAllDataRecaudos } from '@/constants/TypesGenerales'

interface IResponseAveRecaudos {
  status: string
  data: IResponseAllDataRecaudos
}

interface ErrorRespose {
  error: string
  message: string | string[]
}

export { IResponseAveRecaudos, ErrorRespose }
