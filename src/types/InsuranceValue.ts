type InsuranceValue = {
  id: string
  description: string
}

export { InsuranceValue }
