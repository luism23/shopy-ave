type DiscountsRange = {
  lower: number
  upper: number
}

type DiscountsFreight = {
  id: string
  idOrderCondition: string
  discountsRange: DiscountsRange
  percent: number
}

export { DiscountsFreight }
