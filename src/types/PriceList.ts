type PriceList = {
  id: string
  name: string
  CreatedAt: Date
  TypeClient: object
  totalSold: number
  profit: number
  clientNumber: number
  isActive: boolean
}

export { PriceList }
