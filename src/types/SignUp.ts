type SignUpRequest = {
  email: string
  password: string
  type?: 1 | 2 | 3
}

export { SignUpRequest }
