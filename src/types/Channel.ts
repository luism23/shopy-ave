type Channel = {
  id: string
  name: string
  isActive: boolean
}

interface ChannelResponse {
  page?: number
  perPage?: number
  isActive?: boolean | string | undefined
  search?: string | undefined
}

export { Channel, ChannelResponse }
