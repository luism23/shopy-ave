import { Module } from './Module'

type Role = {
  id: string
  name: string
  status: boolean
  description: string
  modules: Module[]
}

export { Role }
