import { Pagination } from '@/types'
interface Filters extends Pagination {
  search?: string
  channelName?: Array<string>
  isActive?: boolean | undefined
}

export { Filters }
