type BreadcrumbContent = { name: string; bread: string }

type BreadcrumbComponent =
  | 'TheLayout'
  | 'Dispatch'
  | 'CreateDispatch'
  | 'Inventory'
  | 'InventoryManager'
  | 'CreateProduct'
  | 'Pickup'
  | 'ShippingRatio'
  | 'StepsShippingRatio'
  | 'CreateSale'
  | 'Channel'
  | 'TabPriceList'
  | 'Discounts'
  | 'Customer'
  | 'Dealer'
  | 'Dropshipping'
  | 'Suppliers'
  | 'MySales'
  | 'CreatedCustomer'
  | 'CreateDiscount'
  | 'CreatePriceList'
  | 'UpdatePriceList'
  | 'PaymentLink'
  | 'MyTreasury'
  | 'AccountStatus'
  | 'AccountStatusAve'
  | 'CollectGuides'
  | 'Campaing'
  | 'NewsAve'
  | 'DiscountAve'
  | 'DiscountCrudAve'
  | 'referidos'
  | 'conciliaciones'
  | 'ChannelCampain'
  | 'CampaingAve'
  | 'ChannelAve'
  | 'NewsChargeAve'

type Breadcrumb = Record<
  'breadcrumb',
  Record<BreadcrumbComponent, BreadcrumbContent>
>

export { BreadcrumbComponent, BreadcrumbContent, Breadcrumb }
