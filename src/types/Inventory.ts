type Inventory = {
  id: string
  reference: string
  avaliableUnits: number
  stock: number
  cost: number
  price: number
  location: string
  isActive: boolean
  attributes: object
}

export { Inventory }
