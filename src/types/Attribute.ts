export interface Attribute {
  id?: string
  name: string
}
export interface AttributeValue {
  id?: string
  value: string
}

export interface AttributeItem extends Attribute {
  values: AttributeValue[]
}
