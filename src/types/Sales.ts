/* eslint-disable camelcase */
import {
  OrderCondition,
  StateProductSaleByCategory,
  AppTableColumn
} from '@/types'
import { ComputedRef } from 'vue'

type CartAttribute = {
  attribute1?: Record<string, any>
  attribute2?: Record<string, any>
  attribute3?: Record<string, any>
}

type FilterProductForDispatchCenter = {
  productID: string
  stock: number
  measures?: {
    width: number
    height: number
    length: number
    weight: number
  }
  dispatchCenter: {
    id: string
    name: string
    address: string
    city: {
      id: string
      name: string
    }
  }
}

type FilterResponseAttributes = Omit<
  FilterProductForDispatchCenter,
  'productID'
> & {
  id: string
}

type CartProduct = {
  id: string | number
  name: string
  reference: string
  selectedUnits: number
  isAddProduct?: boolean
  isVariable?: boolean
  description?: string
  img?: string
  discount: {
    value: number
    type: number
  }
  totalAmount: {
    withDiscount: number
    notDiscount: number
  }
  attributes: CartAttribute
  filter?: FilterProductForDispatchCenter[]
}

type CartProductListObject = Record<string, CartProduct>

type SummaryProduct = Omit<CartProduct, 'discount'>

type SaleCollection = {
  isByCashDelivery: boolean
  isByElectronic: boolean
}

type Methods = 'WIRE_TRANSFER' | 'MONEY' | 'CREDIT' | ''

type PaymentMethods = {
  id: Methods
  name: string
}

type QuoteCardTable = {
  selected: string
  showMore: boolean
  dispatch: any
  products: CartProduct[]
  productsSelected: ComputedRef<any>
  totalProducts: ComputedRef<any>
  headers: AppTableColumn[]
  quotes: any[]
}

type SaleFormCustomer = {
  id: string
  name: string
  email: string
  phone: number | undefined
  campaign: { id: string; name: string }
  country: { id: number; name: string }
  cityID: undefined
}

type SaleProducts = {
  searchCategory: string
  byCategory: StateProductSaleByCategory[]
  product: CartProductListObject | any
  selected: (CartProduct | undefined)[]
  orderCondition: OrderCondition
}

type SalePayment = {
  isCashDelivery: boolean
  isOther: boolean
  isElectronic: boolean
  isDeliveryPointSale: boolean
  isShippingCostDestiny: boolean
  otherMethodID: Methods
  collection: SaleCollection
  paymentsMethods: PaymentMethods[]
}

type SaleInfoQuote = {
  list: QuoteCardTable[]
}

type SaleCollectedValue = {
  total: number
  description: string
}

type SaleQuote = {
  daysShippingTime: number
  totalOrder: number
  paymentCost: number
  subtotal: number
  IVA: number
  totalQuote: number
}

export {
  CartProduct,
  CartProductListObject,
  SummaryProduct,
  SaleFormCustomer,
  SalePayment,
  CartAttribute,
  SaleQuote,
  FilterResponseAttributes,
  SaleProducts,
  SaleInfoQuote,
  SaleCollectedValue
}
