/**
 *  1: 'PLAN BASICO'
 *  4: 'PLAN GRATUITO'
 *  5: 'PLAN PREMUIM'
 *  1: 'valorMensual'
 *  2: 'valorSemestral'
 *  3: 'valorAnual'
 */

type PricingFrequency = {
  id: 1 | 2 | 3
  valor: string
  nombre: string
}

type PricingPropList = {
  id: 1 | 4 | 5
  nombre: string
  frecuencia: PricingFrequency[]
}

type PricingResponseList = {
  status: 'ok' | 'error'
  message: string
  data: PricingPropList[]
}

type PricingRequestList = {
  tipo: string
}

type RequestUpdatePlan = {
  tipo?: string
  idPlan: string
  periodoPago: string
  idEmpresa: string
}

type Plan = {
  planID: number
  frequencyID: number
}

export {
  PricingRequestList,
  PricingResponseList,
  PricingPropList,
  RequestUpdatePlan,
  Plan
}
