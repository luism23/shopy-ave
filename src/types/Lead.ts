interface RequestLeadAveonline {
  tipo: string
  nombre: string
  correo: string
  telefono: string
}

export { RequestLeadAveonline }
