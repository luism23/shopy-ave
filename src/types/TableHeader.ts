export interface TableHeader {
  name: string
  align: 'left' | 'right' | 'center'
  title: string
  sorted?: boolean
  width?: string
}
