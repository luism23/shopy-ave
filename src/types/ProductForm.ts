export type ProductForm = {
  typeProduct: string
  name: string
  sku: string
  categoryId: string
  daysEnlistment: number | undefined
  cost: number | undefined
  price: number | undefined
  status: boolean
  isWebsite: boolean
  description: string
  refundDescription: string
  dispatchCenterId: string
  isSendingProvider: boolean
  packingUnits: [
    {
      width: number | undefined
      height: number | undefined
      length: number | undefined
      weight: number | undefined
    }
  ]
}
