interface ResponseAveonline {
  status: 'ok' | 'error'
  message?: string
}

export { ResponseAveonline }
