/* eslint-disable camelcase */
export interface IdataCampana {
  ds_canal: string
  ds_servicio: string
  fecha: string
  id: number
  idactivo: number
  idcanal: number
  idservicio: number
  inversion: number
  nombre: string
  roi: number
  total_clientes: number
}

export interface IguadarCampana {
  ds_canal: string
  ds_servicio: string
  fecha: string
  id: number
  idactivo: number
  idcanal: number
  idservicio: number
  inversion: number
  nombre: string
  roi: number
  total_clientes: number
}

export interface IRespGeneAveData {
  message: string
  data: IdataCampana[]
  status: 'ok' | 'error'
}

export type ModesCampaing = 'Created' | 'Edit' | 'Load'
export type TypeIconOrder = 'chevron-down' | 'chevron-up'

export interface InfoFecha {
  init: string
  final: string
}
