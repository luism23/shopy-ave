type Campaign = {
  id: string
  dateStart: string
  dateEnd: string
  name: string
  isActive: boolean
  idChannel: string
}

export { Campaign }
