type TributationGeneral = {
  id: number
  name: string
}

type ResponseTributation = {
  status: 'ok' | 'error'
  regimen: TributationGeneral[]
  organizacion: TributationGeneral[]
  responsabilidad: TributationGeneral[]
}

export { TributationGeneral, ResponseTributation }
