type FileUpload = {
  type: string
  name: string
  file: string
}

export { FileUpload }
