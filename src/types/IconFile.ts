type IconFileEntry =
  | 'image/jpeg'
  | 'image/png'
  | 'application/pdf'
  | 'text/csv'
  | 'jpeg'
  | 'jpg'
  | 'png'
  | 'csv'
  | 'pdf'
  | ''

type IconFile = {
  type: IconFileEntry
  icon: 'file-image' | 'file-image' | 'file-pdf' | 'file-csv' | 'file-alt'
}

export { IconFile, IconFileEntry }
