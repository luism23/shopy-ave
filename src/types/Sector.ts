type Sector = {
  id: string
  name: string
}

export { Sector }
