type AccountBankType = {
  id: string
  name: string
  acronym: string
}

export { AccountBankType }
