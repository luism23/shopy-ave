import { TypeDocument } from './'
/* eslint-disable camelcase */
type User = {
  documentNumber: number
  mobile: string
  name: string
  lastName: string
  gender: string
  idDocumentType: string
}

type RoleAssign = {
  // eslint-disable-next-line camelcase
  id_dispatch_center: string
  // eslint-disable-next-line camelcase
  id_campaign: string
  // eslint-disable-next-line camelcase
  id_price_list: string
  email: string
  name: string
  nickname: string
  // eslint-disable-next-line camelcase
  ids_ispatch_center: [string]
}

interface UserProfile {
  document_number: number
  mobile: string
  name: string
  last_name: string
  email: string
  document_type: TypeDocument
  status: string
  url_profile: string
  role_name: string
}

interface UserProfileFormEdit {
  name: string
  last_name: string
  mobile: string
  email: string
  id_document_type: string
  document_number: number
}

export { User, RoleAssign, UserProfile, UserProfileFormEdit }
