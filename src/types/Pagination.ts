/* eslint-disable camelcase */
interface Pagination {
  page: number
  per_page: number
}

export { Pagination }
