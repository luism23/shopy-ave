export interface HeaderAmountItem {
  title: string
  amount: string
}
