type SendEmail = {
  email: string
  type: 1 | 2
}

export { SendEmail }
