const ListChart = {
  options: [
    {
      id: 'Lista',
      icon: 'list'
    },
    {
      id: 'Gráfico',
      icon: 'chart-pie'
    }
  ],
  position: 'top-center',
  currentID: 'Lista'
}

const ListKanban = {
  options: [
    {
      id: 'Lista',
      icon: 'list'
    },
    {
      id: 'Kanban',
      icon: 'line-columns'
    }
  ],
  position: 'top-center',
  currentID: 'Lista'
}

const ListCalendarChart = {
  options: [
    {
      id: 'Lista',
      icon: 'list'
    },
    {
      id: 'Calendario',
      icon: 'calendar'
    },
    {
      id: 'Gráfico',
      icon: 'chart-pie'
    }
  ],
  position: 'top-center',
  currentID: 'Lista'
}

const ListFunnelChart = {
  options: [
    {
      id: 'Lista',
      icon: 'list'
    },
    {
      id: 'filtro',
      icon: 'funnel'
    },
    {
      id: 'Gráfico',
      icon: 'chart-pie'
    }
  ],
  position: 'top-center',
  currentID: 'Lista'
}

const ListKanbanChart = {
  options: [
    {
      id: 'Lista',
      icon: 'list'
    },
    {
      id: 'Kanban',
      icon: 'line-columns'
    },
    {
      id: 'Gráfico',
      icon: 'chart-pie'
    }
  ],
  position: 'top-center',
  currentID: 'Lista'
}

export {
  ListChart,
  ListKanban,
  ListCalendarChart,
  ListFunnelChart,
  ListKanbanChart
}
