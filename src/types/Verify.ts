type Verify = {
  type: string
  email: string
  code: string
}

type TypesVerify = '1' | '2' | '3'

type PropsVerify = {
  code: string
  type: string | number
  email: string
  enterprise?: string
  role?: string
}

export { Verify, TypesVerify, PropsVerify }
