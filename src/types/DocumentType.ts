/* eslint-disable camelcase */
type TypeDocument = {
  id: string
  name: string
  acronym: string
}

type DocumentTypeResponse = {
  total_pages: number
  document_types: TypeDocument[]
}

export { DocumentTypeResponse, TypeDocument }
