/* eslint-disable camelcase */
type MenuChildren = {
  name: string
  path_name: string
}

type Menu = {
  id?: string
  display?: string
  name: string
  path_name?: string
  icon_name: string
  children?: MenuChildren[]
}

type LoginRequest = {
  email: string
  password: string
}

type LoginResponse = {
  role: string
  name: string
  is_company_default?: boolean
  menu: Menu[]
}

type ChangeSessionResponse = Omit<LoginResponse, 'is_company_default'>

export {
  LoginRequest,
  LoginResponse,
  Menu,
  ChangeSessionResponse,
  MenuChildren
}
