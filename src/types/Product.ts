/* eslint-disable camelcase */
import { Inventory } from './Inventory'

type PakingUnits = {
  width: number
  high: number
  length: number
  weight: number
}

type Product = {
  idProductType: string
  name: string
  reference: string
  urlVideo: string
  daysEnlistment: number
  isOverselling: boolean
  isSoldMarketplace: boolean
  isShowWebsite: boolean
  description: string
  refundDescription: string
  cost: number
  idCategory: string
  idAttributes: string[]
  products: []
  pakingUnits: PakingUnits[]
  inventory: Inventory[]
}

interface ProductSale {
  id: string
  stock: number
  name: string
  reference: string
  price: number
  id_category?: string
  discount?: number
  img: string
  description?: string
  is_variable?: boolean
  attributes: Record<string, any>
}

type ProductByCategory = {
  id: string
  name: string
  products: ProductSale[]
}

interface StateProductSale extends ProductSale {
  attribute1: any[]
  attribute2: any[]
  attribute3: any[]
}

type StateProductSaleByCategory = {
  id: string
  name: string
  products: StateProductSale[]
}

type ProductCardSale = Omit<StateProductSale, 'attributes'>

export {
  Product,
  PakingUnits,
  ProductByCategory,
  ProductSale,
  StateProductSaleByCategory,
  ProductCardSale
}
