type UserInfo = {
  role: string
  name: string
}

export { UserInfo }
