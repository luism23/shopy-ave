interface AppTableColumn {
  label: string
  field: string
  sortable?: boolean
  width?: string
  align?: 'left' | 'center' | 'right'
  size?: 'base' | 'sm'
}

export { AppTableColumn }
