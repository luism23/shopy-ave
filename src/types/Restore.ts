type Restore = {
  email: string
  password: string
  code: string
}

export { Restore }
