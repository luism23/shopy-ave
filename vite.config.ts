import { defineConfig } from 'vitest/config'
import vue from '@vitejs/plugin-vue'
import alias from '@rollup/plugin-alias'

export default defineConfig({
  base: './',
  plugins: [
    vue(),
    alias({
      entries: [
        { find: '@', replacement: '/src' },
        { find: '@components', replacement: '/src/components' },
        { find: '@composables', replacement: '/src/composables' },
        { find: '@router', replacement: '/src/router' },
        { find: '@services', replacement: '/src/services' },
        { find: '@types', replacement: '/src/types' },
        { find: '@utils', replacement: '/src/utils' }
      ]
    })
  ],
  test: {
    globals: true,
    environment: 'happy-dom',
    includeSource: ['src/**/*.{js,ts}']
  },
  server: {
    host: '127.0.0.1',
    port: 3000
  },
  build: {
    emptyOutDir: true,
    outDir: './www',
    assetsDir: 'assets',
    cssCodeSplit: true,
    chunkSizeWarningLimit: 1500,
    reportCompressedSize: true,
    minify: 'esbuild',
    ssr: false,
    rollupOptions: {
      output: {
        entryFileNames: 'assets/[hash].js',
        chunkFileNames: 'assets/[hash].js',
        assetFileNames: 'assets/[hash].[ext]'
      }
    }
  }
})
