const path = require('path')
const { loadConfigFromFile, mergeConfig } = require('vite')
const alias = require('@rollup/plugin-alias')

module.exports = {
  stories: ['../src/**/*.stories.@(js|ts)'],
  addons: [
    '@storybook/addon-docs',
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-actions'
  ],
  framework: '@storybook/vue3',
  core: {
    builder: '@storybook/builder-vite'
  },
  async viteFinal(config) {
    const { config: userConfig } = await loadConfigFromFile(
      path.resolve(__dirname, '../vite.config.ts')
    )

    return mergeConfig(config, {
      ...userConfig,
      plugins: [
        alias({
          entries: [{ find: '@', replacement: '/src' }]
        })
      ]
    })
  }
}
