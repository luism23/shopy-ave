import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/pro-solid-svg-icons'
import { far } from '@fortawesome/pro-regular-svg-icons'
import { fal } from '@fortawesome/pro-light-svg-icons'
import { fad } from '@fortawesome/pro-duotone-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import withEvents from 'storybook-auto-events'

import '../src/styles/index.scss'

export const decorators = [
  withEvents,
  (story) => ({
    components: { story },
    template: '<div style="margin: 2rem;"><story /></div>'
  })
]

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/
    }
  }
}

library.add(fas, fal, far, fad, fab)
