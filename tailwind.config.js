module.exports = {
  content: ['./index.html', './public/**/*.html', './src/**/*.{ts,vue}'],
  theme: {
    screens: {
      sm: { max: '1240px' },
      md: { max: '1440px' },
      lg: { max: '1536px' },
      xl: { min: '1920px' }
    },
    container: { center: true },
    borderRadius: {
      none: '0',
      sm: '0.125rem',
      DEFAULT: '0.25rem',
      md: '10px',
      xl: '11px',
      lg: '20px',
      full: '9999px',
      large: '12px'
    },
    extend: {
      colors: {
        gray: {
          50: '#F8F8F8',
          100: '#F1F1F1',
          200: '#E2E2E2',
          300: '#D8D8D8',
          400: '#C2C2C2',
          450: '#A7A6A6',
          500: '#8F8F90',
          600: '#8A8A8A',
          800: '#42484E'
        },
        red: {
          50: ' #E8BFCA',
          100: '#EABCC7',
          150: '#F7DFE5',
          300: '#DD341D',
          400: '#DF1125',
          500: '#C80935',
          600: '#B0052D',
          700: '#B21A1B',
          800: '#981433',
          900: '#8E181B'
        },
        amber: {
          100: '#E15B24',
          200: '#EDE437',
          300: '#E3E030',
          400: '#F7D718',
          500: '#FBAB2D',
          600: '#F9B017',
          700: '#F28C1B',
          800: '#F18403',
          900: '#ED6E1D'
        },
        emerald: {
          100: '#C7F7D7',
          200: '#BEC61A',
          300: '#1DDD5E',
          400: '#17BE50',
          500: '#94A611',
          600: '#139840',
          700: '#1BC755',
          800: '#107E35',
          900: '#0F642C',
          1000: '#D0D82B'
        },
        blue: {
          600: '#2F6997'
        }
      },
      fontFamily: {
        sans: 'Roboto',
        serif: 'Poppins'
      },
      minWidth: {
        desktop: '1366px'
      },
      borderRadius: {
        'ave-sm': '1.25rem'
      },
      boxShadow: {
        'ave-card-primary': '0px 8px 16px 0px #E5E5E5'
      },
      fontSize: {
        xs: ['10px', '12px'],
        sm: ['12px', '14px'],
        base: ['14px', '16px'],
        lg: ['16px', '19px'],
        xl: ['18px', '21px'],
        '2xl': ['16px', '28px'],
        '3xl': ['24px', '36px'],
        '4xl': ['32px', '48px'],
        '5xl': ['40px', '60px'],
        '6xl': ['60px', '60px'],
        '7xl': ['80px', '80px']
      },
      backgroundImage: {
        'ave-img-login': "url('/assets/img/background/background-login.png')",
        'ave-img-create-account':
          "url('/assets/img/background/background-create-account.png')",
        'ave-img-restore':
          "url('/assets/img/background/background-restore.png')"
      }
    }
  },
  variants: {
    extend: {
      backgroundColor: ['checked', 'active'],
      textColor: ['checked', 'active'],
      borderColor: ['checked', 'hover', 'active']
    }
  },
  plugins: [require('@tailwindcss/forms')]
}
